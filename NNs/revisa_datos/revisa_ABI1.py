import datetime as dt
import numpy as np
import glob
import pandas as pd
import sys
from scipy.interpolate import griddata
sys.path.insert(1, '/home/storm/scripts')
from informacion_zonas import limites_zonas
from obtiene_fr_o_bt import *
import pickle

region        = "Valle_de_Aburra"
lim05k        = limites_zonas()["0.5km"][region]
lim1k         = limites_zonas()["1.0km"][region]
lim2k         = limites_zonas()["2.0km"][region]
interp_method = "linear" # metodo de interpolación
lons_guide, lats_guide, _ = obtiene_fr_o_bt(lim2k, ["/home/storm/lightnings_research/NNs/data/OR_ABI-L1b-RadF-M6C13_G16_s20220851750203_e20220851759522_c20220851759591.nc"], '13')


lons_sel, lats_sel, data_sel = obtiene_fr_o_bt(lim2k, ["/mnt/GOES_data/20240327/OR_ABI-L1b-RadF-M6C13_G16_s20240872210185_e20240872219505_c20240872219557.nc"], '13')
# shape   = data_sel.shape[0] * data_sel.shape[1]
# point   = np.transpose(np.array([lons_sel.reshape(shape), lats_sel.reshape(shape)]))
# data2km = griddata(point, data_sel.reshape(shape), (lons_guide, lats_guide), method=interp_method)

