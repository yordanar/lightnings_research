import numpy as np
import glob
import sys
import datetime as dt
from dateutil.relativedelta import *
import pandas as pd
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.feature import ShapelyFeature
from cartopy.io.shapereader import Reader
sys.path.insert(1, '/home/storm/lightnings_research/archivos_data')
from informacion_zonas import limites_zonas
from obtiene_fr_o_bt import *


ruta_files    = glob.glob("/home/storm/lightnings_research/NNs/data/VA_conteo_10m/30min_cumuled/conteo_descargas_30min_*.npy")
ruta_files.sort()
equatorial_lag= 5
freq_ABI      = 10  # frecuencia en minutos de ABI
ruta_amva     = '/home/storm/Shapes/mpios_alt.shp'
figuras_save  = "/home/storm/lightnings_research/NNs/revisa_datos/images/"
ruta_example = "/home/storm/lightnings_research/NNs/data/OR_ABI-L1b-RadF-M6C13_G16_s20220851750203_e20220851759522_c20220851759591.nc"
latmax = 6.6
latmin = 5.9
lonmax = -75.2
lonmin = -75.8

for ruta_data in ruta_files:

    boundaries_region_2k = limites_zonas()["2.0km"]["Valle_de_Aburra"]
    lons, lats, _ = obtiene_fr_o_bt(boundaries_region_2k, [ruta_example], '13')
    # lonmin, lonmax = np.min(lons), np.max(lons)
    # latmin, latmax = np.min(lats), np.max(lats)

    mapa_acum       = np.load(ruta_data)

    total_lightnings = [np.sum(mapa_acum[i]) for i in range(len(mapa_acum))]
    index            = np.argmax(total_lightnings)

    mapas_descargas = np.load(ruta_data)/4 # 4 km2 el área de cada pixel
    mapas_descargas[mapas_descargas == 0] = np.nan

    fecha1       = dt.datetime.strptime(ruta_data[ruta_data.find(".npy")-6:-4], "%Y%m")
    fecha2       = fecha1 + relativedelta(months=1) - relativedelta(seconds=1)
    fechas_10min = pd.date_range(fecha1, fecha2, freq=str(freq_ABI)+"min") 
    fechas_10min = fechas_10min[(fechas_10min.hour >= 12) & (fechas_10min.hour <= 23)] 
    fechas_10min = fechas_10min + pd.Timedelta(minutes=equatorial_lag)


    # pinta
    fig = plt.figure(figsize=(9,9))
    ax1 = fig.add_axes([0.1, 0.1, 0.8, 0.8], projection=ccrs.PlateCarree())

    gl  = ax1.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=0.9, color='k', linestyle=':', xlocs=np.arange(lonmin, lonmax, 0.1), ylocs=np.arange(latmin, latmax, 0.1))
    gl.ylabel_style = {'weight':'light'}
    gl.xlabel_style = {'weight':'light'}
    gl.top_labels   = False
    gl.right_labels = False
    ax1.set_extent([lonmin, lonmax, latmin, latmax], ccrs.PlateCarree())

    shape_feature = ShapelyFeature(Reader(ruta_amva).geometries(), ccrs.PlateCarree(), facecolor='none', edgecolor ='k')
    ax1.add_feature(shape_feature, linewidth=1)
    max_density = 15.0
    bounds = np.linspace(0, max_density, 50)
    P = ax1.pcolormesh(lons, lats, mapas_descargas[index], cmap="rainbow", transform=ccrs.PlateCarree(), vmin=0, vmax=np.nanmax(mapas_descargas[index]))

    barra   = plt.colorbar(P, orientation='horizontal',extend='max')
    barra.set_label('Número de Descargas/$km^2$')

    fig.suptitle("Densidad de descargas\n" + fechas_10min[index].strftime("%Y-%m-%d %H:%M"), fontsize=16)

    # guarda
    plt.savefig(figuras_save + fechas_10min[index].strftime("%Y%m%d%H%M") + ".png", dpi=300, bbox_inches='tight')
    plt.close("all")

    print('"', fechas_10min[index].strftime("%Y-%m-%d %H:%M"), '"', '"', (fechas_10min[index]+relativedelta(minutes=freq_ABI)).strftime("%Y-%m-%d %H:%M"), '"')

