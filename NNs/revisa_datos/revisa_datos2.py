import numpy as np
import glob
import sys
import datetime as dt
from dateutil.relativedelta import *
import pandas as pd
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.feature import ShapelyFeature
from cartopy.io.shapereader import Reader
sys.path.insert(1, '/home/storm/lightnings_research/archivos_data')
sys.path.insert(1, '/home/storm/lightnings_research/NNs/data/data_ready')
from depura_datos_lightnings import lightnings_10_min, lightnings_15_min
from informacion_zonas import limites_zonas
from obtiene_fr_o_bt import *


raw_lightnings_data_15m, dates_lightnings_15m = lightnings_15_min()
raw_lightnings_data_10m, dates_lightnings_10m = lightnings_10_min()
dates_lightnings_10m = pd.DatetimeIndex(dates_lightnings_10m)
dates_lightnings_15m = pd.DatetimeIndex(dates_lightnings_15m)
equatorial_lag= 5
freq_ABI      = 10  # frecuencia en minutos de ABI
ruta_amva     = '/home/storm/Shapes/mpios_alt.shp'
figuras_save  = "/home/storm/lightnings_research/NNs/revisa_datos/images2/"
ruta_example = "/home/storm/lightnings_research/NNs/data/OR_ABI-L1b-RadF-M6C13_G16_s20220851750203_e20220851759522_c20220851759591.nc"
boundaries_region_2k = limites_zonas()["2.0km"]["Valle_de_Aburra"]
lons, lats, _ = obtiene_fr_o_bt(boundaries_region_2k, [ruta_example], '13')
# lonmin, lonmax = np.min(lons), np.max(lons)
# latmin, latmax = np.min(lats), np.max(lats)
latmax = 6.6
latmin = 5.9
lonmax = -75.2
lonmin = -75.8


#index = np.arange(len(dates_lightnings_10m), dtype=int)
index = np.arange(len(dates_lightnings_15m), dtype=int)
np.random.shuffle(index)
#dates_lightnings    = dates_lightnings_10m[index]
dates_lightnings    = dates_lightnings_15m[index]
#raw_lightnings_data = raw_lightnings_data_10m[index]
raw_lightnings_data = raw_lightnings_data_15m[index]
total_lightnings = np.array([np.sum(raw_lightnings_data[i]) for i in range(len(dates_lightnings))])
pos              = np.where(total_lightnings > 10)[0]

np.random.shuffle(pos)


for i in pos[:30]:
    mapa_acum       = raw_lightnings_data[i]
    mapas_descargas = mapa_acum/4 # 4 km2 el área de cada pixel
    mapas_descargas[mapas_descargas == 0] = np.nan
    # pinta
    fig = plt.figure(figsize=(9,9))
    ax1 = fig.add_axes([0.1, 0.1, 0.8, 0.8], projection=ccrs.PlateCarree())
    gl  = ax1.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=0.9, color='k', linestyle=':', xlocs=np.arange(lonmin, lonmax, 0.1), ylocs=np.arange(latmin, latmax, 0.1))
    gl.ylabel_style = {'weight':'light'}
    gl.xlabel_style = {'weight':'light'}
    gl.top_labels   = False
    gl.right_labels = False
    ax1.set_extent([lonmin, lonmax, latmin, latmax], ccrs.PlateCarree())
    shape_feature = ShapelyFeature(Reader(ruta_amva).geometries(), ccrs.PlateCarree(), facecolor='none', edgecolor ='k')
    ax1.add_feature(shape_feature, linewidth=1)
    max_density = 15.0
    bounds = np.linspace(0, max_density, 50)
    P = ax1.pcolormesh(lons, lats, mapas_descargas, cmap="rainbow", transform=ccrs.PlateCarree(), vmin=0, vmax=np.nanmax(mapas_descargas))
    barra   = plt.colorbar(P, orientation='horizontal',extend='max')
    barra.set_label('Número de Descargas/$km^2$')
    fig.suptitle("Densidad de descargas\n" + dates_lightnings[i].strftime("%Y-%m-%d %H:%M"), fontsize=16)
    # guarda
    plt.savefig(figuras_save + dates_lightnings[i].strftime("%Y%m%d%H%M") + ".png", dpi=300, bbox_inches='tight')
    plt.close("all")
    print('"', dates_lightnings[i].strftime("%Y-%m-%d %H:%M"), '"', '"', (dates_lightnings[i]+relativedelta(minutes=freq_ABI)).strftime("%Y-%m-%d %H:%M"), '"')

