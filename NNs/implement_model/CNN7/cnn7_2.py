import numpy as np
import tensorflow as tf
import pickle

# Verificar si TensorFlow detecta la GPU
physical_devices = tf.config.list_physical_devices('GPU')
print("Num GPUs Available: ", len(physical_devices))

if len(physical_devices) > 0:
    #tf.config.experimental.set_memory_growth(physical_devices[0], True)
    tf.config.experimental.set_visible_devices(physical_devices[0], 'GPU')
    print("TensorFlow está usando la GPU")
else:
    print("TensorFlow no detecta ninguna GPU")


#######################################                      #######################################
#######################################   HYPERPARAMETERS    #######################################
#######################################                      #######################################

layer_num           = [2,2,2]
kernel_num          = [[16,32],[32,64], [48,96]]
batch_size          = [64,128,256]
#learning_rate       = [10e-2, 10e-3, 10e-4]
learning_rate       = [0.005, 0.007, 0.009, 0.02, 0.04, 0.06, 0.08, 0.15, 0.25, 0.35, 0.45, 0.55]
learning_rate_decay = [0.8]
#reg_parameter       = [10e-4, 10e-5, 10e-6, 10e-7]
reg_parameter       = [10e-5]
kernel_size         = (3,3)
num_epochs          = 1000

#######################################                      #######################################
####################################### LECTURA DE LOS DATOS #######################################
#######################################                      #######################################

ABI_train           = np.load("/home/storm/lightnings_research/NNs/data/balanced_data5/TRAIN_ABI.npy")
ABI_val             = np.load("/home/storm/lightnings_research/NNs/data/balanced_data5/VAL_ABI.npy")
ABI_test            = np.load("/home/storm/lightnings_research/NNs/data/balanced_data5/TEST_ABI.npy")

lightnings_train    = np.load("/home/storm/lightnings_research/NNs/data/balanced_data5/TRAIN_LIGHTNING_TAGS.npy")
lightnings_val      = np.load("/home/storm/lightnings_research/NNs/data/balanced_data5/VAL_LIGHTNING_TAGS.npy")
lightnings_test     = np.load("/home/storm/lightnings_research/NNs/data/balanced_data5/TEST_LIGHTNING_TAGS.npy")

samples = len(lightnings_train)

#######################################                                     #######################################
####################################### TRAINING AND HYPERPARAMETER TUNNING #######################################
#######################################                                     #######################################

for ln, kn in list(zip(layer_num,kernel_num)):
    for bz in batch_size:
        for lr in learning_rate:
            for lrd in learning_rate_decay:
                for rp in reg_parameter:
                
                    print("kn1=" + str(kn[0]) + ", kn2=" + str(kn[1]) + ", bs=" + str(bz) + ", lr=" + str(lr) + ", lrd=" + str(lrd) + ", rp=" + str(rp))
                    model_filename = "model_kn1_" + str(kn[0]) + "_kn2_" + str(kn[1]) + "_bs_" + str(bz) + "_lr_" + str(lr)[2:] + "_lrd_" + str(int(lrd*100)) + "_rp_" + str(int(np.log10(rp)*-1))

                    ##########################################                                      #######################################
                    ########################################## CONSTRUYE LA ARQUITECTURA DEL MODELO #######################################
                    ##########################################                                      #######################################

                    model = tf.keras.models.Sequential()
                    model.add(tf.keras.layers.Input(shape=ABI_train.shape[1:]))
                    model.add(tf.keras.layers.Conv2D(kn[0], kernel_size, activation='relu', kernel_regularizer=tf.keras.regularizers.l2(rp), kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(ABI_train.shape[-1]*kernel_size[0]*kernel_size[1]))))
                    model.add(tf.keras.layers.BatchNormalization())
                    model.add(tf.keras.layers.MaxPooling2D(2,2))
                    model.add(tf.keras.layers.Dropout(0.3))
                    model.add(tf.keras.layers.Conv2D(kn[1], kernel_size, activation='relu', kernel_regularizer=tf.keras.regularizers.l2(rp), kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(kn[0]*kernel_size[0]*kernel_size[1]))))
                    model.add(tf.keras.layers.BatchNormalization())
                    model.add(tf.keras.layers.MaxPooling2D(2,2))
                    model.add(tf.keras.layers.Dropout(0.3))
                    final_size = (6,6)
                    
                    if ln == 3:
                        model.add(tf.keras.layers.Conv2D(kn[2], kernel_size, activation='relu', kernel_regularizer=tf.keras.regularizers.l2(rp), kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(kn[1]*kernel_size[0]*kernel_size[1]))))
                        model.add(tf.keras.layers.BatchNormalization())
                        model.add(tf.keras.layers.MaxPooling2D(2,2))
                        model.add(tf.keras.layers.Dropout(0.3))
                        final_size = (2,2)

                    " Flatten the results to feed into a DNN"
                    model.add(tf.keras.layers.Flatten())
                    " 512 neuron hidden layer"
                    model.add(tf.keras.layers.Dense(512, activation='relu', kernel_regularizer=tf.keras.regularizers.l2(rp), kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(kn[-1]*final_size[0]*final_size[1]))))
                    model.add(tf.keras.layers.BatchNormalization())
                    model.add(tf.keras.layers.Dropout(0.3))
                    "Only 1 output neuron. It will contain a value from 0-1 where 0 for 1 class ('cats') and 1 for the other ('dogs')"
                    model.add(tf.keras.layers.Dense(1024, activation='sigmoid'))


                    ##########################################                     #######################################
                    ########################################## METRICAS DEL MODELO #######################################
                    ##########################################                     #######################################

                    accuracy = tf.keras.metrics.BinaryAccuracy()
                    AUC      = tf.keras.metrics.AUC()
                    TP       = tf.keras.metrics.TruePositives()
                    FP       = tf.keras.metrics.FalsePositives()
                    TN       = tf.keras.metrics.TrueNegatives()
                    FN       = tf.keras.metrics.FalseNegatives()


                    ##########################################                   #######################################
                    ########################################## COMPILA Y ENTRENA #######################################
                    ##########################################                   #######################################

                    LR_init = tf.keras.optimizers.schedules.InverseTimeDecay(lr, int(samples/bz), lrd)
                    adam = tf.keras.optimizers.Adam(learning_rate=LR_init)
                    model.compile(optimizer = adam,
                                loss = tf.keras.losses.BinaryCrossentropy(),#'crossentropy',
                                metrics=[accuracy,AUC,TP,FP,TN,FN])
                    
                    result = model.fit(ABI_train, lightnings_train,
                            batch_size=bz,
                            epochs=3000,
                            verbose=0,
                            validation_data=(ABI_val, lightnings_val))

                    ##########################################                                               #######################################
                    ########################################## GUARDA MODELO (weights and biases) y METRICAS #######################################
                    ##########################################                                               #######################################

                    "Save model"
                    save_path  = "/home/storm/lightnings_research/NNs/implement_model/CNN7/"
                    model.save(save_path + model_filename + ".keras")

                    "Save metrics history"
                    f = open(save_path + "history_" + model_filename + ".pkl", "wb") 
                    pickle.dump(result.history, f)