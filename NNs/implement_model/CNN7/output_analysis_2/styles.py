# markers

def return_marker(vHP, HP):
    if HP == "LR":
        if vHP == "-1":
            marker = "*"
        elif vHP == "-2":
            marker = "d"
        elif vHP == "-3":
            marker = "o"
        elif vHP == "0":
            marker = "X"
        elif vHP == "-5":
            marker = "^"
        elif vHP == "-6":
            marker = "p"
    elif HP == "RP":
        if vHP == "-3":
            marker = "*"
        elif vHP == "-4":
            marker = "d"
        elif vHP == "-5":
            marker = "o"
        elif vHP == "-6":
            marker = "X"
        elif vHP == "-6":
            marker = "^"
        elif vHP == "-7":
            marker = "p"

    return marker

def return_color(vHP, HP):
        
    if HP == "KNS":
        if vHP == "8_16":
            Color = "k"
        elif vHP == "16_32":
            Color = "b"
        elif vHP == "32_64":
            Color = "r"
        elif vHP == "48_96":
            Color = "g"


    elif HP == "LR":
        if vHP == "0.005":
            Color = "#a93226"
        elif vHP == "0.007":
            Color = "#e74c3c"
        elif vHP == "0.009":
            Color = "#f1948a"
        elif vHP == "0.02":
            Color = "#aed6f1"
        elif vHP == "0.04":
            Color = "#780fb4"
        elif vHP == "0.06":
            Color = "#a54bd8"
        elif vHP == "0.08":
            Color = "#ca81f4"
        elif vHP == "0.15":
            Color = "#145a32"
        elif vHP == "0.25":
            Color = "#148f77"
        elif vHP == "0.35":
            Color = "#2ecc71"
        elif vHP == "0.45":
            Color = "#82e0aa"
        elif vHP == "0.55":
            Color = "#d5f5e3"

    elif HP == "RP":
        if vHP == "-1":
            Color = "k"
        elif vHP == "-2":
            Color = "b"
        elif vHP == "-3":
            Color = "r"
        elif vHP == "-4":
            Color = "g"


    elif HP == "BS":
        if vHP == "64":
            Color = "b"
        elif vHP == "128":
            Color = "r"
        elif vHP == "256":
            Color = "g"


    elif HP == "LRD":
        if vHP == "60":
            Color = "b"
        elif vHP == "80":
            Color = "r"
        elif vHP == "90":
            Color = "g"
        elif vHP == "95":
            Color = "k"

    return Color
