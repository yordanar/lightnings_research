import glob
import numpy as np
import pickle
import matplotlib.pyplot as plt
from styles import *

# epochs
epochs = 1000

# axis
axis_x = "Precision"
axis_y = "Recall"

# compare hyperparameters
HP1 = "RP"

# names hyperparameters
legend_hyperp = {"LR": "",
                "RP" : "$10^{",
                "BS" : "",
                "LRD": "",
                "KN1": "",
                "KN2": "", 
                "KNS": ""}
    
legend_hyperp_final_part = {"LR" : "",
                            "RP" : "}$",
                            "BS" : "",
                            "LRD": "",
                            "KN1": "",
                            "KN2": "", 
                            "KNS": ""}
    
# paths
ruta_files = "/mnt/data/storm/lightnings_research/NNs/implement_model/CNN7/CNN7_2/"
ruta_save  = "/home/storm/lightnings_research/NNs/implement_model/CNN7/output_analysis_2/miscelanea/"

# files list
files = glob.glob(ruta_files + "*.pkl")
files.sort()


for HP2 in ["LR", "KNS", "LRD", "BS"]:

    # Graph results
    Set_V1 = set()
    Set_V2   = set()
    countV1   = 0
    countV2   = 0

    Fig       = plt.figure(figsize=(13,9))
    ax        = Fig.add_axes([0.07, 0.1, 0.4, 0.75])
    ax2       = Fig.add_axes([0.57, 0.1, 0.4, 0.75])

    for f in files:
        
        metrics       = pickle.load(open(f, "rb"))
            
        # extract a rare index in the name, common to all of the metrics
        metrics_list  = list(metrics.keys())

        # return marker
        auc_str       = metrics_list[0]
        index_metrics = ""
        if len(auc_str) >= 4:
            index_metrics = "_" + auc_str[4:]

        indexes = {}

        # Now extract the metrics
        AUC_train  = np.array(metrics["auc" + index_metrics]            ); AUC_val  = np.array(metrics["val_auc" + index_metrics])
        ACC_train  = np.array(metrics["binary_accuracy"]                ); ACC_val  = np.array(metrics["val_binary_accuracy"])
        loss_train = np.array(metrics["loss"]                           ); loss_val = np.array(metrics["val_loss"])
        FN_train   = np.array(metrics["false_negatives" + index_metrics]); FN_val   = np.array(metrics["val_false_negatives" + index_metrics])
        FP_train   = np.array(metrics["false_positives" + index_metrics]); FP_val   = np.array(metrics["val_false_positives" + index_metrics])
        TN_train   = np.array(metrics["true_negatives"  + index_metrics]); TN_val   = np.array(metrics["val_true_negatives"  + index_metrics])
        TP_train   = np.array(metrics["true_positives"  + index_metrics]); TP_val   = np.array(metrics["val_true_positives"  + index_metrics])

        indexes.update({"AUC_train"  : AUC_train}) ; indexes.update({"AUC_val"  : AUC_val})
        indexes.update({"ACC_train"  : ACC_train}) ; indexes.update({"ACC_val"  : ACC_val})
        indexes.update({"loss_train" : loss_train}); indexes.update({"loss_val" : loss_val})
        indexes.update({"FN_train"   : FN_train})  ; indexes.update({"FN_val"   : FN_val})
        indexes.update({"FP_train"   : FP_train})  ; indexes.update({"FP_val"   : FP_val})
        indexes.update({"TN_train"   : TN_train})  ; indexes.update({"TN_val"   : TN_val})
        indexes.update({"TP_train"   : TP_train})  ; indexes.update({"TP_val"   : TP_val})


        # Compute performance indexes
        indexes.update({"Precision_train"   : TP_train / (TP_train + FP_train)})     ; indexes.update({"Precision_val"   : TP_val / (TP_val + FP_val)})
        indexes.update({"Recall_train"      : TP_train / (TP_train + FN_train)})     ; indexes.update({"Recall_val"      : TP_val / (TP_val + FN_val)})
        indexes.update({"Specificity_train" : TN_train / (TN_train + FP_train)})     ; indexes.update({"Specificity_val" : TN_val / (TN_val + FP_val)})
        indexes.update({"FPR_train"         : FP_train / (TN_train + FP_train)})     ; indexes.update({"FPR_val"         : FP_val / (TN_val + FP_val)})
        
        indexes.update({"ACC_train_comp"    : (TP_train + TN_train)/(TP_train + TN_train + FP_train + FN_train)})
        indexes.update({"ACC_val_comp"      :     (TP_val + TN_val)/(TP_val   + TN_val   + FP_val   + FN_val)})
        
        
        # Graph scatterplot from the final epoch
        values_metrics = {"LR" : "0." + f[f.find("lr_")+3:f.find("lrd_")-1],
                        "RP" : str(-1*int(f[f.find("rp")+3:f.find("rp")+4])),
                        "BS" : f[f.find("bs")+3:f.find("lr")-1],
                        "LRD": f[f.find("lrd")+4:f.find("lrd")+6],
                        "KN1": f[f.find("kn1")+4:f.find("kn2")-1], 
                        "KN2": f[f.find("kn2")+4:f.find("bs")-1], 
                        "KNS": f[f.find("kn1")+4:f.find("kn2")-1] + "_" + f[f.find("kn2")+4:f.find("bs")-1]}
        
        
        V1             = values_metrics[HP1]
        V2             = values_metrics[HP2]

        Set_V1.add(V1)
        Set_V2.add(V2)
        
        Marker       = return_marker(V1, HP1)
        Color        = return_color(V2, HP2)
        
        if len(Set_V1) == countV1:
            ax.scatter(indexes[axis_x + "_train"][-1], indexes[axis_y + "_train"][-1], marker=Marker, s=100, alpha=0.5, color=Color)
        else:
            ax.scatter(indexes[axis_x + "_train"][-1], indexes[axis_y + "_train"][-1], marker=Marker, s=100, alpha=0.5, color=Color, label=HP1 + ": " + legend_hyperp[HP1] + V1 + legend_hyperp_final_part[HP1])

        ax2.scatter(indexes[axis_x + "_val"][-1], indexes[axis_y + "_val"][-1], marker=Marker, s=100, alpha=0.5, color=Color)
        if len(Set_V2) != countV2 and V1 == "-3":
            ax2.scatter(indexes[axis_x + "_val"][-1], indexes[axis_y + "_val"][-1], marker=Marker, s=100, alpha=0.5, color=Color, label=HP2 + ": " + legend_hyperp[HP2] + V2 + legend_hyperp_final_part[HP2])
            countV2 = len(Set_V2)

        countV1 = len(Set_V1)

    Fig.suptitle(axis_x + " vs " + axis_y + " (epochs: " + str(epochs) + ")\nBinary cross entropy - ADAM", fontsize=20)
    ax.set_title('Training subset', fontsize=17)
    ax2.set_title('Validation subset', fontsize=17)

    ax.tick_params(axis='both', which='major', labelsize=17)
    ax2.tick_params(axis='both', which='major', labelsize=17)

    ax.set_xlabel(axis_x, fontsize=17)
    ax.set_ylabel(axis_y, fontsize=17)
    ax2.set_xlabel(axis_x, fontsize=17)

    ax.legend(loc="upper left", prop={'size': 17}, ncol=2)
    ax2.legend(loc="center right", prop={'size': 17}, ncol=1)

    plt.savefig(ruta_save + axis_x + "_" + axis_y + "_HP1_" + HP1 + "_HP2_" + HP2 +".png")
    plt.close("all")
