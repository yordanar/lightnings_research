import numpy as np
import tensorflow as tf
import pickle

# Verificar si TensorFlow detecta la GPU
physical_devices = tf.config.list_physical_devices('GPU')
print("Num GPUs Available: ", len(physical_devices))

if len(physical_devices) > 0:
    #tf.config.experimental.set_memory_growth(physical_devices[0], True)
    tf.config.experimental.set_visible_devices(physical_devices[0], 'GPU')
    print("TensorFlow está usando la GPU")
else:
    print("TensorFlow no detecta ninguna GPU")



#######################################                      #######################################
#######################################   HYPERPARAMETERS    #######################################
#######################################                      #######################################

layer_num           = [2,2,3]
kernel_num          = [[16,32],[32,64],[16,32,64]]
batch_size          = [64,128,512]
learning_rate       = [10e-2, 10e-4, 10e-5, 10e-6, 10e-7]
learning_rate_decay = [0.6,0.8,0.9,0.95]
reg_parameter       = [10e-4, 10e-3, 10e-2]
kernel_size         = (3,3)
num_epochs          = 3000

#######################################                      #######################################
####################################### LECTURA DE LOS DATOS #######################################
#######################################                      #######################################

ABI_05 = np.load("/home/storm/lightnings_research/NNs/data/data_ready/ABI_05.npy")
ABI_06 = np.load("/home/storm/lightnings_research/NNs/data/data_ready/ABI_06.npy")
ABI_08 = np.load("/home/storm/lightnings_research/NNs/data/data_ready/ABI_08.npy")
ABI_09 = np.load("/home/storm/lightnings_research/NNs/data/data_ready/ABI_09.npy")
ABI_10 = np.load("/home/storm/lightnings_research/NNs/data/data_ready/ABI_10.npy")
ABI_13 = np.load("/home/storm/lightnings_research/NNs/data/data_ready/ABI_13.npy")

mean_ABI_05 = np.load("/home/storm/lightnings_research/NNs/data/data_ready/mean_ABI_05.npy")
mean_ABI_06 = np.load("/home/storm/lightnings_research/NNs/data/data_ready/mean_ABI_06.npy")
mean_ABI_08 = np.load("/home/storm/lightnings_research/NNs/data/data_ready/mean_ABI_08.npy")
mean_ABI_09 = np.load("/home/storm/lightnings_research/NNs/data/data_ready/mean_ABI_09.npy")
mean_ABI_10 = np.load("/home/storm/lightnings_research/NNs/data/data_ready/mean_ABI_10.npy")
mean_ABI_13 = np.load("/home/storm/lightnings_research/NNs/data/data_ready/mean_ABI_13.npy")

lightnings                 = np.load("/home/storm/lightnings_research/NNs/data/data_ready/lightnings.npy")
lightnings[lightnings > 0] = 1
samples                    = len(lightnings)

fechas = pickle.load(open("/home/storm/lightnings_research/NNs/data/data_ready/fechas.bin", "rb"))

#######################################                                             #######################################
####################################### compute statistics of independent variables #######################################
#######################################                                             #######################################

ABI_05_media      = np.mean(     ABI_05, axis=0); ABI_05_std      = np.std(     ABI_05, axis=0) 
ABI_06_media      = np.mean(     ABI_06, axis=0); ABI_06_std      = np.std(     ABI_06, axis=0) 
ABI_08_media      = np.mean(     ABI_08, axis=0); ABI_08_std      = np.std(     ABI_08, axis=0) 
ABI_09_media      = np.mean(     ABI_09, axis=0); ABI_09_std      = np.std(     ABI_09, axis=0) 
ABI_10_media      = np.mean(     ABI_10, axis=0); ABI_10_std      = np.std(     ABI_10, axis=0) 
ABI_13_media      = np.mean(     ABI_13, axis=0); ABI_13_std      = np.std(     ABI_13, axis=0) 
mean_ABI_05_media = np.mean(mean_ABI_05, axis=0); mean_ABI_05_std = np.std(mean_ABI_05, axis=0) 
mean_ABI_06_media = np.mean(mean_ABI_06, axis=0); mean_ABI_06_std = np.std(mean_ABI_06, axis=0) 
mean_ABI_08_media = np.mean(mean_ABI_08, axis=0); mean_ABI_08_std = np.std(mean_ABI_08, axis=0) 
mean_ABI_09_media = np.mean(mean_ABI_09, axis=0); mean_ABI_09_std = np.std(mean_ABI_09, axis=0) 
mean_ABI_10_media = np.mean(mean_ABI_10, axis=0); mean_ABI_10_std = np.std(mean_ABI_10, axis=0) 
mean_ABI_13_media = np.mean(mean_ABI_13, axis=0); mean_ABI_13_std = np.std(mean_ABI_13, axis=0)

ABI_media = np.concatenate([[ABI_05_media], [ABI_06_media], [ABI_08_media], [ABI_09_media], [ABI_10_media], [ABI_13_media], [mean_ABI_05_media], 
                            [mean_ABI_06_media], [mean_ABI_08_media], [mean_ABI_09_media], [mean_ABI_10_media], [mean_ABI_13_media]])
ABI_media = ABI_media.reshape(list(ABI_media.shape[1:])+ [ABI_media.shape[0]])
ABI_std = np.concatenate([[ABI_05_std], [ABI_06_std], [ABI_08_std], [ABI_09_std], [ABI_10_std], [ABI_13_std], [mean_ABI_05_std], 
                            [mean_ABI_06_std], [mean_ABI_08_std], [mean_ABI_09_std], [mean_ABI_10_std], [mean_ABI_13_std]])
ABI_std = ABI_std.reshape(list(ABI_std.shape[1:])+ [ABI_std.shape[0]])

#######################################                                                        #######################################
####################################### SEPARA LOS DATOS PARA TRAINING, VALIDATION AND TESTING #######################################
#######################################                                                        #######################################

"crea unos indices y los mezcla"
indexes = np.arange(0, samples)
np.random.shuffle(indexes)

"selecciona los datos de training, validación y testing"
train_indexes = indexes[:int(samples*0.8)]                 # 80% de datos para entrenar
val_indexes   = indexes[int(samples*0.8):int(samples*0.9)] # 10% de datos para validar
test_indexes  = indexes[int(samples*0.9):]                 # 10% de datos para testear

ABI_05_train      =      ABI_05[train_indexes];      ABI_05_val =      ABI_05[val_indexes];      ABI_05_test =      ABI_05[test_indexes]
ABI_06_train      =      ABI_06[train_indexes];      ABI_06_val =      ABI_06[val_indexes];      ABI_06_test =      ABI_06[test_indexes]
ABI_08_train      =      ABI_08[train_indexes];      ABI_08_val =      ABI_08[val_indexes];      ABI_08_test =      ABI_08[test_indexes]
ABI_09_train      =      ABI_09[train_indexes];      ABI_09_val =      ABI_09[val_indexes];      ABI_09_test =      ABI_09[test_indexes]
ABI_10_train      =      ABI_10[train_indexes];      ABI_10_val =      ABI_10[val_indexes];      ABI_10_test =      ABI_10[test_indexes]
ABI_13_train      =      ABI_13[train_indexes];      ABI_13_val =      ABI_13[val_indexes];      ABI_13_test =      ABI_13[test_indexes]
mean_ABI_05_train = mean_ABI_05[train_indexes]; mean_ABI_05_val = mean_ABI_05[val_indexes]; mean_ABI_05_test = mean_ABI_05[test_indexes]
mean_ABI_06_train = mean_ABI_06[train_indexes]; mean_ABI_06_val = mean_ABI_06[val_indexes]; mean_ABI_06_test = mean_ABI_06[test_indexes]
mean_ABI_08_train = mean_ABI_08[train_indexes]; mean_ABI_08_val = mean_ABI_08[val_indexes]; mean_ABI_08_test = mean_ABI_08[test_indexes]
mean_ABI_09_train = mean_ABI_09[train_indexes]; mean_ABI_09_val = mean_ABI_09[val_indexes]; mean_ABI_09_test = mean_ABI_09[test_indexes]
mean_ABI_10_train = mean_ABI_10[train_indexes]; mean_ABI_10_val = mean_ABI_10[val_indexes]; mean_ABI_10_test = mean_ABI_10[test_indexes]
mean_ABI_13_train = mean_ABI_13[train_indexes]; mean_ABI_13_val = mean_ABI_13[val_indexes]; mean_ABI_13_test = mean_ABI_13[test_indexes]
lightnings_train  =  lightnings[train_indexes];  lightnings_val =  lightnings[val_indexes];  lightnings_test =  lightnings[test_indexes]



#######################################                                      #######################################
####################################### ORGANIZA EL SHAPE DE LOS DATOS       #######################################
####################################### ESTANDARIZA LOS DATOS INDEPENDIENTES #######################################


ABI_train        = np.concatenate([[ABI_05_train], [ABI_06_train], [ABI_08_train], [ABI_09_train], [ABI_10_train], [ABI_13_train], [mean_ABI_05_train], [mean_ABI_06_train], [mean_ABI_08_train], [mean_ABI_09_train], [mean_ABI_10_train], [mean_ABI_13_train]])
ABI_train        = ABI_train.reshape(list(ABI_train.shape[1:]) + [ABI_train.shape[0]])
ABI_train        = (ABI_train - ABI_media)/ABI_std
lightnings_train = lightnings_train.reshape([lightnings_train.shape[0], lightnings_train.shape[1]*lightnings_train.shape[2]])

ABI_val          = np.concatenate([[ABI_05_val], [ABI_06_val], [ABI_08_val], [ABI_09_val], [ABI_10_val], [ABI_13_val], [mean_ABI_05_val], [mean_ABI_06_val], [mean_ABI_08_val], [mean_ABI_09_val], [mean_ABI_10_val], [mean_ABI_13_val]])
ABI_val          = ABI_val.reshape(list(ABI_val.shape[1:]) + [ABI_val.shape[0]])
ABI_val          = (ABI_val - ABI_media)/ABI_std
lightnings_val   = lightnings_val.reshape([lightnings_val.shape[0], lightnings_val.shape[1]*lightnings_val.shape[2]])

ABI_test        = np.concatenate([[ABI_05_test], [ABI_06_test], [ABI_08_test], [ABI_09_test], [ABI_10_test], [ABI_13_test], [mean_ABI_05_test], [mean_ABI_06_test], [mean_ABI_08_test], [mean_ABI_09_test], [mean_ABI_10_test], [mean_ABI_13_test]])
ABI_test        = ABI_test.reshape(list(ABI_test.shape[1:]) + [ABI_test.shape[0]])
ABI_test        = (ABI_test - ABI_media)/ABI_std
lightnings_test = lightnings_test.reshape([lightnings_test.shape[0], lightnings_test.shape[1]*lightnings_test.shape[2]])

jkhbvkjhvkjvh
#######################################                                     #######################################
####################################### TRAINING AND HYPERPARAMETER TUNNING #######################################
#######################################                                     #######################################

for ln, kn in list(zip(layer_num,kernel_num)):
    for bz in batch_size:
        for lr in learning_rate:
            for lrd in learning_rate_decay:
                for rp in reg_parameter:
                
                    if len(kn) == 2:
                        print("kn1=" + str(kn[0]) + ", kn2=" + str(kn[1]) + ", bs=" + str(bz) + ", lr=" + str(lr) + ", lrd=" + str(lrd) + ", rp=" + str(rp))
                        model_filename = "model_kn1_" + str(kn[0]) + "_kn2_" + str(kn[1]) + "_bs_" + str(bz) + "_lr_" + str(int(np.log10(lr)*-1)) + "_lrd_" + str(int(lrd*100)) + "_rp_" + str(int(np.log10(rp)*-1))
                    else:
                        print("kn1=" + str(kn[0]) + ", kn2=" + str(kn[1]) + ", kn3=" + str(kn[2]) + ", bs=" + str(bz) + ", lr=" + str(lr) + ", lrd=" + str(lrd) + ", rp=" + str(rp))
                        model_filename = "model_kn1_" + str(kn[0]) + "_kn2_" + str(kn[1]) + "_kn3_" + str(kn[2]) + "_bs_" + str(bz) + "_lr_" + str(int(np.log10(lr)*-1)) + "_lrd_" + str(int(lrd*100)) + "_rp_" + str(int(np.log10(rp)*-1))
            
                    ##########################################                                      #######################################
                    ########################################## CONSTRUYE LA ARQUITECTURA DEL MODELO #######################################
                    ##########################################                                      #######################################

                    model = tf.keras.models.Sequential()
                    model.add(tf.keras.layers.Input(shape=ABI_train.shape[1:]))
                    model.add(tf.keras.layers.Conv2D(kn[0], kernel_size, activation='relu', kernel_regularizer=tf.keras.regularizers.l2(rp), kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(ABI_train.shape[-1]*kernel_size[0]*kernel_size[1]))))
                    model.add(tf.keras.layers.BatchNormalization())
                    model.add(tf.keras.layers.MaxPooling2D(2,2))
                    model.add(tf.keras.layers.Dropout(0.3))
                    model.add(tf.keras.layers.Conv2D(kn[1], kernel_size, activation='relu', kernel_regularizer=tf.keras.regularizers.l2(rp), kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(kn[0]*kernel_size[0]*kernel_size[1]))))
                    model.add(tf.keras.layers.BatchNormalization())
                    model.add(tf.keras.layers.MaxPooling2D(2,2))
                    model.add(tf.keras.layers.Dropout(0.3))
                    final_size = (6,6)
                    
                    if ln == 3:
                        model.add(tf.keras.layers.Conv2D(kn[2], kernel_size, activation='relu', kernel_regularizer=tf.keras.regularizers.l2(rp), kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(kn[1]*kernel_size[0]*kernel_size[1]))))
                        model.add(tf.keras.layers.BatchNormalization())
                        model.add(tf.keras.layers.MaxPooling2D(2,2))
                        model.add(tf.keras.layers.Dropout(0.3))
                        final_size = (2,2)

                    " Flatten the results to feed into a DNN"
                    model.add(tf.keras.layers.Flatten())
                    " 512 neuron hidden layer"
                    model.add(tf.keras.layers.Dense(512, activation='relu', kernel_regularizer=tf.keras.regularizers.l2(rp), kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(kn[-1]*final_size[0]*final_size[1]))))
                    model.add(tf.keras.layers.BatchNormalization())
                    model.add(tf.keras.layers.Dropout(0.3))
                    "Only 1 output neuron. It will contain a value from 0-1 where 0 for 1 class ('cats') and 1 for the other ('dogs')"
                    model.add(tf.keras.layers.Dense(1024, activation='sigmoid'))


                    ##########################################                     #######################################
                    ########################################## METRICAS DEL MODELO #######################################
                    ##########################################                     #######################################

                    accuracy = tf.keras.metrics.BinaryAccuracy()
                    AUC      = tf.keras.metrics.AUC()
                    TP       = tf.keras.metrics.TruePositives()
                    FP       = tf.keras.metrics.FalsePositives()
                    TN       = tf.keras.metrics.TrueNegatives()
                    FN       = tf.keras.metrics.FalseNegatives()


                    ##########################################                   #######################################
                    ########################################## COMPILA Y ENTRENA #######################################
                    ##########################################                   #######################################

                    LR_init = tf.keras.optimizers.schedules.InverseTimeDecay(lr, int(samples/bz), lrd)
                    adam = tf.keras.optimizers.Adam(learning_rate=LR_init)
                    model.compile(optimizer = adam,
                                loss = tf.keras.losses.BinaryCrossentropy(),#'crossentropy',
                                metrics=[accuracy,AUC,TP,FP,TN,FN])
                    
                    result = model.fit(ABI_train, lightnings_train,
                            batch_size=bz,
                            epochs=3000,
                            verbose=0,
                            validation_data=(ABI_val, lightnings_val))

                    ##########################################                                               #######################################
                    ########################################## GUARDA MODELO (weights and biases) y METRICAS #######################################
                    ##########################################                                               #######################################

                    "Save model"
                    save_path  = "/home/storm/lightnings_research/NNs/implement_model/CNN1/"
                    model.save(save_path + model_filename + ".keras")

                    "Save metrics history"
                    f = open(save_path + "history_" + model_filename + ".pkl", "wb") 
                    pickle.dump(result.history, f)