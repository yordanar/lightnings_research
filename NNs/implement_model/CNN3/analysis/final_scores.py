import numpy as np
import pandas as pd
import glob 
import matplotlib.pyplot as plt 

ruta_save = "/home/storm/lightnings_research/NNs/implement_model/CNN3/analysis/miscelanea/"

folders = glob.glob("/home/storm/lightnings_research/NNs/implement_model/CNN3/mlruns/414002003286506507/**/")

metrics = {"loss": "loss", "auc": "AUC", "binary_accuracy": "accuracy", "false_negatives": "false negatives", "false_positives": "false positives", "true_negatives": "true negatives", "true_positives": "true positives"}

metrics_values = {}

for m in metrics.keys():

    values_train = []
    values_val   = []

    for f in folders:
        
        file_index = glob.glob(f + "metrics/auc*")
        if len(file_index) >= 1:
            
            file_index = file_index[0]
            
            if file_index[-1].isnumeric() == True:
                index      = "_" + file_index[file_index.find("auc")+4:]
            else:
                index=""
            
            if m != "loss" and m != "binary_accuracy":
                file     = pd.read_csv(f + "metrics/" + m + index    , sep=" ", names=["id", m, "epoch"])
                val_file = pd.read_csv(f + "metrics/val_" + m + index, sep=" ", names=["id", m, "epoch"])
            else:
                file     = pd.read_csv(f + "metrics/" + m    , sep=" ", names=["id", m, "epoch"])
                val_file = pd.read_csv(f + "metrics/val_" + m, sep=" ", names=["id", m, "epoch"])
         
        values_train.append(file.query("epoch == 999")[m].values[0])
        values_val.append(val_file.query("epoch == 999")[m].values[0])
        
    metrics_values.update({m + "_train": np.array(values_train)})
    metrics_values.update({m + "_val"  : np.array(values_val)})
        
# compute other metrics

precision_train = metrics_values["true_positives_train"] / (metrics_values["true_positives_train"] + metrics_values["false_positives_train"])
recall_train    = metrics_values["true_positives_train"] / (metrics_values["true_positives_train"] + metrics_values["false_negatives_train"])

precision_val   = metrics_values["true_positives_val"]   / (metrics_values["true_positives_val"]   + metrics_values["false_positives_val"])
recall_val      = metrics_values["true_positives_val"]   / (metrics_values["true_positives_val"]   + metrics_values["false_negatives_val"])

# Draw

Fig       = plt.figure(figsize=(13,9))
ax        = Fig.add_axes([0.07, 0.1, 0.4, 0.75])
ax2       = Fig.add_axes([0.57, 0.1, 0.4, 0.75])

ax.scatter(recall_train, precision_train, marker="o", s=100, alpha=0.5, color="k")
ax.scatter(recall_val  , precision_val  , marker="o", s=100, alpha=0.5, color="k")

Fig.suptitle("Recall vs Precision (epochs: 1000)\nBinary cross entropy - ADAM", fontsize=20)
ax.set_title('Training subset', fontsize=17)
ax2.set_title('Validation subset', fontsize=17)

ax.tick_params(axis='both', which='major', labelsize=17)
ax2.tick_params(axis='both', which='major', labelsize=17)

ax.set_xlabel("Recall", fontsize=17)
ax.set_ylabel("Precision", fontsize=17)
ax2.set_xlabel("Recall", fontsize=17)

ax.legend(loc="center right", prop={'size': 17}, ncol=1)
ax2.legend(loc="center right", prop={'size': 17}, ncol=1)

plt.savefig(ruta_save + "recall_precision.png")
plt.close("all")
