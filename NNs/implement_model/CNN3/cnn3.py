import numpy as np
import tensorflow as tf
import pickle
import mlflow
import matplotlib.pyplot as plt
import os
import optuna

# Verificar si TensorFlow detecta la GPU
physical_devices = tf.config.list_physical_devices('GPU')
print("Num GPUs Available: ", len(physical_devices))

if len(physical_devices) > 0:
    #tf.config.experimental.set_memory_growth(physical_devices[0], True)
    tf.config.experimental.set_visible_devices(physical_devices[0], 'GPU')
    print("TensorFlow está usando la GPU")
else:
    print("TensorFlow no detecta ninguna GPU")


#######################################                      #######################################
#######################################        PATHS         #######################################
#######################################                      #######################################

save_path  = "/home/storm/lightnings_research/NNs/implement_model/CNN3/"

#######################################                      #######################################
#######################################   HYPERPARAMETERS    #######################################
#######################################                      #######################################

kernel_size    = (3,3)
num_epochs     = 1000

#######################################                      #######################################
####################################### LECTURA DE LOS DATOS #######################################
#######################################                      #######################################

ABI_05 = np.load("/home/storm/lightnings_research/NNs/data/balanced_data/ABI_05_balanced.npy")
ABI_06 = np.load("/home/storm/lightnings_research/NNs/data/balanced_data/ABI_06_balanced.npy")
ABI_08 = np.load("/home/storm/lightnings_research/NNs/data/balanced_data/ABI_08_balanced.npy")
ABI_09 = np.load("/home/storm/lightnings_research/NNs/data/balanced_data/ABI_09_balanced.npy")
ABI_10 = np.load("/home/storm/lightnings_research/NNs/data/balanced_data/ABI_10_balanced.npy")
ABI_13 = np.load("/home/storm/lightnings_research/NNs/data/balanced_data/ABI_13_balanced.npy")

mean_ABI_05 = np.load("/home/storm/lightnings_research/NNs/data/balanced_data/mean_ABI_05_balanced.npy")
mean_ABI_06 = np.load("/home/storm/lightnings_research/NNs/data/balanced_data/mean_ABI_06_balanced.npy")
mean_ABI_08 = np.load("/home/storm/lightnings_research/NNs/data/balanced_data/mean_ABI_08_balanced.npy")
mean_ABI_09 = np.load("/home/storm/lightnings_research/NNs/data/balanced_data/mean_ABI_09_balanced.npy")
mean_ABI_10 = np.load("/home/storm/lightnings_research/NNs/data/balanced_data/mean_ABI_10_balanced.npy")
mean_ABI_13 = np.load("/home/storm/lightnings_research/NNs/data/balanced_data/mean_ABI_13_balanced.npy")

lightnings                 = np.load("/home/storm/lightnings_research/NNs/data/balanced_data/lightnings_balanced.npy")
lightnings[lightnings > 0] = 1
samples                    = len(lightnings)

fechas = pickle.load(open("/home/storm/lightnings_research/NNs/data/balanced_data/fechas_balanced.bin", "rb"))

#######################################                                             #######################################
####################################### compute statistics of independent variables #######################################
#######################################                                             #######################################

ABI_05_media      = np.mean(     ABI_05, axis=0); ABI_05_std      = np.std(     ABI_05, axis=0) 
ABI_06_media      = np.mean(     ABI_06, axis=0); ABI_06_std      = np.std(     ABI_06, axis=0) 
ABI_08_media      = np.mean(     ABI_08, axis=0); ABI_08_std      = np.std(     ABI_08, axis=0) 
ABI_09_media      = np.mean(     ABI_09, axis=0); ABI_09_std      = np.std(     ABI_09, axis=0) 
ABI_10_media      = np.mean(     ABI_10, axis=0); ABI_10_std      = np.std(     ABI_10, axis=0) 
ABI_13_media      = np.mean(     ABI_13, axis=0); ABI_13_std      = np.std(     ABI_13, axis=0) 
mean_ABI_05_media = np.mean(mean_ABI_05, axis=0); mean_ABI_05_std = np.std(mean_ABI_05, axis=0) 
mean_ABI_06_media = np.mean(mean_ABI_06, axis=0); mean_ABI_06_std = np.std(mean_ABI_06, axis=0) 
mean_ABI_08_media = np.mean(mean_ABI_08, axis=0); mean_ABI_08_std = np.std(mean_ABI_08, axis=0) 
mean_ABI_09_media = np.mean(mean_ABI_09, axis=0); mean_ABI_09_std = np.std(mean_ABI_09, axis=0) 
mean_ABI_10_media = np.mean(mean_ABI_10, axis=0); mean_ABI_10_std = np.std(mean_ABI_10, axis=0) 
mean_ABI_13_media = np.mean(mean_ABI_13, axis=0); mean_ABI_13_std = np.std(mean_ABI_13, axis=0)

ABI_media = np.concatenate([[ABI_05_media], [ABI_06_media], [ABI_08_media], [ABI_09_media], [ABI_10_media], [ABI_13_media], [mean_ABI_05_media], 
                            [mean_ABI_06_media], [mean_ABI_08_media], [mean_ABI_09_media], [mean_ABI_10_media], [mean_ABI_13_media]])
ABI_media = ABI_media.reshape(list(ABI_media.shape[1:])+ [ABI_media.shape[0]])
ABI_std = np.concatenate([[ABI_05_std], [ABI_06_std], [ABI_08_std], [ABI_09_std], [ABI_10_std], [ABI_13_std], [mean_ABI_05_std], 
                            [mean_ABI_06_std], [mean_ABI_08_std], [mean_ABI_09_std], [mean_ABI_10_std], [mean_ABI_13_std]])
ABI_std = ABI_std.reshape(list(ABI_std.shape[1:])+ [ABI_std.shape[0]])

#######################################                                                        #######################################
####################################### SEPARA LOS DATOS PARA TRAINING, VALIDATION AND TESTING #######################################
#######################################                                                        #######################################

"crea unos indices y los mezcla"
indexes = np.arange(0, samples)
np.random.shuffle(indexes)

"selecciona los datos de training, validación y testing"
train_indexes = indexes[:int(samples*0.8)]                 # 80% de datos para entrenar
val_indexes   = indexes[int(samples*0.8):int(samples*0.9)] # 10% de datos para validar
test_indexes  = indexes[int(samples*0.9):]                 # 10% de datos para testear

ABI_05_train      =      ABI_05[train_indexes];      ABI_05_val =      ABI_05[val_indexes];      ABI_05_test =      ABI_05[test_indexes]
ABI_06_train      =      ABI_06[train_indexes];      ABI_06_val =      ABI_06[val_indexes];      ABI_06_test =      ABI_06[test_indexes]
ABI_08_train      =      ABI_08[train_indexes];      ABI_08_val =      ABI_08[val_indexes];      ABI_08_test =      ABI_08[test_indexes]
ABI_09_train      =      ABI_09[train_indexes];      ABI_09_val =      ABI_09[val_indexes];      ABI_09_test =      ABI_09[test_indexes]
ABI_10_train      =      ABI_10[train_indexes];      ABI_10_val =      ABI_10[val_indexes];      ABI_10_test =      ABI_10[test_indexes]
ABI_13_train      =      ABI_13[train_indexes];      ABI_13_val =      ABI_13[val_indexes];      ABI_13_test =      ABI_13[test_indexes]
mean_ABI_05_train = mean_ABI_05[train_indexes]; mean_ABI_05_val = mean_ABI_05[val_indexes]; mean_ABI_05_test = mean_ABI_05[test_indexes]
mean_ABI_06_train = mean_ABI_06[train_indexes]; mean_ABI_06_val = mean_ABI_06[val_indexes]; mean_ABI_06_test = mean_ABI_06[test_indexes]
mean_ABI_08_train = mean_ABI_08[train_indexes]; mean_ABI_08_val = mean_ABI_08[val_indexes]; mean_ABI_08_test = mean_ABI_08[test_indexes]
mean_ABI_09_train = mean_ABI_09[train_indexes]; mean_ABI_09_val = mean_ABI_09[val_indexes]; mean_ABI_09_test = mean_ABI_09[test_indexes]
mean_ABI_10_train = mean_ABI_10[train_indexes]; mean_ABI_10_val = mean_ABI_10[val_indexes]; mean_ABI_10_test = mean_ABI_10[test_indexes]
mean_ABI_13_train = mean_ABI_13[train_indexes]; mean_ABI_13_val = mean_ABI_13[val_indexes]; mean_ABI_13_test = mean_ABI_13[test_indexes]
lightnings_train  =  lightnings[train_indexes];  lightnings_val =  lightnings[val_indexes];  lightnings_test =  lightnings[test_indexes]

escenas           = len(lightnings_train)


#######################################                                      #######################################
####################################### ORGANIZA EL SHAPE DE LOS DATOS       #######################################
####################################### ESTANDARIZA LOS DATOS INDEPENDIENTES #######################################


ABI_train        = np.concatenate([[ABI_05_train], [ABI_06_train], [ABI_08_train], [ABI_09_train], [ABI_10_train], [ABI_13_train], [mean_ABI_05_train], [mean_ABI_06_train], [mean_ABI_08_train], [mean_ABI_09_train], [mean_ABI_10_train], [mean_ABI_13_train]])
ABI_train        = ABI_train.reshape(list(ABI_train.shape[1:]) + [ABI_train.shape[0]])
ABI_train        = (ABI_train - ABI_media)/ABI_std
lightnings_train = lightnings_train.reshape([lightnings_train.shape[0], lightnings_train.shape[1]*lightnings_train.shape[2]])

ABI_val          = np.concatenate([[ABI_05_val], [ABI_06_val], [ABI_08_val], [ABI_09_val], [ABI_10_val], [ABI_13_val], [mean_ABI_05_val], [mean_ABI_06_val], [mean_ABI_08_val], [mean_ABI_09_val], [mean_ABI_10_val], [mean_ABI_13_val]])
ABI_val          = ABI_val.reshape(list(ABI_val.shape[1:]) + [ABI_val.shape[0]])
ABI_val          = (ABI_val - ABI_media)/ABI_std
lightnings_val   = lightnings_val.reshape([lightnings_val.shape[0], lightnings_val.shape[1]*lightnings_val.shape[2]])

ABI_test        = np.concatenate([[ABI_05_test], [ABI_06_test], [ABI_08_test], [ABI_09_test], [ABI_10_test], [ABI_13_test], [mean_ABI_05_test], [mean_ABI_06_test], [mean_ABI_08_test], [mean_ABI_09_test], [mean_ABI_10_test], [mean_ABI_13_test]])
ABI_test        = ABI_test.reshape(list(ABI_test.shape[1:]) + [ABI_test.shape[0]])
ABI_test        = (ABI_test - ABI_media)/ABI_std
lightnings_test = lightnings_test.reshape([lightnings_test.shape[0], lightnings_test.shape[1]*lightnings_test.shape[2]])


#######################################                                     #######################################
####################################### TRAINING AND HYPERPARAMETER TUNNING #######################################
#######################################                                     #######################################

# Iniciar seguimiento automático con MLflow
mlflow.set_experiment("lightnings_balanced_data_1try")

def create_model(trial):
    # Sugerir hiperparámetros con Optuna
    num_filters    = trial.suggest_categorical(f'filters', [[16,32],[32,64],[16,32,64]])
    num_layers     = len(num_filters)
    #num_layers     = trial.suggest_categorical('num_layers', [2,3,4,5])  # Número de capas Conv2D
    dropout_rate   = trial.suggest_float('dropout_rate', 0.2, 0.5, step=0.1)
    learning_rate  = trial.suggest_categorical('learning_rate', [1e-2,1e-3,1e-4,1e-5,1e-6,1e-7])
    decay_rate     = trial.suggest_float('decay_rate', 0.1, 0.9, step=0.1) # log=True)
    batch_size     = trial.suggest_categorical('batch_size', [64,128,512])
    regularization = trial.suggest_float('l2_regularization', 1e-6, 1e-2, log=True)

    # Definir el modelo CNN
    model = tf.keras.Sequential()
    model.add(tf.keras.layers.Input(shape=ABI_train.shape[1:]))
    
    "layer 1"
    model.add(tf.keras.layers.Conv2D(filters=num_filters[0], kernel_size=kernel_size,
                            kernel_regularizer=tf.keras.regularizers.l2(regularization),
                            activation='relu', 
                            kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(ABI_train.shape[-1]*kernel_size[0]*kernel_size[1]))))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.MaxPooling2D(2, 2))
    model.add(tf.keras.layers.Dropout(dropout_rate))
    "layer 2"
    model.add(tf.keras.layers.Conv2D(filters=num_filters[1], kernel_size=kernel_size,
                            kernel_regularizer=tf.keras.regularizers.l2(regularization),
                            activation='relu', 
                            kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(num_filters[0]*kernel_size[0]*kernel_size[1]))))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.MaxPooling2D(2, 2))
    model.add(tf.keras.layers.Dropout(dropout_rate))

    final_size = (6,6)

    "layer 3"
    if num_layers == 3:
        model.add(tf.keras.layers.Conv2D(filters=num_filters[2], kernel_size=kernel_size,
                                kernel_regularizer=tf.keras.regularizers.l2(regularization),
                                activation='relu', 
                                kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(num_filters[1]*kernel_size[0]*kernel_size[1]))))
        model.add(tf.keras.layers.BatchNormalization())
        model.add(tf.keras.layers.MaxPooling2D(2, 2))
        model.add(tf.keras.layers.Dropout(dropout_rate))
        final_size = (2,2)


    "Flatten the results to feed into a DNN"
    model.add(tf.keras.layers.Flatten())
    "512 neuron hidden layer"
    model.add(tf.keras.layers.Dense(512, activation='relu', kernel_regularizer=tf.keras.regularizers.l2(regularization), kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(num_filters[-1]*final_size[0]*final_size[1]))))
    model.add(tf.keras.layers.BatchNormalization())
    model.add(tf.keras.layers.Dropout(dropout_rate))
    "Only 1 output neuron. It will contain a value from 0-1 where 0 for 1 class ('cats') and 1 for the other ('dogs')"
    model.add(tf.keras.layers.Dense(1024, activation='sigmoid'))

    ##########################################                     #######################################
    ########################################## METRICAS DEL MODELO #######################################
    ##########################################                     #######################################

    accuracy = tf.keras.metrics.BinaryAccuracy()
    AUC      = tf.keras.metrics.AUC()
    TP       = tf.keras.metrics.TruePositives()
    FP       = tf.keras.metrics.FalsePositives()
    TN       = tf.keras.metrics.TrueNegatives()
    FN       = tf.keras.metrics.FalseNegatives()

    # Configurar el optimizador con decay
    lr_schedule = tf.keras.optimizers.schedules.InverseTimeDecay(
        initial_learning_rate=learning_rate,
        decay_steps=int(escenas / batch_size),
        decay_rate=decay_rate)

    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=lr_schedule),
                  loss=tf.keras.losses.BinaryCrossentropy(),
                  metrics=[accuracy,AUC,TP,FP,TN,FN])

    return model


def objective(trial):
    
    # Crear el modelo con los hiperparámetros sugeridos
    with mlflow.start_run(run_name=f"run_{trial.number:03d}"):
        
        print(f"{trial.number:03d}")
        
        # Crear el modelo
        model = create_model(trial)
        # Registrar los hiperparámetros
        mlflow.log_param('filters', trial.params['filters'])
        mlflow.log_param('dropout_rate', trial.params['dropout_rate'])
        mlflow.log_param('learning_rate', trial.params['learning_rate'])
        mlflow.log_param('decay_rate', trial.params['decay_rate'])
        mlflow.log_param('batch_size', trial.params['batch_size'])
        mlflow.log_param('regularization', trial.params['l2_regularization'])

        # train
        history = model.fit(ABI_train, lightnings_train,
                batch_size=trial.suggest_categorical('batch_size', [64,128,512]),
                epochs=num_epochs,
                verbose=2,
                validation_data=(ABI_val, lightnings_val))

        # Registrar las métricas de cada época
        metrics = list(history.history.keys())
        for metric in metrics:
            for epoch, value in enumerate(history.history[metric]):
                mlflow.log_metric(metric.split(f'_{trial.number}')[0], value, step=epoch)

            # Gráfico de la historia del entrenamiento
            if not 'val_' in metric:
                plt.figure()
                plt.plot(history.history[metric], label=metric.split(f'_{trial.number}')[0])
                plt.plot(history.history[f"val_{metric}"], label = f"val_{metric.split(f'_{trial.number}')[0]}")
                plt.xlabel('Epoch')
                plt.ylabel(metric.split(f'_{trial.number}')[0].upper())
                plt.ylim([0, 1])
                plt.legend(loc='lower right')
                pathsave = f"model_{trial.number:03d}"
                if not os.path.exists(pathsave):
                    os.system(f" mkdir {pathsave}")
                plt.savefig(f"{pathsave}/{metric.split(f'_{trial.number}')[0]}_plot_{trial.number:03d}.svg")
                plt.savefig(f"{pathsave}/{metric.split(f'_{trial.number}')[0]}_plot_{trial.number:03d}.png")
                
                #Registrar el gráfico en MLflow
                mlflow.log_artifact(f"{pathsave}/{metric.split(f'_{trial.number}')[0]}_plot_{trial.number:03d}.png")

        # Guardar el modelo final
        mlflow.keras.log_model(model, f"model_{trial.number:03d}")
        "save model en lorenz"
        model.save(f"{pathsave}/{trial.number:03d}.keras")
        "Save metrics history"
        perf_metrics = open(f"{pathsave}/history_{trial.number:03d}.pkl", "wb") 
        pickle.dump(history.history, perf_metrics)

# Crear el estudio de Optuna
study = optuna.create_study(direction='maximize', 
    study_name='balanced_data_1try', 
    storage='sqlite:///balanced_data_1try.db', 
    load_if_exists=True
    )
study.optimize(objective, n_trials=300)


# Mostrar mejores hiperparámetros
print('Mejores Hiperparámetros: ', study.best_trial.params)

# Visualización y Análisis
optimization_history = optuna.visualization.plot_optimization_history(study).show()
param_importances = optuna.visualization.plot_param_importances(study).show()