import glob
import numpy as np
import pickle
import matplotlib.pyplot as plt

axis_x = "Precision"
axis_y = "Recall" 

# paths
ruta_files = "/mnt/data/storm/lightnings_research/NNs/implement_model/CNN4/"
ruta_save  = "/home/storm/lightnings_research/NNs/implement_model/CNN4/output_analysis/todo/miscelanea/"

# files list
files = glob.glob(ruta_files + "*.pkl")
files.sort()

# markers
def return_marker(lr):
    if lr == 1:
        marker = "*"
    elif lr == 2:
        marker = "d"
    elif lr == 3:
        marker = "o"
    elif lr == 4:
        marker = "X"
    elif lr == 5:
        marker = "^"
    elif lr == 6:
        marker = "p"

    return marker

# def return_color(ln):
#     if ln == "kn1_16_kn2_32":
#         Color = "b"
#         capas = "16, 32"
#     elif ln == "kn1_32_kn2_64":
#         Color = "r"
#         capas = "32, 64"
#     elif ln == "kn1_16_kn2_32_kn3_64":
#         Color = "g"
#         capas = "16, 32, 64"

#     return capas, Color

# def return_color(ln):
#     if ln == "1":
#         Color = "b"
#     elif ln == "2":
#         Color = "r"
#     elif ln == "3":
#         Color = "g"
#     elif ln == "4":
#         Color = "k"

#     return Color

# def return_color(ln):
#     if ln == "64":
#         Color = "b"
#     elif ln == "128":
#         Color = "r"
#     elif ln == "512":
#         Color = "g"

#     return Color


def return_color(ln):
    if ln == "60":
        Color = "b"
    elif ln == "80":
        Color = "r"
    elif ln == "90":
        Color = "g"
    elif ln == "95":
        Color = "k"

    return Color

# Graph results
LearningR = set()
LayersN   = set()
countLR   = 0
countLN   = 0

Fig       = plt.figure(figsize=(13,9))
ax        = Fig.add_axes([0.07, 0.1, 0.4, 0.75])
ax2       = Fig.add_axes([0.57, 0.1, 0.4, 0.75])

for f in files:
    
    metrics       = pickle.load(open(f, "rb"))
        
    # extract a rare index in the name, common to all of the metrics
    metrics_list  = list(metrics.keys())

    # return marker
    auc_str       = metrics_list[0]
    index_metrics = ""
    if len(auc_str) >= 4:
        index_metrics = "_" + auc_str[4:]

    indexes = {}

    # Now extract the metrics
    AUC_train  = np.array(metrics["auc" + index_metrics]            ); AUC_val  = np.array(metrics["val_auc" + index_metrics])
    ACC_train  = np.array(metrics["binary_accuracy"]                ); ACC_val  = np.array(metrics["val_binary_accuracy"])
    loss_train = np.array(metrics["loss"]                           ); loss_val = np.array(metrics["val_loss"])
    FN_train   = np.array(metrics["false_negatives" + index_metrics]); FN_val   = np.array(metrics["val_false_negatives" + index_metrics])
    FP_train   = np.array(metrics["false_positives" + index_metrics]); FP_val   = np.array(metrics["val_false_positives" + index_metrics])
    TN_train   = np.array(metrics["true_negatives"  + index_metrics]); TN_val   = np.array(metrics["val_true_negatives"  + index_metrics])
    TP_train   = np.array(metrics["true_positives"  + index_metrics]); TP_val   = np.array(metrics["val_true_positives"  + index_metrics])

    indexes.update({"AUC_train"  : AUC_train}) ; indexes.update({"AUC_val"  : AUC_val})
    indexes.update({"ACC_train"  : ACC_train}) ; indexes.update({"ACC_val"  : ACC_val})
    indexes.update({"loss_train" : loss_train}); indexes.update({"loss_val" : loss_val})
    indexes.update({"FN_train"   : FN_train})  ; indexes.update({"FN_val"   : FN_val})
    indexes.update({"FP_train"   : FP_train})  ; indexes.update({"FP_val"   : FP_val})
    indexes.update({"TN_train"   : TN_train})  ; indexes.update({"TN_val"   : TN_val})
    indexes.update({"TP_train"   : TP_train})  ; indexes.update({"TP_val"   : TP_val})


    # Compute performance indexes
    indexes.update({"Precision_train"   : TP_train / (TP_train + FP_train)})     ; indexes.update({"Precision_val"   : TP_val / (TP_val + FP_val)})
    indexes.update({"Recall_train"      : TP_train / (TP_train + FN_train)})     ; indexes.update({"Recall_val"      : TP_val / (TP_val + FN_val)})
    indexes.update({"Specificity_train" : TN_train / (TN_train + FP_train)})     ; indexes.update({"Specificity_val" : TN_val / (TN_val + FP_val)})
    indexes.update({"FPR_train"         : FP_train / (TN_train + FP_train)})     ; indexes.update({"FPR_val"         : FP_val / (TN_val + FP_val)})
    
    indexes.update({"ACC_train_comp"    : (TP_train + TN_train)/(TP_train + TN_train + FP_train + FN_train)})
    indexes.update({"ACC_val_comp"      :     (TP_val + TN_val)/(TP_val   + TN_val   + FP_val   + FN_val)})
    
    # Graph scatterplot from the final epoch
    LR           = f[f.find("lr_")+3:f.find("lr_")+4]
    LN           = f[f.find("rp")+3:f.find("rp")+4]
    LN           = f[f.find("bs")+3:f.find("lr")-1]
    LN           = f[f.find("lrd")+4:f.find("lrd")+6]

    LearningR.add(LR)
    LayersN.add(LN)
    Marker       = return_marker(int(LR))
    Color = return_color(LN)
    
    if len(LearningR) == countLR:
        ax.scatter(indexes[axis_x + "_train"][-1], indexes[axis_y + "_train"][-1], marker=Marker, s=100, alpha=0.5, color=Color)
    else:
        ax.scatter(indexes[axis_x + "_train"][-1], indexes[axis_y + "_train"][-1], marker=Marker, s=100, alpha=0.5, color=Color, label="LR=$10^{-" + LR + "}$")
    
    
    ax2.scatter(indexes[axis_x + "_val"][-1], indexes[axis_y + "_val"][-1], marker=Marker, s=100, alpha=0.5, color=Color)
    if len(LayersN) != countLN and LR == "1":
        ax2.scatter(indexes[axis_x + "_val"][-1], indexes[axis_y + "_val"][-1], marker=Marker, s=100, alpha=0.5, color=Color, label="Kernels: "+LN)
        countLN = len(LayersN)

    countLR = len(LearningR)

Fig.suptitle(axis_x + " vs " + axis_y + " (epochs: 1000)\nBinary cross entropy - ADAM", fontsize=20)
ax.set_title('Training subset', fontsize=17)
ax2.set_title('Validation subset', fontsize=17)

ax.tick_params(axis='both', which='major', labelsize=17)
ax2.tick_params(axis='both', which='major', labelsize=17)

ax.set_xlabel(axis_x, fontsize=17)
ax.set_ylabel(axis_y, fontsize=17)
ax2.set_xlabel(axis_x, fontsize=17)

ax.legend(loc="upper right", prop={'size': 17}, ncol=2)
ax2.legend(loc="center right", prop={'size': 17}, ncol=1)

plt.savefig(ruta_save + axis_x + "_" + axis_y + ".png")
plt.close("all")
