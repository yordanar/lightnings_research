import glob
import numpy as np
import pickle
import matplotlib.pyplot as plt

YLIM   = {"loss": 0.8, "Recall": 0.5, "Precision":1, "FPR": 0.5, "AUC": 0.9}
epochs = 3000 

# paths
ruta_files = "/mnt/data/storm/lightnings_research/NNs/implement_model/CNN4/"

# files list
files = pickle.load(open("/home/storm/lightnings_research/NNs/implement_model/CNN4/output_analysis/selected_files.bin", "rb"))
files.sort()

# markers
def return_color(kn1, kn2, kn3=None):
    if kn3:
        color   = "k"
        kernels = "[16, 32, 64]"  
    else:
        if kn1 == "16" and kn2 == "32":
            color = "r"
            kernels = "[16, 32]"
        elif kn1 == "32" and kn2 == "64":
            color = "b"
            kernels = "[32, 64]"

    return kernels, color

def return_style(lr):
    if lr == 1:
        estilo = "solid"
    elif lr == 2:
        estilo = "dashed"
    elif lr == 3:
        estilo = "dotted"
    elif lr == 4:
        estilo = (0, (1, 10))
    elif lr == 5:
        estilo = "dashdot"
    elif lr == 6:
        estilo = (0, (3, 5, 1, 5, 1, 5))
        
    return estilo

# def return_marker(bs):
#     if bs == 64:
#         marker = "*"
#     elif bs == 128:
#         marker = "o"
#     elif bs == 512:
#         marker = "^"
    
#     return marker

# def return_marker_color(arch):
#     if bs == 64:
#         marker = "*"
#     elif bs == 128:
#         marker = "o"
#     elif bs == 512:
#         marker = "^"
    
#     return marker

def return_style2(pr):
    if pr == 1:
        estilo = "solid"
    elif pr == 2:
        estilo = "dashed"
    elif pr == 3:
        estilo = "dotted"
    elif pr == 4:
        estilo = (0, (1, 10))
        
    return estilo


for axis_y in ["loss", "AUC", "FPR", "Recall", "Precision"]:

    #LRselected = None  # 10^(-LRselected)
    
    for LRselected in [1, 2, 3, 4, 5, 6]:  # 10^(-LRselected)
            
        KERNELselected = None # LRD*10^(-2)
        #for KERNELselected in [60, 80, 90, 95]: # LRD*10^(-2)

        ruta_save  = "/home/storm/lightnings_research/NNs/implement_model/CNN4/output_analysis/todo_ylimits_selected_modelos/" + axis_y + "/"

        # Graph results
        LearningR = set()
        KERNELS   = set()
        RParameter= set()
        countLR   = 0
        countK    = 0
        countRP   = 0

        Fig       = plt.figure(figsize=(13,9))
        ax        = Fig.add_axes([0.07, 0.1, 0.4, 0.75])
        ax2       = Fig.add_axes([0.57, 0.1, 0.4, 0.75])

        for f in files:
            
            metrics       = pickle.load(open(f, "rb"))
                
            # extract a rare index in the name, common to all of the metrics
            metrics_list  = list(metrics.keys())

            # return marker
            auc_str       = metrics_list[0]
            index_metrics = ""
            if len(auc_str) >= 4:
                index_metrics = "_" + auc_str[4:]

            indexes = {}

            # Now extract the metrics
            AUC_train  = np.array(metrics["auc" + index_metrics]            ); AUC_val  = np.array(metrics["val_auc" + index_metrics])
            ACC_train  = np.array(metrics["binary_accuracy"]                ); ACC_val  = np.array(metrics["val_binary_accuracy"])
            loss_train = np.array(metrics["loss"]                           ); loss_val = np.array(metrics["val_loss"])
            FN_train   = np.array(metrics["false_negatives" + index_metrics]); FN_val   = np.array(metrics["val_false_negatives" + index_metrics])
            FP_train   = np.array(metrics["false_positives" + index_metrics]); FP_val   = np.array(metrics["val_false_positives" + index_metrics])
            TN_train   = np.array(metrics["true_negatives"  + index_metrics]); TN_val   = np.array(metrics["val_true_negatives"  + index_metrics])
            TP_train   = np.array(metrics["true_positives"  + index_metrics]); TP_val   = np.array(metrics["val_true_positives"  + index_metrics])

            indexes.update({"AUC_train"  : AUC_train}) ; indexes.update({"AUC_val"  : AUC_val})
            indexes.update({"ACC_train"  : ACC_train}) ; indexes.update({"ACC_val"  : ACC_val})
            indexes.update({"loss_train" : loss_train}); indexes.update({"loss_val" : loss_val})
            indexes.update({"FN_train"   : FN_train})  ; indexes.update({"FN_val"   : FN_val})
            indexes.update({"FP_train"   : FP_train})  ; indexes.update({"FP_val"   : FP_val})
            indexes.update({"TN_train"   : TN_train})  ; indexes.update({"TN_val"   : TN_val})
            indexes.update({"TP_train"   : TP_train})  ; indexes.update({"TP_val"   : TP_val})


            # Compute performance indexes
            indexes.update({"Precision_train"   : TP_train / (TP_train + FP_train)})     ; indexes.update({"Precision_val"   : TP_val / (TP_val + FP_val)})
            indexes.update({"Recall_train"      : TP_train / (TP_train + FN_train)})     ; indexes.update({"Recall_val"      : TP_val / (TP_val + FN_val)})
            indexes.update({"Specificity_train" : TN_train / (TN_train + FP_train)})     ; indexes.update({"Specificity_val" : TN_val / (TN_val + FP_val)})
            indexes.update({"FPR_train"         : FP_train / (TN_train + FP_train)})     ; indexes.update({"FPR_val"         : FP_val / (TN_val + FP_val)})
            
            indexes.update({"ACC_train_comp"    : (TP_train + TN_train)/(TP_train + TN_train + FP_train + FN_train)})
            indexes.update({"ACC_val_comp"      :     (TP_val + TN_val)/(TP_val   + TN_val   + FP_val   + FN_val)})
            

            # Graph scatterplot from the final epoch
            LR             = f[f.find("lr_")+3:f.find("lr_")+4]
            LRD            = f[f.find("lrd_")+4:-4]
            BS             = f[f.find("bs_")+3:f.find("_lr")]
            RP             = f[f.find("rp_")+3:f.find(".pkl")]
            KN1            = f[f.find("kn1_")+4:f.find("_kn2")]
            KN2            = f[f.find("kn2_")+4:f.find("_kn2")+7]
            KN3            = None
            
            if f.find("kn3") != -1:
                KN3 = f[f.find("kn3_")+4:f.find("_kn3")+7]  
            
            #Style          = return_style(int(LR ))
            Style          = return_style2(int(RP))
            kernels, Color = return_color(KN1, KN2, KN3)
            #Marker         = return_marker(int(BS))

            if LRselected == None and KERNELselected == None:

                LearningR.add(LR)
                if len(LearningR) == countLR:
                    ax.plot(np.arange(epochs), indexes[axis_y + "_train"][:], alpha=0.5, color=Color, linestyle=Style)
                else:
                    ax.plot(np.arange(epochs), indexes[axis_y + "_train"][:], alpha=0.5, color=Color, linestyle=Style, label="LR:10$^{-" + LR + "}$")

                if KN3 == None:
                    KERNELS.add(KN1 + "_" + KN2)
                else:
                    KERNELS.add(KN1 + "_" + KN2 + "_" + KN3)
                    
                ax2.plot(np.arange(epochs), indexes[axis_y + "_val"][:], alpha=0.5, color=Color, linestyle=Style)
                
                if len(KERNELS) != countK and LR == "1":
                    ax2.plot(np.arange(epochs), indexes[axis_y + "_val"][:], alpha=0.5, color=Color, linestyle=Style, label="Kernels:"+kernels)
                    countK =   len(KERNELS)
                
                countLR = len(LearningR)

            elif int(LR) == LRselected:

                RParameter.add(RP)
                if len(RParameter) == countRP:
                    ax.plot(np.arange(epochs), indexes[axis_y + "_train"][:], alpha=0.5, color=Color, linestyle=Style)
                else:
                    ax.plot(np.arange(epochs), indexes[axis_y + "_train"][:], alpha=0.5, color=Color, linestyle=Style, label="RP:10$^{-" + RP + "}$")

                if KN3 == None:
                    KERNELS.add(KN1 + "_" + KN2)
                else:
                    KERNELS.add(KN1 + "_" + KN2 + "_" + KN3)
                
                ax2.plot(np.arange(epochs), indexes[axis_y + "_val"][:], alpha=0.5, color=Color, linestyle=Style)

                if len(KERNELS) != countK:
                    ax2.plot(np.arange(epochs), indexes[axis_y + "_val"][:], alpha=0.5, color=Color, linestyle=Style, label="Kernels:"+kernels)
                    countK =   len(KERNELS)
                    
                countRP = len(RParameter)
                    

        Fig.suptitle(axis_y + " (epochs: " + str(epochs) + ") - LR = 10$^{-" + str(LRselected) + "}$\nBinary cross entropy - ADAM", fontsize=20)
        
        ax.set_title('Training subset', fontsize=17)
        ax2.set_title('Validation subset', fontsize=17)

        ax.tick_params(axis='both', which='major', labelsize=17)
        ax2.tick_params(axis='both', which='major', labelsize=17)

        ax.set_xlabel("epoch", fontsize=17)
        ax.set_ylabel(axis_y, fontsize=17)
        ax2.set_xlabel("epoch", fontsize=17)

        ax.legend(loc="center right", prop={'size': 17}, ncol=1)
        ax2.legend(loc="center right", prop={'size': 17}, ncol=1)

        ax.set_ylim(-0.02, YLIM[axis_y])

        if LRselected == None and KERNELselected == None:
            plt.savefig(ruta_save + axis_y + ".png")
        elif LRselected != None and KERNELselected == None:
            plt.savefig(ruta_save + axis_y + "_LR_" + str(LRselected) + ".png")
        elif LRselected == None and KERNELselected != None:
            plt.savefig(ruta_save + axis_y + "_kernels_" + str(KERNELselected) + ".png")

        plt.close("all")
