# markers

def return_marker(lr):
    if lr == "-1":
        marker = "*"
    elif lr == "-2":
        marker = "d"
    elif lr == "0":
        marker = "o"
    elif lr == "1":
        marker = "X"
    elif lr == "-5":
        marker = "^"
    elif lr == "-6":
        marker = "p"

    return marker

def return_color(vHP, HP):
        
    if HP == "KNS":
        if vHP == "16_32":
            Color = "b"
        elif vHP == "32_64":
            Color = "r"


    elif HP == "RP":
        if vHP == "-3":
            Color = "b"
        elif vHP == "-4":
            Color = "r"
        elif vHP == "-5":
            Color = "g"
        elif vHP == "-6":
            Color = "k"


    elif HP == "BS":
        if vHP == "64":
            Color = "b"
        elif vHP == "128":
            Color = "r"
        elif vHP == "512":
            Color = "g"


    elif HP == "LRD":
        if vHP == "60":
            Color = "b"
        elif vHP == "80":
            Color = "r"
        elif vHP == "90":
            Color = "g"
        elif vHP == "95":
            Color = "k"

    return Color
