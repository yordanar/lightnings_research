import glob
import numpy as np
import pickle
import matplotlib.pyplot as plt
import pandas as pd
from styles import *

   
# paths
ruta_files = "/mnt/data/storm/lightnings_research/NNs/implement_model/CNN8/refine_hp2/"

# files list
files = glob.glob(ruta_files + "*.pkl")
files.sort()


AUC_train         = []; AUC_val         = []; AUC_test         = []
ACC_train         = []; ACC_val         = []; ACC_test         = []
loss_train        = []; loss_val        = []; loss_test        = []
Precision_train   = []; Precision_val   = []; Precision_test   = []
Recall_train      = []; Recall_val      = []; Recall_test      = []
Specificity_train = []; Specificity_val = []; Specificity_test = []
FPR_train         = []; FPR_val         = []; FPR_test         = []


for f in files:
    
    metrics        = pickle.load(open(f, "rb"))
    model_filename = "test_metrics_" + f[f.find("history_model")+8:-4]
    
    # test metrics
    test_performance = pickle.load(open(ruta_files + model_filename + ".bin", "rb"))

    # extract a rare index in the name, common to all of the metrics
    metrics_list  = list(metrics.keys())

    # return marker
    auc_str       = metrics_list[0]
    index_metrics = ""
    if len(auc_str) >= 4:
        index_metrics = "_" + auc_str[4:]


    # Now extract the metrics
    
    FN_train   = np.array(metrics["false_negatives" + index_metrics]); FN_val   = np.array(metrics["val_false_negatives" + index_metrics]); FN_test   = test_performance["FN"]
    FP_train   = np.array(metrics["false_positives" + index_metrics]); FP_val   = np.array(metrics["val_false_positives" + index_metrics]); FP_test   = test_performance["FP"]
    TN_train   = np.array(metrics["true_negatives"  + index_metrics]); TN_val   = np.array(metrics["val_true_negatives"  + index_metrics]); TN_test   = test_performance["TN"]
    TP_train   = np.array(metrics["true_positives"  + index_metrics]); TP_val   = np.array(metrics["val_true_positives"  + index_metrics]); TP_test   = test_performance["TP"]
    
    AUC_train .append(np.array(metrics["auc" + index_metrics]            )[-1]); AUC_val .append(np.array(metrics["val_auc" + index_metrics])[-1]); AUC_test .append(test_performance["AUC"])
    ACC_train .append(np.array(metrics["binary_accuracy"]                )[-1]); ACC_val .append(np.array(metrics["val_binary_accuracy"])[-1])    ; ACC_test .append(test_performance["ACC"])
    loss_train.append(np.array(metrics["loss"]                           )[-1]); loss_val.append(np.array(metrics["val_loss"])[-1])               ; loss_test.append(test_performance["loss"])

    # Compute performance indexes
    Precision_train  .append((TP_train / (TP_train + FP_train))[-1])     ; Precision_val  .append((TP_val / (TP_val + FP_val))[-1]); Precision_test  .append((TP_test / (TP_test + FP_test)))
    Recall_train     .append((TP_train / (TP_train + FN_train))[-1])     ; Recall_val     .append((TP_val / (TP_val + FN_val))[-1]); Recall_test     .append((TP_test / (TP_test + FN_test)))
    Specificity_train.append((TN_train / (TN_train + FP_train))[-1])     ; Specificity_val.append((TN_val / (TN_val + FP_val))[-1]); Specificity_test.append((TN_test / (TN_test + FP_test)))
    FPR_train        .append((FP_train / (TN_train + FP_train))[-1])     ; FPR_val        .append((FP_val / (TN_val + FP_val))[-1]); FPR_test        .append((FP_test / (TN_test + FP_test)))

# creates a data frame with the data

metrics_train = pd.DataFrame(data={"file": files, "Precision_train": Precision_train, "Recall_train":Recall_train, "Specificity_train": Specificity_train, 
                                   "FPR_train": FPR_train, "AUC_train": AUC_train, "ACC_train":ACC_train, "loss_train": loss_train})

metrics_val   = pd.DataFrame(data={"file": files, "Precision_val": Precision_val, "Recall_val":Recall_val, "Specificity_val": Specificity_val, 
                                   "FPR_val": FPR_val, "AUC_val": AUC_val, "ACC_val":ACC_val, "loss_val": loss_val})

metrics_test  = pd.DataFrame(data={"file": files, "Precision_test": Precision_test, "Recall_test":Recall_test, "Specificity_test": Specificity_test, 
                                   "FPR_test": FPR_test, "AUC_test": AUC_test, "ACC_test":ACC_test, "loss_test": loss_test})

