import tensorflow as tf
import numpy as np
import glob
import pickle
import os

# paths
ruta_files   = "/mnt/data/storm/lightnings_research/NNs/implement_model/CNN8/refine_hp2/"
save_metrics = "/mnt/data/storm/lightnings_research/NNs/implement_model/CNN8/refine_hp2/"

# load test data
test_data = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/TEST_ABI.npy")
test_tags = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/TEST_LIGHTNING_TAGS.npy")

# files list
files = glob.glob(ruta_files + "*.keras")
files.sort()


for i, f in enumerate(files):

    
    if not os.path.exists(save_metrics + "test_metrics_" + f[f.find("refine_hp2")+11:-6] + ".bin"):

        print(i)

        loaded_model   = tf.keras.models.load_model(f)
        prediccions    = loaded_model.predict(test_data)

        # Make predictions
        test_performance = loaded_model.evaluate(test_data, test_tags, verbose=0)
        loss_test        = test_performance[0]
        ACC_test         = test_performance[1]
        AUC_test         = test_performance[2]
        TP_test          = test_performance[3]
        FP_test          = test_performance[4]
        TN_test          = test_performance[5]
        FN_test          = test_performance[6]
        
        # dict de metricas
        metrics          = {"loss":loss_test, "ACC":ACC_test, "AUC":AUC_test, "TP":TP_test, "FP":FP_test, "TN":TN_test, "FN":FN_test}    
        
        # save
        pickle.dump(metrics, open(save_metrics + "test_metrics_" + f[f.find("refine_hp")+11:-6] + ".bin", "wb"))
    
    


