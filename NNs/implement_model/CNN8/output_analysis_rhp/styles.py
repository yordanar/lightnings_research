# markers

def return_marker(lr):
    if lr == "-1":
        marker = "*"
    elif lr == "-2":
        marker = "d"
    elif lr == "-3":
        marker = "o"
    elif lr == "-4":
        marker = "X"
    elif lr == "-5":
        marker = "^"
    elif lr == "-6":
        marker = "p"

    return marker

def return_color(vHP, HP):
        
    if HP == "KNS":
        if vHP == "48_96":
            Color = "k"
        elif vHP == "16_32":
            Color = "b"
        elif vHP == "32_64":
            Color = "r"


    elif HP == "RP":
        if vHP == "0.002":
            Color = "#641e16"
        elif vHP == "0.003":
            Color = "#a93226"
        elif vHP == "0.004":
            Color = "#d35400"
        elif vHP == "0.005":
            Color = "#e67e22"
        elif vHP == "0.006":
            Color = "#f5b041"
        elif vHP == "0.007":
            Color = "#f1c40f"
        elif vHP == "0.008":
            Color = "#58d68d"
        elif vHP == "0.009":
            Color = "#27ae60"
        elif vHP == "0.02":
            Color = "#27ae9e"
        elif vHP == "0.03":
            Color = "#2789ae"
        elif vHP == "0.04":
            Color = "#165cdf"
        elif vHP == "0.05":
            Color = "#5016df"
        elif vHP == "0.06":
            Color = "#8259e8"
        elif vHP == "0.07":
            Color = "#ad24ee"
        elif vHP == "0.08":
            Color = "#ee24e2"
        elif vHP == "0.09":
            Color = "#ffa2f9"
        

    elif HP == "BS":
        if vHP == "64":
            Color = "b"
        elif vHP == "128":
            Color = "r"
        elif vHP == "32":
            Color = "g"


    elif HP == "LRD":
        if vHP == "60":
            Color = "b"
        elif vHP == "80":
            Color = "r"
        elif vHP == "90":
            Color = "g"
        elif vHP == "95":
            Color = "k"

    return Color
