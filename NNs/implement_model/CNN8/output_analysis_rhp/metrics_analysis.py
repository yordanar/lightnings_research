import glob
import numpy as np
import pickle
import matplotlib.pyplot as plt
from styles import *

# epochs
epochs = 1000

# axis
axis_x = "Precision"
axis_y = "Recall"

# limites de los ejes
limites = {"Precision": [-0.05, 1], "Recall": [-0.05, 1]}

# compare hyperparameters
HP1 = "LR"

# names hyperparameters
legend_hyperp = {"LR": "$10^{",
                "RP" : "",
                "BS" : "",
                "LRD": "",
                "KN1": "",
                "KN2": "", 
                "KNS": ""}
    
legend_hyperp_final_part = {"LR" : "}$",
                            "RP" : "",
                            "BS" : "",
                            "LRD": "",
                            "KN1": "",
                            "KN2": "", 
                            "KNS": ""}
    
# paths
ruta_files = "/mnt/data/storm/lightnings_research/NNs/implement_model/CNN8/refine_hp/"
ruta_save  = "/home/storm/lightnings_research/NNs/implement_model/CNN8/output_analysis_rhp/miscelanea/"

# load test data
test_data = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/TEST_ABI.npy")
test_tags = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/TEST_LIGHTNING_TAGS.npy")

# files list
files = glob.glob(ruta_files + "*.pkl")
files.sort()

for HP2 in ["KNS", "LRD", "BS", "RP"]:

    # Graph results
    Set_V1    = set()
    Set_V2    = set()
    countV1   = 0
    countV2   = 0

    Fig       = plt.figure(figsize=(20,9))
    ax        = Fig.add_axes([0.06 , 0.1, 0.3, 0.75])
    ax2       = Fig.add_axes([0.375, 0.1, 0.3, 0.75])
    ax3       = Fig.add_axes([0.685, 0.1, 0.3, 0.75])

    for contadorcito, f in enumerate(files):

        metrics        = pickle.load(open(f, "rb"))
        model_filename = "test_metrics_" + f[f.find("history_model")+8:-4]

        # test metrics
        test_performance = pickle.load(open(ruta_files + model_filename + ".bin", "rb"))

        # extract a rare index in the name, common to all of the metrics
        metrics_list  = list(metrics.keys())

        # return marker
        auc_str       = metrics_list[0]
        index_metrics = ""
        if len(auc_str) >= 4:
            index_metrics = "_" + auc_str[4:]

        indexes = {}

        # Now extract the metrics
        AUC_train  = np.array(metrics["auc" + index_metrics]            ); AUC_val  = np.array(metrics["val_auc" + index_metrics])            ; AUC_test  = test_performance["AUC"]
        ACC_train  = np.array(metrics["binary_accuracy"]                ); ACC_val  = np.array(metrics["val_binary_accuracy"])                ; ACC_test  = test_performance["ACC"]
        loss_train = np.array(metrics["loss"]                           ); loss_val = np.array(metrics["val_loss"])                           ; loss_test = test_performance["loss"]
        FN_train   = np.array(metrics["false_negatives" + index_metrics]); FN_val   = np.array(metrics["val_false_negatives" + index_metrics]); FN_test   = test_performance["FN"]
        FP_train   = np.array(metrics["false_positives" + index_metrics]); FP_val   = np.array(metrics["val_false_positives" + index_metrics]); FP_test   = test_performance["FP"]
        TN_train   = np.array(metrics["true_negatives"  + index_metrics]); TN_val   = np.array(metrics["val_true_negatives"  + index_metrics]); TN_test   = test_performance["TN"]
        TP_train   = np.array(metrics["true_positives"  + index_metrics]); TP_val   = np.array(metrics["val_true_positives"  + index_metrics]); TP_test   = test_performance["TP"]

        try:
            indexes.update({"AUC_train"  : AUC_train})                               
            indexes.update({"ACC_train"  : ACC_train})                               
            indexes.update({"loss_train" : loss_train})                              
            indexes.update({"Precision_train"   : TP_train / (TP_train + FP_train)}) 
            indexes.update({"Recall_train"      : TP_train / (TP_train + FN_train)}) 
            indexes.update({"Specificity_train" : TN_train / (TN_train + FP_train)}) 
            indexes.update({"FPR_train"         : FP_train / (TN_train + FP_train)}) 
        except:
            pass

        try:
            indexes.update({"AUC_val"  : AUC_val})                           
            indexes.update({"ACC_val"  : ACC_val})                           
            indexes.update({"loss_val" : loss_val})                          
            indexes.update({"Precision_val"   : TP_val / (TP_val + FP_val)}) 
            indexes.update({"Recall_val"      : TP_val / (TP_val + FN_val)}) 
            indexes.update({"Specificity_val" : TN_val / (TN_val + FP_val)}) 
            indexes.update({"FPR_val"         : FP_val / (TN_val + FP_val)})
        except:
            pass
        
        try:
            indexes.update({"AUC_test"  : AUC_test})
            indexes.update({"ACC_test"  : ACC_test})
            indexes.update({"loss_test" : loss_test})
            indexes.update({"Precision_test"  : TP_test / (TP_test + FP_test)})
            indexes.update({"Recall_test"     : TP_test / (TP_test + FN_test)})
            indexes.update({"Specificity_test": TN_test / (TN_test + FP_test)})
            indexes.update({"FPR_test"        : FP_test / (TN_test + FP_test)})
        except:
            pass
        
        # Graph scatterplot from the final epoch
        values_metrics = {"LR" : str(-1*int(f[f.find("lr_")+3:f.find("lrd_")-1])),
                        "RP" : str(int(f[f.find("rp")+3:-4])/1000),
                        "BS" : f[f.find("bs")+3:f.find("lr")-1],
                        "LRD": f[f.find("lrd")+4:f.find("lrd")+6],
                        "KN1": f[f.find("kn1")+4:f.find("kn2")-1], 
                        "KN2": f[f.find("kn2")+4:f.find("bs")-1], 
                        "KNS": f[f.find("kn1")+4:f.find("kn2")-1] + "_" + f[f.find("kn2")+4:f.find("bs")-1]}
        
        V1             = values_metrics[HP1]
        V2             = values_metrics[HP2]

        Set_V1.add(V1)
        Set_V2.add(V2)
        
        Marker       = return_marker(V1)
        Color        = return_color(V2, HP2)
        
        if len(Set_V1) == countV1:
            ax.scatter(indexes[axis_x + "_train"][-1], indexes[axis_y + "_train"][-1], marker=Marker, s=100, alpha=0.3, color=Color)
        else:
            ax.scatter(indexes[axis_x + "_train"][-1], indexes[axis_y + "_train"][-1], marker=Marker, s=100, alpha=0.3, color=Color, label=HP1 + ": " + legend_hyperp[HP1] + V1 + legend_hyperp_final_part[HP1])

        ax2.scatter(indexes[axis_x + "_val"][-1], indexes[axis_y + "_val"][-1], marker=Marker, s=100, alpha=0.3, color=Color)
        if len(Set_V2) != countV2 and V1 == "-2":
            ax2.scatter(indexes[axis_x + "_val"][-1], indexes[axis_y + "_val"][-1], marker=Marker, s=100, alpha=0.3, color=Color, label=HP2 + ": " + legend_hyperp[HP2] + V2 + legend_hyperp_final_part[HP2])
            countV2 = len(Set_V2)
        
        try:       
            ax3.scatter(indexes[axis_x + "_test"], indexes[axis_y + "_test"], marker=Marker, s=100, alpha=0.3, color=Color)
        except:
            pass

        countV1 = len(Set_V1)

    Fig.suptitle(axis_x + " vs " + axis_y + " (epochs: " + str(epochs) + ")\nBinary cross entropy - ADAM", fontsize=20)
    ax.set_title('Training subset', fontsize=21)
    ax2.set_title('Validation subset', fontsize=21)
    ax3.set_title('Test subset', fontsize=21)

    ax.tick_params(axis='both', which='major', labelsize=21)
    ax2.tick_params(axis='both', which='major', labelsize=21)
    ax3.tick_params(axis='both', which='major', labelsize=21)

    ax.grid(True)
    ax2.grid(True)
    ax3.grid(True)

    ax2.set_yticklabels([])
    ax3.set_yticklabels([])

    ax.set_xlim(limites[axis_x][0], limites[axis_x][1])
    ax.set_ylim(limites[axis_y][0], limites[axis_y][1])
    ax2.set_xlim(limites[axis_x][0], limites[axis_x][1])
    ax2.set_ylim(limites[axis_y][0], limites[axis_y][1])
    ax3.set_xlim(limites[axis_x][0], limites[axis_x][1])
    ax3.set_ylim(limites[axis_y][0], limites[axis_y][1])

    ax.set_xlabel(axis_x, fontsize=21)
    ax.set_ylabel(axis_y, fontsize=21)
    ax2.set_xlabel(axis_x, fontsize=21)
    ax3.set_xlabel(axis_x, fontsize=21)

    ax.legend(loc="upper left", prop={'size': 21}, ncol=2)
    ax2.legend(loc="upper center", prop={'size': 21}, ncol=2)

    plt.savefig(ruta_save + axis_x + "_" + axis_y + "_HP1_" + HP1 + "_HP2_" + HP2 +".png")
    plt.close("all")
