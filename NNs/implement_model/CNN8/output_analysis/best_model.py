import glob
import numpy as np
import pickle
import matplotlib.pyplot as plt
import pandas as pd
from lightnings_research.NNs.implement_model.CNN8.output_analysis.styles_final_epoch import *

   
# paths
ruta_files = "/mnt/data/storm/lightnings_research/NNs/implement_model/CNN8/"

# files list
files = glob.glob(ruta_files + "*.pkl")
files.sort()


AUC_train         = []; AUC_val         = []
ACC_train         = []; ACC_val         = []
loss_train        = []; loss_val        = []
Precision_train   = []; Precision_val   = []
Recall_train      = []; Recall_val      = []
Specificity_train = []; Specificity_val = []
FPR_train         = []; FPR_val         = []


for f in files:
    
    metrics       = pickle.load(open(f, "rb"))
        
    # extract a rare index in the name, common to all of the metrics
    metrics_list  = list(metrics.keys())

    # return marker
    auc_str       = metrics_list[0]
    index_metrics = ""
    if len(auc_str) >= 4:
        index_metrics = "_" + auc_str[4:]


    # Now extract the metrics
    
    FN_train   = np.array(metrics["false_negatives" + index_metrics]); FN_val   = np.array(metrics["val_false_negatives" + index_metrics])
    FP_train   = np.array(metrics["false_positives" + index_metrics]); FP_val   = np.array(metrics["val_false_positives" + index_metrics])
    TN_train   = np.array(metrics["true_negatives"  + index_metrics]); TN_val   = np.array(metrics["val_true_negatives"  + index_metrics])
    TP_train   = np.array(metrics["true_positives"  + index_metrics]); TP_val   = np.array(metrics["val_true_positives"  + index_metrics])
    
    AUC_train .append(np.array(metrics["auc" + index_metrics]            )[-1]); AUC_val .append(np.array(metrics["val_auc" + index_metrics])[-1])
    ACC_train .append(np.array(metrics["binary_accuracy"]                )[-1]); ACC_val .append(np.array(metrics["val_binary_accuracy"])[-1])
    loss_train.append(np.array(metrics["loss"]                           )[-1]); loss_val.append(np.array(metrics["val_loss"])[-1])

    # Compute performance indexes
    Precision_train  .append((TP_train / (TP_train + FP_train))[-1])     ; Precision_val  .append((TP_val / (TP_val + FP_val))[-1])
    Recall_train     .append((TP_train / (TP_train + FN_train))[-1])     ; Recall_val     .append((TP_val / (TP_val + FN_val))[-1])
    Specificity_train.append((TN_train / (TN_train + FP_train))[-1])     ; Specificity_val.append((TN_val / (TN_val + FP_val))[-1])
    FPR_train        .append((FP_train / (TN_train + FP_train))[-1])     ; FPR_val        .append((FP_val / (TN_val + FP_val))[-1])

# creates a data frame with the data

metrics_train = pd.DataFrame(data={"file": files, "Precision_train": Precision_train, "Recall_train":Recall_train, "Specificity_train": Specificity_train, 
                                   "FPR_train": FPR_train, "AUC_train": AUC_train, "ACC_train":ACC_train, "loss_train": loss_train})

metrics_val   = pd.DataFrame(data={"file": files, "Precision_val": Precision_val, "Recall_val":Recall_val, "Specificity_val": Specificity_val, 
                                   "FPR_val": FPR_val, "AUC_val": AUC_val, "ACC_val":ACC_val, "loss_val": loss_val})

