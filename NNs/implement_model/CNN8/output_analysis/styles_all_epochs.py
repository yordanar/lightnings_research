# markers

def return_marker(lr, HP):
    
    if HP == "LR":
        if lr == "-1":
            marker = "solid"
        elif lr == "-2":
            marker = "dashed"
        elif lr == "-3":
            marker = "dotted"
        elif lr == "-4":
            marker = (0, (1, 10))

    elif HP == "BS":
        if lr == "32":
            marker = "solid"
        elif lr == "64":
            marker = "dashed"
        elif lr == "128":
            marker = "dotted"
        elif lr == "95":
            marker = (0, (1, 10))

    return marker

def return_color(vHP, HP):
        
    if HP == "KNS":
        if vHP == "48_96":
            Color = "k"
        elif vHP == "16_32":
            Color = "b"
        elif vHP == "32_64":
            Color = "r"


    elif HP == "RP":
        if vHP == "-1":
            Color = "k"
        elif vHP == "-2":
            Color = "b"
        elif vHP == "-3":
            Color = "r"
        elif vHP == "-4":
            Color = "g"


    elif HP == "BS":
        if vHP == "64":
            Color = "b"
        elif vHP == "128":
            Color = "r"
        elif vHP == "32":
            Color = "g"


    elif HP == "LRD":
        if vHP == "60":
            Color = "b"
        elif vHP == "80":
            Color = "r"
        elif vHP == "90":
            Color = "g"
        elif vHP == "95":
            Color = "k"

    return Color
