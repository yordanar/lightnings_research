import glob
import numpy as np
import pickle
import matplotlib.pyplot as plt
from styles_all_epochs import *

# compare hyperparameters
HP1 = "BS"
HP2 = "RP"

YLIM   = {"loss": {"-1":0.2, "-2":0.2, "-3":0.2, "-4":50}, "Recall": {"-1":1, "-2":1, "-3":1, "-4":1}, "Precision":{"-1":1, "-2":1, "-3":1, "-4":1}, "FPR": {"-1":1, "-2":1, "-3":1, "-4":1}, "AUC": {"-1":1, "-2":1, "-3":1, "-4":1}}
epochs = 3000 

# names hyperparameters
legend_hyperp = {"LR": "$10^{",
                "RP" : "$10^{",
                "BS" : "",
                "LRD": "",
                "KN1": "",
                "KN2": "", 
                "KNS": ""}
    
legend_hyperp_final_part = {"LR" : "}$",
                            "RP" : "}$",
                            "BS" : "",
                            "LRD": "",
                            "KN1": "",
                            "KN2": "", 
                            "KNS": ""}

# paths
ruta_files = "/mnt/data/storm/lightnings_research/NNs/implement_model/CNN8/"

# files list
files = glob.glob(ruta_files + "*.pkl")
files.sort()


for axis_y in ["loss", "AUC", "FPR", "Recall", "Precision"]:

    for HP2 in ["KNS", "LRD", "RP", "BS"]:
    
        for LRselected in [1, 2, 3, 4]:  # 10^(-LRselected)

            ruta_save  = "/home/storm/lightnings_research/NNs/implement_model/CNN8/output_analysis/todo/" + axis_y + "/"

            # Graph results
            Set_V2     = set()
            Set_V1     = set()
            countV1    = 0
            countV2    = 0

            Fig       = plt.figure(figsize=(13,9))
            ax        = Fig.add_axes([0.07, 0.1, 0.4, 0.75])
            ax2       = Fig.add_axes([0.57, 0.1, 0.4, 0.75])

            for f in files:
                

                # Graph scatterplot from the final epoch
                values_metrics = {"LR" : str(-1*int(f[f.find("lr_")+3:f.find("lrd_")-1])),
                                "RP" : str(-1*int(f[f.find("rp")+3:f.find("rp")+4])),
                                "BS" : f[f.find("bs")+3:f.find("lr")-1],
                                "LRD": f[f.find("lrd")+4:f.find("lrd")+6],
                                "KN1": f[f.find("kn1")+4:f.find("kn2")-1], 
                                "KN2": f[f.find("kn2")+4:f.find("bs")-1], 
                                "KNS": f[f.find("kn1")+4:f.find("kn2")-1] + "_" + f[f.find("kn2")+4:f.find("bs")-1]}
                    
                LR             = values_metrics["LR"]

                if int(LR) == -1*LRselected:
                    
                    metrics       = pickle.load(open(f, "rb"))
                        
                    # extract a rare index in the name, common to all of the metrics
                    metrics_list  = list(metrics.keys())

                    # return marker
                    auc_str       = metrics_list[0]
                    index_metrics = ""
                    if len(auc_str) >= 4:
                        index_metrics = "_" + auc_str[4:]

                    indexes = {}

                    # Now extract the metrics
                    AUC_train  = np.array(metrics["auc" + index_metrics]            ); AUC_val  = np.array(metrics["val_auc" + index_metrics])
                    ACC_train  = np.array(metrics["binary_accuracy"]                ); ACC_val  = np.array(metrics["val_binary_accuracy"])
                    loss_train = np.array(metrics["loss"]                           ); loss_val = np.array(metrics["val_loss"])
                    FN_train   = np.array(metrics["false_negatives" + index_metrics]); FN_val   = np.array(metrics["val_false_negatives" + index_metrics])
                    FP_train   = np.array(metrics["false_positives" + index_metrics]); FP_val   = np.array(metrics["val_false_positives" + index_metrics])
                    TN_train   = np.array(metrics["true_negatives"  + index_metrics]); TN_val   = np.array(metrics["val_true_negatives"  + index_metrics])
                    TP_train   = np.array(metrics["true_positives"  + index_metrics]); TP_val   = np.array(metrics["val_true_positives"  + index_metrics])

                    indexes.update({"AUC_train"  : AUC_train}) ; indexes.update({"AUC_val"  : AUC_val})
                    indexes.update({"ACC_train"  : ACC_train}) ; indexes.update({"ACC_val"  : ACC_val})
                    indexes.update({"loss_train" : loss_train}); indexes.update({"loss_val" : loss_val})
                    indexes.update({"FN_train"   : FN_train})  ; indexes.update({"FN_val"   : FN_val})
                    indexes.update({"FP_train"   : FP_train})  ; indexes.update({"FP_val"   : FP_val})
                    indexes.update({"TN_train"   : TN_train})  ; indexes.update({"TN_val"   : TN_val})
                    indexes.update({"TP_train"   : TP_train})  ; indexes.update({"TP_val"   : TP_val})


                    # Compute performance indexes
                    indexes.update({"Precision_train"   : TP_train / (TP_train + FP_train)})     ; indexes.update({"Precision_val"   : TP_val / (TP_val + FP_val)})
                    indexes.update({"Recall_train"      : TP_train / (TP_train + FN_train)})     ; indexes.update({"Recall_val"      : TP_val / (TP_val + FN_val)})
                    indexes.update({"Specificity_train" : TN_train / (TN_train + FP_train)})     ; indexes.update({"Specificity_val" : TN_val / (TN_val + FP_val)})
                    indexes.update({"FPR_train"         : FP_train / (TN_train + FP_train)})     ; indexes.update({"FPR_val"         : FP_val / (TN_val + FP_val)})
    
                    V1             = values_metrics[HP1]
                    V2             = values_metrics[HP2]
                    
                    Marker       = return_marker(V1, HP1)
                    Color        = return_color(V2, HP2)

                    Set_V1.add(V1)
                    Set_V2.add(V2)

                    if len(Set_V1) == countV1:
                        ax.plot(np.arange(epochs), indexes[axis_y + "_train"][:], alpha=0.5, color=Color, linestyle=Marker)
                    else:
                        ax.plot(np.arange(epochs), indexes[axis_y + "_train"][:], alpha=0.5, color=Color, linestyle=Marker, label=HP1 + ": " + legend_hyperp[HP1] + V1 + legend_hyperp_final_part[HP1])

                    ax2.plot(np.arange(epochs), indexes[axis_y + "_val"][:], alpha=0.5, color=Color, linestyle=Marker)
                    if len(Set_V2) != countV2:
                        ax2.plot(np.arange(epochs), indexes[axis_y + "_val"][:], alpha=0.5, color=Color, linestyle=Marker, label=HP2 + ": " + legend_hyperp[HP2] + V2 + legend_hyperp_final_part[HP2])
                        countV2 =   len(Set_V2)
                        
                    countV1 = len(Set_V1)
                        

            Fig.suptitle(axis_y + " (epochs: " + str(epochs) + ") - LR = 10$^{-" + str(LRselected) + "}$\nBinary cross entropy - ADAM", fontsize=20)
            
            ax.set_title('Training subset', fontsize=17)
            ax2.set_title('Validation subset', fontsize=17)

            ax.tick_params(axis='both', which='major', labelsize=17)
            ax2.tick_params(axis='both', which='major', labelsize=17)

            ax.set_xlabel("epoch", fontsize=17)
            ax.set_ylabel(axis_y, fontsize=17)
            ax2.set_xlabel("epoch", fontsize=17)

            ax.legend(loc="center right", prop={'size': 17}, ncol=1)
            ax2.legend(loc="center right", prop={'size': 17}, ncol=1)

            ax.set_ylim(-0.02, YLIM[axis_y][str(-1*LRselected)])
            ax2.set_ylim(-0.02, YLIM[axis_y][str(-1*LRselected)])

            plt.savefig(ruta_save + axis_y + "_LR_" + str(LRselected) + "_HP1_" + HP1 + "_HP2_" + HP2 + ".png")

            plt.close("all")
