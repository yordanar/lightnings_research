import numpy as np
import pandas as pd
import netCDF4 as nc
import pickle
import geopandas as gpd
from shapely.geometry import Point
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.feature import ShapelyFeature
from cartopy.io.shapereader import Reader
import cartopy.feature as cfeature
import sys
sys.path.insert(1, '/home/storm/lightnings_research/archivos_data')
from informacion_zonas import limites_zonas
from obtiene_fr_o_bt import *
import tensorflow as tf


# Verificar si TensorFlow detecta la GPU
physical_devices = tf.config.list_physical_devices('GPU')
print("Num GPUs Available: ", len(physical_devices))

if len(physical_devices) > 0:
    #tf.config.experimental.set_memory_growth(physical_devices[0], True)
    tf.config.experimental.set_visible_devices(physical_devices[1], 'GPU')
    print("TensorFlow está usando la GPU")
else:
    print("TensorFlow no detecta ninguna GPU")


th      = 0.5
ylimits = {"Recall": [40, 100], "Accuracy": [95, 100], "Specificity": [95, 100], "Precision": [40, 100]}

# paths
ruta_files           = "/mnt/data/storm/lightnings_research/NNs/implement_model/CNN8/refine_hp/"
shape_va             = "/home/storm/Shapes/Amva.shp"
shape_cen            = "/home/storm/Shapes/med_cen.shp"
shape_occ            = "/home/storm/Shapes/med_occ.shp"
shape_ori            = "/home/storm/Shapes/med_occ.shp"
ruta_coords          = "/home/storm/lightnings_research/NNs/data/OR_ABI-L1b-RadF-M6C13_G16_s20220161110208_e20220161119527_c20220161119594.nc"
save_figures         = "/home/storm/lightnings_research/NNs/implement_model/CNN8/best_model/analysis_with_inbalanced_data/images/"

# coordinates
boundaries_region_2k = limites_zonas()["2.0km"]["Valle_de_Aburra"]
lons, lats, _        = obtiene_fr_o_bt(boundaries_region_2k, [ruta_coords], '13')
lon_min              = np.min(lons)
lat_min              = np.min(lats)
lon_max              = np.max(lons)
lat_max              = np.max(lats)

# load test data
test_abi        = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/TEST_ABI.npy")
test_tags       = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/TEST_LIGHTNING_TAGS.npy")
test_abi_inb    = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/inbalanced_data/TEST_ABI.npy")
test_tags_inb   = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/inbalanced_data/TEST_LIGHTNING_TAGS.npy")
fechas_test     = pickle.load(open("/home/storm/lightnings_research/NNs/data/balanced_data6/FECHAS_TEST.bin", "rb"))
fechas_test_inb = pickle.load(open("/home/storm/lightnings_research/NNs/data/balanced_data6/inbalanced_data/FECHAS_TEST.bin", "rb"))

# load val data
val_abi        = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/VAL_ABI.npy")
val_tags       = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/VAL_LIGHTNING_TAGS.npy")
val_abi_inb    = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/inbalanced_data/VAL_ABI.npy")
val_tags_inb   = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/inbalanced_data/VAL_LIGHTNING_TAGS.npy")
fechas_val     = pickle.load(open("/home/storm/lightnings_research/NNs/data/balanced_data6/FECHAS_VAL.bin", "rb"))
fechas_val_inb = pickle.load(open("/home/storm/lightnings_research/NNs/data/balanced_data6/inbalanced_data/FECHAS_VAL.bin", "rb"))

# se usa train pero de datos imbalanceadios que no fueron usados para entrenar el modelo que aquí se está evaluando
train_abi_inb    = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/inbalanced_data/TRAIN_ABI.npy")
train_tags_inb   = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/inbalanced_data/TRAIN_LIGHTNING_TAGS.npy")
fechas_train_inb = pickle.load(open("/home/storm/lightnings_research/NNs/data/balanced_data6/inbalanced_data/FECHAS_TRAIN.bin", "rb"))

# optionals
title        = {"val": "Validation subset", "test": "Testing subset", "all": "All data"}

fechas_data  = pd.concat([pd.Series(fechas_test), pd.Series(fechas_val), pd.Series(fechas_test_inb), pd.Series(fechas_val_inb), pd.Series(fechas_train_inb)]) 
abi_data     = np.concatenate([test_abi, val_abi, test_abi_inb, val_abi_inb, train_abi_inb], axis=0)
tags         = np.concatenate([test_tags, val_tags, test_tags_inb, val_tags_inb, train_tags_inb], axis=0)


# files list
f            = "/mnt/data/storm/lightnings_research/NNs/implement_model/CNN8/refine_hp/model_kn1_32_kn2_64_bs_32_lr_2_lrd_80_rp_3.keras" 

# load model
loaded_model = tf.keras.models.load_model(f)

# do the predictions
predictions    = loaded_model.predict(abi_data)

# mask of predictions
mask_pred                  = predictions.copy()
mask_pred[mask_pred >= th] = 1
mask_pred[mask_pred <  th] = 0

# compute TN, FP, FN, TP positions
TP_pos = np.where((mask_pred == 1) & (tags == 1))
TN_pos = np.where((mask_pred == 0) & (tags == 0))
FP_pos = np.where((mask_pred == 1) & (tags == 0))
FN_pos = np.where((mask_pred == 0) & (tags == 1))

# build matrices of TP, TN, FP, FN
TP = np.zeros(tags.shape)
TN = np.zeros(tags.shape)
FP = np.zeros(tags.shape)
FN = np.zeros(tags.shape)

# pone uno donde se dan las metricas
TP[TP_pos[0], TP_pos[1]] = 1
TN[TN_pos[0], TN_pos[1]] = 1
FP[FP_pos[0], FP_pos[1]] = 1
FN[FN_pos[0], FN_pos[1]] = 1

# compute the metric b []y point in the map
TP = np.sum(TP, axis=0)
TN = np.sum(TN, axis=0)
FP = np.sum(FP, axis=0)
FN = np.sum(FN, axis=0)

# Compute metrics
Recall      = TP / (TP + FN) * 100
Precision   = TP / (TP + FP) * 100
Specificity = TN / (TN + FP) * 100
Accuracy    = (TP+TN)/(TP + TN + FP + FN) * 100

#Leyendo el shape del area AMVA
polygons = gpd.GeoDataFrame.from_file(shape_va)
crs      = None

#Creando puntos en geopandas
Lat_comp =  lats.reshape(lats.shape[0]*lats.shape[1])
Lon_comp =  lons.reshape(lons.shape[0]*lons.shape[1])

Points   = gpd.GeoDataFrame({'Lat': Lat_comp, 'Lon': Lon_comp})
geometry = [Point(xy) for xy in zip(Points.Lon, Points.Lat)]
geo_P    = gpd.GeoDataFrame(Points, crs=crs, geometry=geometry)
pts      = geo_P.copy()


performance = {"Barbosa":{"Recall":[], "Precision":[], "Specificity":[], "Accuracy":[], "Lat":[], "Lon":[]}, 
            "Girardota":{"Recall":[], "Precision":[], "Specificity":[], "Accuracy":[], "Lat":[], "Lon":[]}, 
            "Copacabana":{"Recall":[], "Precision":[], "Specificity":[], "Accuracy":[], "Lat":[], "Lon":[]}, 
            "Bello":{"Recall":[], "Precision":[], "Specificity":[], "Accuracy":[], "Lat":[], "Lon":[]}, 
            "Medellín":{"Recall":[], "Precision":[], "Specificity":[], "Accuracy":[], "Lat":[], "Lon":[]}, 
            "Itagüí":{"Recall":[], "Precision":[], "Specificity":[], "Accuracy":[], "Lat":[], "Lon":[]}, 
            "Envigado":{"Recall":[], "Precision":[], "Specificity":[], "Accuracy":[], "Lat":[], "Lon":[]}, 
            "La Estrella":{"Recall":[], "Precision":[], "Specificity":[], "Accuracy":[], "Lat":[], "Lon":[]}, 
            "Sabaneta":{"Recall":[], "Precision":[], "Specificity":[], "Accuracy":[], "Lat":[], "Lon":[]}, 
            "Caldas":{"Recall":[], "Precision":[], "Specificity":[], "Accuracy":[], "Lat":[], "Lon":[]},
            "AMVA":{"Recall":[], "Precision":[], "Specificity":[], "Accuracy":[]}}

    
for i, poly in polygons.iterrows():
    pts_in_this_poly = []
    for j, pt in pts.iterrows():
        if poly.geometry.contains(pt.geometry):
            pts_in_this_poly.append(pt.geometry)
            pts = pts.drop([j])
            performance[poly["NOMBRE"]]["Recall"].append(Recall[j])          ; performance["AMVA"]["Recall"].append(Recall[j])
            performance[poly["NOMBRE"]]["Specificity"].append(Specificity[j]); performance["AMVA"]["Specificity"].append(Specificity[j])
            performance[poly["NOMBRE"]]["Precision"].append(Precision[j])    ; performance["AMVA"]["Precision"].append(Precision[j])
            performance[poly["NOMBRE"]]["Accuracy"].append(Accuracy[j])      ; performance["AMVA"]["Accuracy"].append(Accuracy[j])
            performance[poly["NOMBRE"]]["Lat"].append(pt.Lat)
            performance[poly["NOMBRE"]]["Lon"].append(pt.Lon)
                
                
# pinta
for m, c in zip(["Recall", "Specificity", "Precision", "Accuracy"], ["k", "r", "g", "b"]):

    metric = [] 
    names  = []

    for i, poly in polygons.iterrows():
    
        metric.append(np.mean(performance[poly["NOMBRE"]][m]))
        names.append(poly["NOMBRE"])
    
    
    figura = plt.figure(figsize=(9, 5))
    ax     = figura.add_axes([0.1, 0.1, 0.8, 0.8])
    
    ax.bar(np.arange(len(names)), metric, width=0.8, color=c)
    
    # Adding labels and title
    ax.set_ylabel(m + " [%]")
    ax.set_title(m)
        
    # Customizing x-axis ticks to show category names
    ax.set_xticks(np.arange(len(names)))
    ax.set_xticklabels(names, rotation=45, ha='right')
    
    # ylim
    ax.set_ylim(-5, 105)
    
    # grid
    ax.grid(True, which='both', axis='y', color='gray', linestyle='--', linewidth=0.5)
    
    # save
    plt.savefig(save_figures + "municipios_" + m + ".png", dpi=200, bbox_inches='tight', pad_inches=0)
    plt.close("all")
    
