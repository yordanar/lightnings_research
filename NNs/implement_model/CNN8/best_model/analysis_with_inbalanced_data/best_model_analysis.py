import numpy as np
import netCDF4 as nc
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.feature import ShapelyFeature
from cartopy.io.shapereader import Reader
import cartopy.feature as cfeature
import sys
sys.path.insert(1, '/home/storm/lightnings_research/archivos_data')
from informacion_zonas import limites_zonas
from obtiene_fr_o_bt import *
import tensorflow as tf


# Verificar si TensorFlow detecta la GPU
physical_devices = tf.config.list_physical_devices('GPU')
print("Num GPUs Available: ", len(physical_devices))

if len(physical_devices) > 0:
    #tf.config.experimental.set_memory_growth(physical_devices[0], True)
    tf.config.experimental.set_visible_devices(physical_devices[0], 'GPU')
    print("TensorFlow está usando la GPU")
else:
    print("TensorFlow no detecta ninguna GPU")


th      = 0.5
ylimits = {"Recall": [40, 100], "Accuracy": [95, 100], "Specificity": [95, 100], "Precision": [40, 100]}

# paths
ruta_files           = "/mnt/data/storm/lightnings_research/NNs/implement_model/CNN8/refine_hp/"
shape_va             = "/home/storm/Shapes/Amva.shp"
ruta_coords          = "/home/storm/lightnings_research/NNs/data/OR_ABI-L1b-RadF-M6C13_G16_s20220161110208_e20220161119527_c20220161119594.nc"
save_figures         = "/home/storm/lightnings_research/NNs/implement_model/CNN8/best_model/analysis_with_inbalanced_data/images/"

# coordinates
boundaries_region_2k = limites_zonas()["2.0km"]["Valle_de_Aburra"]
lons, lats, _        = obtiene_fr_o_bt(boundaries_region_2k, [ruta_coords], '13')
lon_min              = np.min(lons)
lat_min              = np.min(lats)
lon_max              = np.max(lons)
lat_max              = np.max(lats)

# load test data
test_abi      = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/TEST_ABI.npy")
test_tags     = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/TEST_LIGHTNING_TAGS.npy")
test_abi_inb  = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/inbalanced_data/TEST_ABI.npy")
test_tags_inb = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/inbalanced_data/TEST_LIGHTNING_TAGS.npy")

# load test data
val_abi      = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/VAL_ABI.npy")
val_tags     = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/VAL_LIGHTNING_TAGS.npy")
val_abi_inb  = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/inbalanced_data/VAL_ABI.npy")
val_tags_inb = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/inbalanced_data/VAL_LIGHTNING_TAGS.npy")

# se usa train pero de datos imbalanceadios que no fueron usados para entrenar el modelo que aquí se está evaluando
train_abi_inb = np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/inbalanced_data/TRAIN_ABI.npy")
train_tags_inb= np.load("/home/storm/lightnings_research/NNs/data/balanced_data6/inbalanced_data/TRAIN_LIGHTNING_TAGS.npy")


# optionals
data         = {"test_abi": test_abi, "test_tags": test_tags, "val_abi": val_abi, "val_tags": val_tags, 
                "all_tags":np.concatenate([test_tags, val_tags, test_tags_inb, val_tags_inb, train_tags_inb], axis=0), "all_abi":np.concatenate([test_abi, val_abi, test_abi_inb, val_abi_inb, train_abi_inb], axis=0)}
title        = {"val": "Validation subset", "test": "Testing subset", "all": "All data"}



# files list
f            = "/mnt/data/storm/lightnings_research/NNs/implement_model/CNN8/refine_hp/model_kn1_32_kn2_64_bs_32_lr_2_lrd_80_rp_3.keras" 

# load model
loaded_model = tf.keras.models.load_model(f)

#for Type in ["val", "test", "all"]:
for Type in ["all"]:

    abi_data = data[Type + "_abi"]
    tags     = data[Type + "_tags"]

    # do the predictions
    predictions    = loaded_model.predict(abi_data)

    # mask of predictions
    mask_pred                  = predictions.copy()
    mask_pred[mask_pred >= th] = 1
    mask_pred[mask_pred <  th] = 0

    # compute TN, FP, FN, TP positions
    TP_pos = np.where((mask_pred == 1) & (tags == 1))
    TN_pos = np.where((mask_pred == 0) & (tags == 0))
    FP_pos = np.where((mask_pred == 1) & (tags == 0))
    FN_pos = np.where((mask_pred == 0) & (tags == 1))

    # build matrices of TP, TN, FP, FN
    TP = np.zeros(tags.shape)
    TN = np.zeros(tags.shape)
    FP = np.zeros(tags.shape)
    FN = np.zeros(tags.shape)

    # pone uno donde se dan las metricas
    TP[TP_pos[0], TP_pos[1]] = 1
    TN[TN_pos[0], TN_pos[1]] = 1
    FP[FP_pos[0], FP_pos[1]] = 1
    FN[FN_pos[0], FN_pos[1]] = 1

    # compute the metric by point in the map
    TP = np.sum(TP, axis=0)
    TN = np.sum(TN, axis=0)
    FP = np.sum(FP, axis=0)
    FN = np.sum(FN, axis=0)

    # Compute metrics
    Recall      = TP / (TP + FN) * 100
    Precision   = TP / (TP + FP) * 100
    Specificity = TN / (TN + FP) * 100
    Accuracy    = (TP+TN)/(TP + TN + FP + FN) * 100

    # Reshape the metrics
    Recall      = Recall.reshape([abi_data.shape[1], abi_data.shape[2]])
    Precision   = Precision.reshape([abi_data.shape[1], abi_data.shape[2]])
    Specificity = Specificity.reshape([abi_data.shape[1], abi_data.shape[2]])
    Accuracy    = Accuracy.reshape([abi_data.shape[1], abi_data.shape[2]])
    
    # metrics
    metrics = {"Recall": Recall, "Accuracy": Accuracy, "Specificity": Specificity, "Precision": Precision}
    
    # pinta
    
    for metric in ["Recall", "Accuracy", "Specificity", "Precision"]:
    
        plt.close('all')
        fig = plt.figure(figsize=(12, 10)) # , facecolor='none',frameon=False)
        ax1 = fig.add_axes([0, 0, 1, 1], projection=ccrs.Mercator())
        gl  = ax1.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=0.6, color='k', linestyle='--', xlocs=np.arange(round(lon_min, 1), lon_max, 0.1), ylocs=np.arange(round(lat_min, 1), lat_max, 0.1))
        gl.ylabel_style = {'size':22}
        gl.xlabel_style = {'size':22}
        gl.top_labels   = False
        gl.right_labels = False

        ax1.set_extent([lon_min, lon_max, lat_min, lat_max], ccrs.PlateCarree())

        shape_feature3= ShapelyFeature(Reader(shape_va).geometries(), ccrs.PlateCarree(), facecolor='none', edgecolor ='k')
        ax1.add_feature(shape_feature3, edgecolor='k', linewidth=1.)

        VMAX   = ylimits[metric][1]
        VMIN   = ylimits[metric][0]
        bounds = np.linspace(VMIN, VMAX, 11)

        P = ax1.pcolormesh(lons, lats, metrics[metric], cmap="jet", transform=ccrs.PlateCarree(), vmin=VMIN, vmax=VMAX)
        cb = plt.colorbar(P, ax=ax1, shrink=0.95, pad=0.015, extend="both", ticks=bounds) # , format='%1.0f')
        cb.ax.set_yticklabels(bounds)
        cb.ax.tick_params(labelsize=22)
        cb.ax.set_ylabel(u"[%]", size=22)

        ax1.set_title(metric + " - " + title[Type], fontsize=22) #, color='#616a6b') #, x=0.7, y=0.04)

        plt.savefig(save_figures + Type + "_" + metric + "_spatial.png", dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)
        plt.close('all')