import numpy as np
import pandas as pd
import datetime as dt
from dateutil.relativedelta import *


fechas_mensuales = pd.date_range("2018-01", "2019-04", freq="M")
#ruta_lightnings  = "/home/storm/lightnings_research/NNs/data/VA_conteo_15m/"
ruta_lightnings  = "/home/storm/lightnings_research/NNs/data/VA_conteo_15m/30min_cumuled/"


for f in fechas_mensuales:
    
    fecha1       = dt.datetime.strptime(f.strftime("%Y-%m-01 00:00"), "%Y-%m-%d %H:%M")
    fecha2       = fecha1 + relativedelta(months=1) - relativedelta(seconds=1)
    fechas_10min = pd.date_range(fecha1, fecha2, freq="15min")
    fechas_10min = fechas_10min[(fechas_10min.hour >= 12) & (fechas_10min.hour <= 23)] 
    fechas_10min = fechas_10min + pd.Timedelta(minutes=6)
    
    count        = np.load(ruta_lightnings + "conteo_descargas_30min_" + f.strftime("%Y%m") + ".npy")    
    #count        = np.load(ruta_lightnings + "count_10m_" + f.strftime("%Y%m") + ".npy")    

    #if len(fechas_10min) != len(count):

    print(fecha1)
    print("Hay = " + str(len(count)) + ", necesarios = " + str(len(fechas_10min)))



