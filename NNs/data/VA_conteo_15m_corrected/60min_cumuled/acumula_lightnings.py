import glob
import numpy as np
import datetime as dt
import pandas as pd
from dateutil.relativedelta import *

year          = 2019
num_10m_cumul = 4 # numero de mapas de freq_ABI minutos que se acumularán
freq_ABI      = 15  # frecuencia en minutos de ABI

# paths
path_files_raw = "/home/storm/lightnings_research/NNs/data/VA_conteo_15m_corrected/"
path_save      = "/home/storm/lightnings_research/NNs/data/VA_conteo_15m_corrected/" + str(num_10m_cumul*freq_ABI) + "min_cumuled/"

# files to analyze
files_analyze = glob.glob(path_files_raw + "count_" + str(freq_ABI) + "m_" + str(year) + "*.npy")
files_analyze.sort()

# acumula los datos
for f1, f2 in zip(files_analyze[1:-1], files_analyze[2:]):
    
    print(f1, f2)
    
    "load the data"
    data1 = np.load(f1)
    data2 = np.load(f2)
    data  = np.concatenate([data1, data2], axis=0)
    
    "cumuling"
    cumul = np.zeros(data1.shape)
    
    for i in range(len(data1)):
        
        cumul[i] = np.sum(data[i:i+num_10m_cumul], axis=0)

    "save process of grid"
    np.save(path_save + "conteo_descargas_" + str(num_10m_cumul*freq_ABI) + "min_" + f1[f1.find(".npy")-6:-4] + ".npy", cumul)



# guarda los acumulados del último archivo
# f1    = files_analyze[-1]
# data1 = np.load(f1)

# print(f1)

# "cumulling"
# cumul = np.zeros([data1.shape[0]-num_10m_cumul+1, data1.shape[1], data1.shape[2]])

# for i in range(data1.shape[0]-num_10m_cumul+1):

#     cumul[i] = np.sum(data1[i:i+num_10m_cumul], axis=0)
    
# "save process of grid"
# np.save(path_save + "conteo_descargas_" + str(num_10m_cumul*freq_ABI) + "min_" + f1[f1.find(".npy")-6:-4] + ".npy", cumul)