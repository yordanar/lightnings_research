import datetime as dt
import numpy as np
import glob
import pandas as pd
import sys
from scipy.interpolate import griddata
sys.path.insert(1, '/home/storm/scripts')
from informacion_zonas import limites_zonas
from obtiene_fr_o_bt import *
import pickle

scan_mode            = "6"
equatorial_lag       = 5 # hay un lag entre que ABI empieza a medir y pasa por colombia. Ese lag va aquí. Escribir el número de minutos
allowed_missed_files = 1
promedio_time        = 30 # . Este es el tiempo que se va a tomar para promediar. Se da en minutos y sólo se pueden múltiplos de 10
time_resolution_ABI  = 10 # en minutos
ruta_save     = "/home/storm/lightnings_research/NNs/data/VA_ABI/ABI_5_6_9_10_13_15_instant_plus_mean_30min/"
region        = "Valle_de_Aburra"
ruta_ABI      = "/mnt/GOES_data/"
band_list     = ['05', '06', '09', '10', '13', '15']
lim05k        = limites_zonas()["0.5km"][region]
lim1k         = limites_zonas()["1.0km"][region]
lim2k         = limites_zonas()["2.0km"][region]
lons_guide, lats_guide, _ = obtiene_fr_o_bt(lim2k, ["/home/storm/lightnings_research/NNs/data/OR_ABI-L1b-RadF-M6C13_G16_s20220851750203_e20220851759522_c20220851759591.nc"], '13')
interp_method = "linear" # metodo de interpolación

fechas    = pd.date_range("2022-05-01", "2022-05-31", freq="D") # hora local

for i, f in enumerate(fechas):

    hora_utc_inicio          = dt.datetime.strptime(f.strftime("%Y-%m-%d 17:00"), "%Y-%m-%d %H:%M")
    hora_utc_inicio_promedio = hora_utc_inicio - dt.timedelta(minutes=promedio_time)
    
    # lista de fechas 10 minutal en las que debería haber un archivo para el día en cuestión
    # recordar que se tomarán datos desde medio día, es decir desde 12:00 M. Luego se suman cinco horas, es decir 17:00 horas.
    # y se crea un lista de fechas desde una hora antes para coger los archivos de media hora antes para hacer los promedios.
    f_ahead     = f + dt.timedelta(days=1)
    fechas10m   = pd.date_range(hora_utc_inicio_promedio.strftime("%Y-%m-%d %H:%M"), f_ahead.strftime("%Y-%m-%d 04:50"), freq=str(time_resolution_ABI)+"min") # sólo vamos a tomar los daatos después de medio día
    fechas10m_2 = pd.date_range(hora_utc_inicio.strftime("%Y-%m-%d %H:%M")         , f_ahead.strftime("%Y-%m-%d 04:50"), freq=str(time_resolution_ABI)+"min") # sólo vamos a tomar los daatos después de medio día

    # crea diccionario donde se guardarán los datos instantaneos
    data_ahora = {}
    # crea dicccionario para informar la existencia de algunos archivos
    dict_files = {}

    for j, ch in enumerate(band_list):

        # extrae límite de cada resolución
        if ch == "02":
            res = "0.5km"
        elif ch in ["01", "03", "05"]:
            res = "1.0km"
        elif ch in ["04", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16"]:
            res = "2.0km"
            
        "extrae limites del mapa"
        lims = limites_zonas()[res][region]




        ################################################
        ################################################  LISTA LOS ARCHIVOS
        ################################################

        # toma los archivos de la fecha f
        files_ch = glob.glob(ruta_ABI + f.strftime("%Y%m%d") + "/OR_ABI-L1b-RadF-M" + str(scan_mode) + "C" + ch + "_G16_s" + f.strftime("%Y%j") + "*.nc")
        files_ch.sort()
        
        # toma los archivos del día siguiente porque los datos de ABI estan en utc y queremos tener los datos en hora local (+5)
        files_ch_ahead = glob.glob(ruta_ABI + f_ahead.strftime("%Y%m%d") + "/OR_ABI-L1b-RadF-M" + str(scan_mode) + "C" + ch + "_G16_s" + f_ahead.strftime("%Y%j") + "*.nc")
        files_ch_ahead.sort()
        
        # junta los archivos
        files_ABI_raw = files_ch + files_ch_ahead
        files_ABI_raw.sort()

        
        FILES_ABI_cut = []
        for fi in files_ABI_raw:
            FILES_ABI_cut.append(fi[fi.find("OR_ABI")+27:fi.find("OR_ABI")+38])


        FILES_ABI_current = [FILES_ABI_cut[0]]
        files_ABI         = [files_ABI_raw[0]]
        for ll, fi in enumerate(FILES_ABI_cut[1:]):
            if fi not in FILES_ABI_current:
                FILES_ABI_current.append(fi)
                files_ABI.append(files_ABI_raw[ll+1])
            

        ################################################
        ################################################  CONFORMA LA SERIE DE ARCHIVOS
        ################################################


        # selecciona los archivos de ABI correspondientes a la hora local
        "extrae primero las fechas de los archivos"
        dates_files = []
        for archivo in files_ABI:
            date = archivo[archivo.find("_G16_s")+6:archivo.find("_G16_s")+17]
            date = dt.datetime.strptime(date, "%Y%j%H%M")
            dates_files.append(date)

        "construye un dataframe"
        series_files = pd.Series(index=pd.DatetimeIndex(dates_files), data=files_ABI)

        "extrae los datos correspondientes a la hora local. Se toma desde las 16:30 del día anterior para poder calcular el promedio de"
        "la última media hora correspondiente a las 17"
        selected_ABI = series_files[hora_utc_inicio_promedio.strftime("%Y-%m-%d %H:%M") : f_ahead.strftime("%Y-%m-%d 04:50")].reindex(fechas10m)
        


        ################################################
        ################################################  EXTRAE LOS DATOS DEL VALLE DE ABURRÁ. ARCHIVOS DAÑADOS O NO 
        ################################################  EXISTENTES SE REPORTAN CON NANS
        ################################################

        "esto se hace porque pueden haber archivos pero estar malos, entonces deberían tener nan"
        archivos_inspeccionados = [] 

        # extrae los datos de cada archivo
        for k, date_selected, file_selected in zip(range(len(selected_ABI)), selected_ABI.index, selected_ABI.values):
            
            if pd.isna(file_selected) == False:
                
                try:
                    lons_sel, lats_sel, data_sel = obtiene_fr_o_bt(lims, [file_selected], ch)
                    
                    if ch in ["01", "02", "03", "05"]:
                        "interpola"
                        shape   = data_sel.shape[0] * data_sel.shape[1]
                        point   = np.transpose(np.array([lons_sel.reshape(shape), lats_sel.reshape(shape)]))
                        data2km = griddata(point, data_sel.reshape(shape), (lons_guide, lats_guide), method=interp_method)
                    else:
                        data2km = data_sel
                    
                    archivos_inspeccionados.append(1)
                        
                except:
                    data2km    = np.zeros(lons_guide.shape)
                    data2km[:] = np.nan
                    
                    archivos_inspeccionados.append(0) # esto significa que el archivo está malo
                    
            else:
                data2km    = np.zeros(lons_guide.shape)
                data2km[:] = np.nan
                
                archivos_inspeccionados.append(0) # esto significa que el archivo no existe
     
            if k == 0:
                data_daily = np.array([data2km])
            else:
                data_daily = np.concatenate([data_daily, np.array([data2km])])

        archivos_inspeccionados = pd.Series(index=pd.DatetimeIndex(selected_ABI.index), data=archivos_inspeccionados)

        # guarda datos instantaneos
        data_ahora.update({ch : data_daily})
        # guarda nombre de los archivos, especialmente porque informa donde no hay archivos
        dict_files.update({ch : archivos_inspeccionados})

    DF_files = pd.DataFrame(data = dict_files)




    ################################################
    ################################################  DETERMINA QUE LOS DATOS NECESARIOS PARA UNA FECHA ESPECÍFICA EXISTE
    ################################################  PONE UN FLAG NEGATIVO SI FALTA ALGÚN DATO
    ################################################


    fechas_con_datos = []
    datos_ready      = {}
    cont             = 0
    
    for l, f10_2 in enumerate(fechas10m_2):
        
        flag        = True
        files_fecha = DF_files.query("index == '" + f10_2.strftime("%Y-%m-%d %H:%M") + "'").values[0]

        if np.all(files_fecha == 1): # aquí se confirma que ninguna de las 6 bandas falte en la hora instantánea
            
            fmean_end = f10_2 - dt.timedelta(minutes=10)
            fmean_ini = f10_2 - dt.timedelta(minutes=promedio_time)

            files_mean = DF_files.query("index <= '" + fmean_end.strftime("%Y-%m-%d %H:%M") + "' & index >= '" + fmean_ini.strftime("%Y-%m-%d %H:%M") + "'")
            
            for j, ch in enumerate(band_list): 
                
                files_mean_ch = files_mean[ch].values
                
                if len(np.where(files_mean_ch == 0)[0]) >= allowed_missed_files + 1: # aquí se verifica que si faltan más de "allowed_missed_files" archivos el flag se torna False
                    flag = False
        else:
            flag = False




        ################################################
        ################################################ SI EL FLAG ES POSITIVO QUIERE DECIR QUE NO FALTA NINGÚN DATO
        ################################################ SE PROCEDE A CALCULAR LOS PROMEDIO Y LUEGO A GUARDAR LOS DATOS
        ################################################

        if flag == True:
            
            for j, ch in enumerate(band_list):
                
                instantly_ch = data_ahora[ch][l + promedio_time // time_resolution_ABI]
                # calcula la media de la última media hora
                last_hour_ch = np.nanmean(data_ahora[ch][l:l + promedio_time // time_resolution_ABI], axis=0)
                
                if cont == 0:
                    datos_ready.update({ch + "_instantaneo" : np.array([instantly_ch])})
                    datos_ready.update({ch + "_mean"        : np.array([last_hour_ch])})
                else:
                    instantly_ch_aux = datos_ready[ch + "_instantaneo"]
                    last_hour_ch_aux = datos_ready[ch + "_mean"]
                
                    datos_ready.update({ch + "_instantaneo" : np.concatenate([instantly_ch_aux, np.array([instantly_ch])], axis=0) })
                    datos_ready.update({ch + "_mean"        : np.concatenate([last_hour_ch_aux, np.array([last_hour_ch])], axis=0) })
                
            cont = cont + 1
                    
            fechas_con_datos.append(f10_2 - dt.timedelta(hours=5) + dt.timedelta(minutes=equatorial_lag))
       
    # save
    
    for j, ch in enumerate(band_list):
        np.save(ruta_save + "instantaneo_" + ch + f.strftime("%Y%m%d") + ".npy", datos_ready[ch + "_instantaneo"])
        np.save(ruta_save + "mean_" + ch + f.strftime("%Y%m%d") + ".npy", datos_ready[ch + "_mean"])
        pickle.dump(fechas_con_datos, open(ruta_save + "fechas_" + f.strftime("%Y%m%d") + ".bin", "wb"))