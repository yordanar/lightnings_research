import numpy as np
import pickle
import pandas as pd


#######################################       #######################################
####################################### PATHS #######################################
#######################################       #######################################

path_save = "/home/storm/lightnings_research/NNs/data/balanced_data6/"

#######################################                      #######################################
####################################### LECTURA DE LOS DATOS #######################################
#######################################                      #######################################

ABI_05 = np.load("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/ABI_05.npy")
ABI_06 = np.load("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/ABI_06.npy")
ABI_08 = np.load("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/ABI_08.npy")
ABI_09 = np.load("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/ABI_09.npy")
ABI_10 = np.load("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/ABI_10.npy")
ABI_13 = np.load("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/ABI_13.npy")

mean_ABI_05 = np.load("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/mean_ABI_05.npy")
mean_ABI_06 = np.load("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/mean_ABI_06.npy")
mean_ABI_08 = np.load("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/mean_ABI_08.npy")
mean_ABI_09 = np.load("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/mean_ABI_09.npy")
mean_ABI_10 = np.load("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/mean_ABI_10.npy")
mean_ABI_13 = np.load("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/mean_ABI_13.npy")

lightnings                 = np.load("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/lightnings.npy")
lightnings[lightnings > 0] = 1

fechas = pd.DatetimeIndex(pickle.load(open("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/fechas.bin", "rb")))


#######################################                                             #######################################
####################################### SELECCIONA LOS INDICES                      #######################################
#######################################                                             #######################################

pos_all            = set(np.arange(len(fechas)))
pos_lightnings     = list(set(np.where(lightnings != 0)[0]))
POS_LIGHTNINGS     = np.where(lightnings != 0)[0]
selected_positions = []
for pl in pos_lightnings:
    selected_lightnings = np.where(POS_LIGHTNINGS == pl)[0]
    if len(selected_lightnings) >= 26:
        selected_positions.append(pl)

#######################################                      #######################################
####################################### SELECCIONA LOS DATOS #######################################
#######################################                      #######################################

ABI_05_select = ABI_05[selected_positions] 
ABI_06_select = ABI_06[selected_positions] 
ABI_08_select = ABI_08[selected_positions] 
ABI_09_select = ABI_09[selected_positions]
ABI_10_select = ABI_10[selected_positions]
ABI_13_select = ABI_13[selected_positions]

mean_ABI_05_select = mean_ABI_05[selected_positions] 
mean_ABI_06_select = mean_ABI_06[selected_positions] 
mean_ABI_08_select = mean_ABI_08[selected_positions] 
mean_ABI_09_select = mean_ABI_09[selected_positions]
mean_ABI_10_select = mean_ABI_10[selected_positions]
mean_ABI_13_select = mean_ABI_13[selected_positions]

lightnings_select  = lightnings[selected_positions]

fechas_select      = fechas[selected_positions]

samples            = len(fechas_select)


#######################################                                             #######################################
####################################### compute statistics of independent variables #######################################
#######################################                                             #######################################

ABI_05_media      = np.mean(     ABI_05_select, axis=0); ABI_05_std      = np.std(     ABI_05_select, axis=0) 
ABI_06_media      = np.mean(     ABI_06_select, axis=0); ABI_06_std      = np.std(     ABI_06_select, axis=0) 
ABI_08_media      = np.mean(     ABI_08_select, axis=0); ABI_08_std      = np.std(     ABI_08_select, axis=0) 
ABI_09_media      = np.mean(     ABI_09_select, axis=0); ABI_09_std      = np.std(     ABI_09_select, axis=0) 
ABI_10_media      = np.mean(     ABI_10_select, axis=0); ABI_10_std      = np.std(     ABI_10_select, axis=0) 
ABI_13_media      = np.mean(     ABI_13_select, axis=0); ABI_13_std      = np.std(     ABI_13_select, axis=0) 
mean_ABI_05_media = np.mean(mean_ABI_05_select, axis=0); mean_ABI_05_std = np.std(mean_ABI_05_select, axis=0) 
mean_ABI_06_media = np.mean(mean_ABI_06_select, axis=0); mean_ABI_06_std = np.std(mean_ABI_06_select, axis=0) 
mean_ABI_08_media = np.mean(mean_ABI_08_select, axis=0); mean_ABI_08_std = np.std(mean_ABI_08_select, axis=0) 
mean_ABI_09_media = np.mean(mean_ABI_09_select, axis=0); mean_ABI_09_std = np.std(mean_ABI_09_select, axis=0) 
mean_ABI_10_media = np.mean(mean_ABI_10_select, axis=0); mean_ABI_10_std = np.std(mean_ABI_10_select, axis=0) 
mean_ABI_13_media = np.mean(mean_ABI_13_select, axis=0); mean_ABI_13_std = np.std(mean_ABI_13_select, axis=0)

#######################################                                             #######################################
#######################################     compute standardized ABI data           #######################################
#######################################                                             #######################################

ABI_05_norm      = (ABI_05_select - ABI_05_media)/ABI_05_std
ABI_06_norm      = (ABI_06_select - ABI_06_media)/ABI_06_std
ABI_08_norm      = (ABI_08_select - ABI_08_media)/ABI_08_std
ABI_09_norm      = (ABI_09_select - ABI_09_media)/ABI_09_std
ABI_10_norm      = (ABI_10_select - ABI_10_media)/ABI_10_std
ABI_13_norm      = (ABI_13_select - ABI_13_media)/ABI_13_std
mean_ABI_05_norm = (mean_ABI_05_select - mean_ABI_05_media)/mean_ABI_05_std
mean_ABI_06_norm = (mean_ABI_06_select - mean_ABI_06_media)/mean_ABI_06_std
mean_ABI_08_norm = (mean_ABI_08_select - mean_ABI_08_media)/mean_ABI_08_std
mean_ABI_09_norm = (mean_ABI_09_select - mean_ABI_09_media)/mean_ABI_09_std
mean_ABI_10_norm = (mean_ABI_10_select - mean_ABI_10_media)/mean_ABI_10_std
mean_ABI_13_norm = (mean_ABI_13_select - mean_ABI_13_media)/mean_ABI_13_std


#######################################                                                        #######################################
####################################### SEPARA LOS DATOS PARA TRAINING, VALIDATION AND TESTING #######################################
#######################################                                                        #######################################

"crea unos indices y los mezcla"
indexes = np.arange(0, samples)
np.random.shuffle(indexes)

"selecciona los datos de training, validación y testing"
train_indexes = indexes[:int(samples*0.8)]                 # 80% de datos para entrenar
val_indexes   = indexes[int(samples*0.8):int(samples*0.9)] # 10% de datos para validar
test_indexes  = indexes[int(samples*0.9):]                 # 10% de datos para testear

ABI_05_train      =        ABI_05_norm[train_indexes];      ABI_05_val =        ABI_05_norm[val_indexes];      ABI_05_test =        ABI_05_norm[test_indexes]
ABI_06_train      =        ABI_06_norm[train_indexes];      ABI_06_val =        ABI_06_norm[val_indexes];      ABI_06_test =        ABI_06_norm[test_indexes]
ABI_08_train      =        ABI_08_norm[train_indexes];      ABI_08_val =        ABI_08_norm[val_indexes];      ABI_08_test =        ABI_08_norm[test_indexes]
ABI_09_train      =        ABI_09_norm[train_indexes];      ABI_09_val =        ABI_09_norm[val_indexes];      ABI_09_test =        ABI_09_norm[test_indexes]
ABI_10_train      =        ABI_10_norm[train_indexes];      ABI_10_val =        ABI_10_norm[val_indexes];      ABI_10_test =        ABI_10_norm[test_indexes]
ABI_13_train      =        ABI_13_norm[train_indexes];      ABI_13_val =        ABI_13_norm[val_indexes];      ABI_13_test =        ABI_13_norm[test_indexes]
mean_ABI_05_train =   mean_ABI_05_norm[train_indexes]; mean_ABI_05_val =   mean_ABI_05_norm[val_indexes]; mean_ABI_05_test =   mean_ABI_05_norm[test_indexes]
mean_ABI_06_train =   mean_ABI_06_norm[train_indexes]; mean_ABI_06_val =   mean_ABI_06_norm[val_indexes]; mean_ABI_06_test =   mean_ABI_06_norm[test_indexes]
mean_ABI_08_train =   mean_ABI_08_norm[train_indexes]; mean_ABI_08_val =   mean_ABI_08_norm[val_indexes]; mean_ABI_08_test =   mean_ABI_08_norm[test_indexes]
mean_ABI_09_train =   mean_ABI_09_norm[train_indexes]; mean_ABI_09_val =   mean_ABI_09_norm[val_indexes]; mean_ABI_09_test =   mean_ABI_09_norm[test_indexes]
mean_ABI_10_train =   mean_ABI_10_norm[train_indexes]; mean_ABI_10_val =   mean_ABI_10_norm[val_indexes]; mean_ABI_10_test =   mean_ABI_10_norm[test_indexes]
mean_ABI_13_train =   mean_ABI_13_norm[train_indexes]; mean_ABI_13_val =   mean_ABI_13_norm[val_indexes]; mean_ABI_13_test =   mean_ABI_13_norm[test_indexes]
lightnings_train  =  lightnings_select[train_indexes];  lightnings_val =  lightnings_select[val_indexes];  lightnings_test =  lightnings_select[test_indexes]
fechas_train      =      fechas_select[train_indexes];      fechas_val =      fechas_select[val_indexes];      fechas_test =      fechas_select[test_indexes]


#######################################                                      #######################################
####################################### ORGANIZA EL SHAPE DE LOS DATOS       #######################################
####################################### ESTANDARIZA LOS DATOS INDEPENDIENTES #######################################


ABI_train        = np.stack([ABI_05_train, ABI_06_train, ABI_08_train, ABI_09_train, ABI_10_train, ABI_13_train, mean_ABI_05_train, mean_ABI_06_train, mean_ABI_08_train, mean_ABI_09_train, mean_ABI_10_train, mean_ABI_13_train], axis=-1)
lightnings_train = lightnings_train.reshape([lightnings_train.shape[0], lightnings_train.shape[1]*lightnings_train.shape[2]])

ABI_val          = np.stack([ABI_05_val, ABI_06_val, ABI_08_val, ABI_09_val, ABI_10_val, ABI_13_val, mean_ABI_05_val, mean_ABI_06_val, mean_ABI_08_val, mean_ABI_09_val, mean_ABI_10_val, mean_ABI_13_val], axis=-1)
lightnings_val   = lightnings_val.reshape([lightnings_val.shape[0], lightnings_val.shape[1]*lightnings_val.shape[2]])

ABI_test        = np.stack([ABI_05_test, ABI_06_test, ABI_08_test, ABI_09_test, ABI_10_test, ABI_13_test, mean_ABI_05_test, mean_ABI_06_test, mean_ABI_08_test, mean_ABI_09_test, mean_ABI_10_test, mean_ABI_13_test], axis=-1)
lightnings_test = lightnings_test.reshape([lightnings_test.shape[0], lightnings_test.shape[1]*lightnings_test.shape[2]])


#######################################               #######################################
####################################### SAVE THE DATA #######################################
#######################################               #######################################

np.save(path_save + "TRAIN_ABI.npy", ABI_train)
np.save(path_save + "VAL_ABI.npy"  , ABI_val)
np.save(path_save + "TEST_ABI.npy" , ABI_test)

np.save(path_save + "TRAIN_LIGHTNING_TAGS.npy", lightnings_train)
np.save(path_save + "VAL_LIGHTNING_TAGS.npy"  , lightnings_val)
np.save(path_save + "TEST_LIGHTNING_TAGS.npy" , lightnings_test)

pickle.dump(fechas_train, open(path_save + "FECHAS_TRAIN.bin", "wb"))
pickle.dump(fechas_val, open(path_save + "FECHAS_VAL.bin", "wb"))
pickle.dump(fechas_test, open(path_save + "FECHAS_TEST.bin", "wb"))

