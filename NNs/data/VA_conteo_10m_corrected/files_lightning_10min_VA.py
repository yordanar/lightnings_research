import glob
import numpy as np
import datetime as dt
import pandas as pd
from dateutil.relativedelta import *
import sys
sys.path.insert(1, '/home/storm/lightnings_research/archivos_data')
from informacion_zonas import limites_zonas
from obtiene_fr_o_bt import *

    

# year
year = 2019

# ruta save
ruta_save = "/home/storm/lightnings_research/NNs/data/VA_conteo_10m_corrected/"

# obtiene las latitudes y longitudes de ABI
ruta = "/home/storm/lightnings_research/NNs/data/OR_ABI-L1b-RadF-M6C13_G16_s20220161110208_e20220161119527_c20220161119594.nc"

boundaries_region_2k = limites_zonas()["2.0km"]["Valle_de_Aburra"]
lons, lats, _ = obtiene_fr_o_bt(boundaries_region_2k, [ruta], '13')

"latitude máxima y mínima donde buscar"
dx = 0.01814
dy = 0.01830
max_lat = np.max(lats) + dy/2; max_lon = np.max(lons) + dx/2
min_lat = np.min(lats) - dy/2; min_lon = np.min(lons) - dx/2

# datos de descargas
ruta_descargas = "/home/storm/lightnings_research/archivos_data/colombia_by_month_calidad/"
for i, anio in enumerate([year, year+1]):
    if i == 0:
        files          = glob.glob(ruta_descargas + "*_" + str(anio) + "*.csv")
    elif len(files) == 12:
            files          = files + glob.glob(ruta_descargas + "*_" + str(anio) + "*.csv")

files.sort()

# extrae los datos y crea los archivos necesarios
for I, f in enumerate(files[3:6]):
    
    print(f)
    
    "lectura del archivo de hoy"
    df         = pd.read_csv(f)
    df['date'] = pd.to_datetime(df['date'])
    
    "lectura del archivo de mañana"
    df_next         = pd.read_csv(files[I+1])
    df_next['date'] = pd.to_datetime(df_next['date'])
    
    "merge the dataframes"
    DF = pd.concat([df, df_next])
    
    "extrae la fecha del nombre del archivo y crea fechas 10 minutales"
    fecha1       = dt.datetime.strptime(f[f.find(".csv")-6:-4], "%Y%m")
    fecha2       = fecha1 + relativedelta(months=1) - relativedelta(seconds=1)
    fechas_10min = pd.date_range(fecha1, fecha2, freq="10min") 
    fechas_10min = fechas_10min[(fechas_10min.hour >= 12) & (fechas_10min.hour <= 23)] 
    fechas_10min = fechas_10min + pd.Timedelta(minutes=5)

    "crea la matriz que se va a llenar"
    grid_conteo     = np.zeros([len(fechas_10min)]+list(lats.shape))
    
    for l, d in enumerate(fechas_10min):

        d1   = d
        d2   = d1 + relativedelta(minutes=10)

        "desecha las fechas que no interesan"
        VA_df_select_fechas     = DF[(DF.date >= d1) & (DF.date < d2)]
        
        if len(VA_df_select_fechas) > 0:
            "desecha los datos por fuera del dominio de interes"
            VA_df               = VA_df_select_fechas.query("latitude <= " + str(max_lat) + " & latitude >= " + str(min_lat) + " & longitude <= " + str(max_lon) + " & longitude >= " + str(min_lon))
            
            if len(VA_df) > 0:
                
                for i in range(lats.shape[0]):
                    for j in range(lons.shape[1]):
                        
                        #lat_2 = lats[i,j]
                        #lat_1 = lats[i+1,j]
                        #d_y = lat_2 - lat_1
                        
                        #lon_1 = lons[i,j]
                        #lon_2 = lons[i,j+1]
                        #d_lon = lon_2 - lon_1
                        
                        lat_up  = lats[i,j] + dy/2
                        lat_dwn = lats[i,j] - dy/2
                        
                        lon_rgt = lons[i,j] + dx/2
                        lon_lft = lons[i,j] - dx/2
                        
                        filtered_df = VA_df.query("latitude <= " + str(lat_up) + " & latitude > " + str(lat_dwn) + " & longitude <= " + str(lon_rgt) + " & longitude > " + str(lon_lft)) 

                        grid_conteo[l,i,j] = len(filtered_df)

    # save process of grid
    np.save(ruta_save + 'count_10m_' + fecha1.strftime("%Y%m") + '.npy', grid_conteo)
