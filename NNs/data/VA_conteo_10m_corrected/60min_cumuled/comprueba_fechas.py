from dateutil.relativedelta import *
import pandas as pd
import numpy as np
import datetime as dt
import glob

freq_data = 15 # minutes
lag       = 6  # minutes

files = glob.glob("/home/storm/lightnings_research/NNs/data/VA_conteo_15m_corrected/60min_cumuled/*.npy")
files.sort()

for file in files:

    fecha1 = dt.datetime.strptime(file[file.find("_60min")+7:-4], "%Y%m") + relativedelta(minutes=lag)
    fecha2 = fecha1 + relativedelta(months=1) - relativedelta(minutes=freq_data)

    dates  = pd.date_range(fecha1, fecha2, freq=str(freq_data)+"T")

    dates_selected = dates[(dates.hour >= 12) & (dates.hour <= 23)]

    data = np.load(file)

    if len(data) == len(dates_selected):
        print(fecha1.strftime("%Y%m"), "ok")
    else:
        print(fecha1.strftime("%Y%m"), "error")