import numpy as np
import pandas as pd
import pickle
from depura_datos_lightnings import lightnings_10_min, lightnings_15_min
from read_ABI import read_ABI_data
import tensorflow as tf

# Verificar si TensorFlow detecta la GPU
physical_devices = tf.config.list_physical_devices('GPU')
print("Num GPUs Available: ", len(physical_devices))

if len(physical_devices) > 0:
    #tf.config.experimental.set_memory_growth(physical_devices[0], True)
    tf.config.experimental.set_visible_devices(physical_devices[1], 'GPU')
    print("TensorFlow está usando la GPU")
else:
    print("TensorFlow no detecta ninguna GPU")

# read data
"recover ABI data"
#dates_ABI, data_ABI_05, data_ABI_06, data_ABI_08, data_ABI_09, data_ABI_10, data_ABI_13, mean_data_ABI_05, mean_data_ABI_06, mean_data_ABI_08, mean_data_ABI_09, mean_data_ABI_10, mean_data_ABI_13 = read_ABI_data()


"recover lightnings data"
raw_lightnings_data_15m, dates_lightnings_15m = lightnings_15_min()
raw_lightnings_data_10m, dates_lightnings_10m = lightnings_10_min()

dates_lightnings     = dates_lightnings_15m + dates_lightnings_10m
dates_lightnings     = pd.DatetimeIndex(dates_lightnings)
raw_lightnings_data  = np.concatenate([raw_lightnings_data_15m, raw_lightnings_data_10m], axis=0)
ioubhikgbujikug

# extract those lightning maps where for certain date in ABI it exists in the lightnings dates
lightnings_pos     = []
ABI_pos            = []
fechas_definitivas = []

for i, f in enumerate(dates_ABI):
    pos = np.where(dates_lightnings == f)[0]
    if len(pos) != 0:
        lightnings_pos.append(pos[0])
        ABI_pos.append(i)
        fechas_definitivas.append(f)

ABI_05       = data_ABI_05[ABI_pos]
ABI_06       = data_ABI_06[ABI_pos]
ABI_08       = data_ABI_08[ABI_pos]
ABI_09       = data_ABI_09[ABI_pos]
ABI_10       = data_ABI_10[ABI_pos]
ABI_13       = data_ABI_13[ABI_pos]
mean_ABI_05  = mean_data_ABI_05[ABI_pos]
mean_ABI_06  = mean_data_ABI_06[ABI_pos]
mean_ABI_08  = mean_data_ABI_08[ABI_pos]
mean_ABI_09  = mean_data_ABI_09[ABI_pos]
mean_ABI_10  = mean_data_ABI_10[ABI_pos]
mean_ABI_13  = mean_data_ABI_13[ABI_pos]
lightnings   = raw_lightnings_data[lightnings_pos]


np.save("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/ABI_05.npy", ABI_05)
np.save("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/ABI_06.npy", ABI_06)
np.save("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/ABI_08.npy", ABI_08)
np.save("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/ABI_09.npy", ABI_09)
np.save("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/ABI_10.npy", ABI_10)
np.save("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/ABI_13.npy", ABI_13)

np.save("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/mean_ABI_05.npy", mean_ABI_05)
np.save("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/mean_ABI_06.npy", mean_ABI_06)
np.save("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/mean_ABI_08.npy", mean_ABI_08)
np.save("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/mean_ABI_09.npy", mean_ABI_09)
np.save("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/mean_ABI_10.npy", mean_ABI_10)
np.save("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/mean_ABI_13.npy", mean_ABI_13)

np.save("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/lightnings.npy", lightnings)

pickle.dump(fechas_definitivas, open("/home/storm/lightnings_research/NNs/data/data_ready_5_6_8_9_10_13_60_min_corrected/fechas.bin", "wb"))
