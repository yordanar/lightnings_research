import numpy as np
import glob
import pickle


# Rutas
ruta_ABI = "/home/storm/lightnings_research/NNs/data/VA_ABI/ABI_5_6_8_9_10_13_instant_plus_mean_30min/"

def read_ABI_data():

    # list files
    dates_files       = glob.glob(ruta_ABI + "fechas*.bin")
    ABI_files_05      = glob.glob(ruta_ABI + "instantaneo_05*.npy")
    ABI_files_06      = glob.glob(ruta_ABI + "instantaneo_06*.npy")
    ABI_files_08      = glob.glob(ruta_ABI + "instantaneo_08*.npy")
    ABI_files_09      = glob.glob(ruta_ABI + "instantaneo_09*.npy")
    ABI_files_10      = glob.glob(ruta_ABI + "instantaneo_10*.npy")
    ABI_files_13      = glob.glob(ruta_ABI + "instantaneo_13*.npy")
    mean_ABI_files_05 = glob.glob(ruta_ABI + "mean_05*.npy")
    mean_ABI_files_06 = glob.glob(ruta_ABI + "mean_06*.npy")
    mean_ABI_files_08 = glob.glob(ruta_ABI + "mean_08*.npy")
    mean_ABI_files_09 = glob.glob(ruta_ABI + "mean_09*.npy")
    mean_ABI_files_10 = glob.glob(ruta_ABI + "mean_10*.npy")
    mean_ABI_files_13 = glob.glob(ruta_ABI + "mean_13*.npy")

    dates_files.sort()
    ABI_files_05.sort()
    ABI_files_06.sort()
    ABI_files_08.sort()
    ABI_files_09.sort()
    ABI_files_10.sort()
    ABI_files_13.sort()
    mean_ABI_files_05.sort()
    mean_ABI_files_06.sort()
    mean_ABI_files_08.sort()
    mean_ABI_files_09.sort()
    mean_ABI_files_10.sort()
    mean_ABI_files_13.sort()


    # Read the dates
    for i in range(len(dates_files)):

        if i != 0:
            dates_ABI   = dates_ABI + pickle.load(open(dates_files[i], "rb"))
            data_ABI_05 = np.concatenate([data_ABI_05, np.load(ABI_files_05[i])], axis=0);          mean_data_ABI_05 = np.concatenate([mean_data_ABI_05, np.load(mean_ABI_files_05[i])], axis=0)
            data_ABI_06 = np.concatenate([data_ABI_06, np.load(ABI_files_06[i])], axis=0);          mean_data_ABI_06 = np.concatenate([mean_data_ABI_06, np.load(mean_ABI_files_06[i])], axis=0)
            data_ABI_08 = np.concatenate([data_ABI_08, np.load(ABI_files_08[i])], axis=0);          mean_data_ABI_08 = np.concatenate([mean_data_ABI_08, np.load(mean_ABI_files_08[i])], axis=0)
            data_ABI_09 = np.concatenate([data_ABI_09, np.load(ABI_files_09[i])], axis=0);          mean_data_ABI_09 = np.concatenate([mean_data_ABI_09, np.load(mean_ABI_files_09[i])], axis=0)
            data_ABI_10 = np.concatenate([data_ABI_10, np.load(ABI_files_10[i])], axis=0);          mean_data_ABI_10 = np.concatenate([mean_data_ABI_10, np.load(mean_ABI_files_10[i])], axis=0)
            data_ABI_13 = np.concatenate([data_ABI_13, np.load(ABI_files_13[i])], axis=0);          mean_data_ABI_13 = np.concatenate([mean_data_ABI_13, np.load(mean_ABI_files_13[i])], axis=0)
        else:
            dates_ABI   = pickle.load(open(dates_files[i], "rb"))
            data_ABI_05 = np.load(ABI_files_05[i])                               ;          mean_data_ABI_05 = np.load(mean_ABI_files_05[i])
            data_ABI_06 = np.load(ABI_files_06[i])                               ;          mean_data_ABI_06 = np.load(mean_ABI_files_06[i])
            data_ABI_08 = np.load(ABI_files_08[i])                               ;          mean_data_ABI_08 = np.load(mean_ABI_files_08[i])
            data_ABI_09 = np.load(ABI_files_09[i])                               ;          mean_data_ABI_09 = np.load(mean_ABI_files_09[i])
            data_ABI_10 = np.load(ABI_files_10[i])                               ;          mean_data_ABI_10 = np.load(mean_ABI_files_10[i])
            data_ABI_13 = np.load(ABI_files_13[i])                               ;          mean_data_ABI_13 = np.load(mean_ABI_files_13[i])

    dates_ABI.sort()
    
    return dates_ABI, data_ABI_05, data_ABI_06, data_ABI_08, data_ABI_09, data_ABI_10, data_ABI_13, mean_data_ABI_05, mean_data_ABI_06, mean_data_ABI_08, mean_data_ABI_09, mean_data_ABI_10, mean_data_ABI_13
