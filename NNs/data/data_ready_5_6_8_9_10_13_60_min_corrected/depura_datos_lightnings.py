import numpy as np
import pandas as pd
import glob
import datetime as dt
from dateutil.relativedelta import *

# funciones
def depura_mapas(ruta_mapas, numero_fechas_mode, elimina_element_mode, meses, lag, freq_data):
    
    # lista de files de minutos
    for i, m in enumerate(meses):
        if i != 0:
            files_lightnings = files_lightnings + glob.glob(ruta_mapas + "*" + "%.2d" % m + ".npy")
        else:
            files_lightnings = glob.glob(ruta_mapas + "*" + "%.2d" % m + ".npy")
            
    files_lightnings.sort()

    for i, fi in enumerate(files_lightnings):

        fecha1 = dt.datetime.strptime(fi[fi.find("_60min")+7:-4], "%Y%m") + relativedelta(minutes=lag)
        fecha2 = fecha1 + relativedelta(months=1) - relativedelta(minutes=freq_data)

        dates = pd.date_range(fecha1, fecha2, freq=str(freq_data)+"T")

        if i != 0:
            lightnings_cumuled_mode = np.concatenate([lightnings_cumuled_mode, np.load(fi)])
            fechas_lightnings_mode  = fechas_lightnings_mode.append(dates)
        else:
            lightnings_cumuled_mode = np.load(fi)
            fechas_lightnings_mode  = dates

    cont = 0

    # elimina el último mapa de cada día para los datos 15 minutales
    while cont < len(lightnings_cumuled_mode):
        
        if cont != 0:
            lightnings_mode = np.concatenate([lightnings_mode, lightnings_cumuled_mode[cont : cont + numero_fechas_mode - elimina_element_mode]], axis=0)
            fechas_mode     = fechas_mode + list(fechas_lightnings_mode[cont : cont + numero_fechas_mode - elimina_element_mode])
        else:
            lightnings_mode = lightnings_cumuled_mode[cont : cont + numero_fechas_mode - elimina_element_mode]
            fechas_mode     = list(fechas_lightnings_mode[cont : cont + numero_fechas_mode - elimina_element_mode])

        cont = cont + numero_fechas_mode

    return lightnings_mode, fechas_mode


# datos 15 minutales de 2018 a 2019-03

def lightnings_15_min():
    equatorial_lag_mode3    = 6  # minutes
    freq_mode3              = 15 # minutes
    elimina_element_mode3   = 1  # el # de mapas de cada día que debe eliminarse ya que no se calculó bien su sumatoria
    fechas_un_dia_mode3     = pd.date_range("2018-01-01", "2018-01-01 23:45", freq="15min")
    
    numero_fechas_mode3     = len(fechas_un_dia_mode3[(fechas_un_dia_mode3.hour >= 12) & (fechas_un_dia_mode3.hour <= 23)])

    lightnings_mode3, fechas_mode3 = depura_mapas("/home/storm/lightnings_research/NNs/data/VA_conteo_15m_corrected/60min_cumuled/", numero_fechas_mode3, elimina_element_mode3, [3, 4, 5], equatorial_lag_mode3, freq_mode3)

    return lightnings_mode3, fechas_mode3


# datos 10 minutales de 2019-04 a 2024-05
def lightnings_10_min():
    equatorial_lag_mode6    = 5  # minutes
    freq_mode6              = 10 # minutes
    elimina_element_mode6   = 2  # el # de mapas de cada día que debe eliminarse ya que no se calculó bien su sumatoria
    fechas_un_dia_mode6     = pd.date_range("2019-04-01", "2019-04-01 23:50", freq="10min")
    
    numero_fechas_mode6     = len(fechas_un_dia_mode6[(fechas_un_dia_mode6.hour >= 12) & (fechas_un_dia_mode6.hour <= 23)])

    lightnings_mode6, fechas_mode6 = depura_mapas("/home/storm/lightnings_research/NNs/data/VA_conteo_10m_corrected/60min_cumuled/", numero_fechas_mode6, elimina_element_mode6, [3, 4, 5], equatorial_lag_mode6, freq_mode6)

    return lightnings_mode6, fechas_mode6

