import numpy as np
import tensorflow as tf
import pickle

# Verificar si TensorFlow detecta la GPU
physical_devices = tf.config.list_physical_devices('GPU')
print("Num GPUs Available: ", len(physical_devices))

if len(physical_devices) > 0:
    #tf.config.experimental.set_memory_growth(physical_devices[0], True)
    tf.config.experimental.set_visible_devices(physical_devices[1], 'GPU')
    print("TensorFlow está usando la GPU")
else:
    print("TensorFlow no detecta ninguna GPU")



#######################################                      #######################################
#######################################   HYPERPARAMETERS    #######################################
#######################################                      #######################################



hidden_layer1       = [10, 16, 24]
hidden_layer2       = [10, 16, 24]
batch_size          = [64,128,512]
learning_rate       = [10e-3, 10e-4, 10e-5, 10e-6, 10e-7]
#learning_rate_decay = [0.6,0.8,0.9,0.95]
learning_rate_decay = [0.8]
reg_parameter       = [10e-3, 10e-2, 1, 10]
num_epochs          = 100

#######################################                      #######################################
####################################### LECTURA DE LOS DATOS #######################################
#######################################                      #######################################

ABI_train         = np.load("/home/storm/lightnings_research/NNs/data/FC_data2/TRAIN_ABI.npy")
ABI_val           = np.load("/home/storm/lightnings_research/NNs/data/FC_data2/VAL_ABI.npy")
ABI_test           = np.load("/home/storm/lightnings_research/NNs/data/FC_data2/TEST_ABI.npy")

lightnings_train  = np.load("/home/storm/lightnings_research/NNs/data/FC_data2/TRAIN_LIGHTNINGS_TAGS.npy")
lightnings_val    = np.load("/home/storm/lightnings_research/NNs/data/FC_data2/VAL_LIGHTNINGS_TAGS.npy")
lightnings_test   = np.load("/home/storm/lightnings_research/NNs/data/FC_data2/TEST_LIGHTNINGS_TAGS.npy")


#######################################                                     #######################################
####################################### TRAINING AND HYPERPARAMETER TUNNING #######################################
#######################################                                     #######################################

#for hl1 in hidden_layer1:
for hl1, hl2 in zip(hidden_layer1, hidden_layer2):
    for bz in batch_size:
        for lr in learning_rate:
            for lrd in learning_rate_decay:
                for rp in reg_parameter:
                
                    print("hl1=" + str(hl1) + ", hl2=" + str(hl2) + ", bs=" + str(bz) + ", lr=" + str(lr) + ", lrd=" + str(lrd) + ", rp=" + str(rp))
                    model_filename = "model_hl1_" + str(hl1) + "_hl2_" + str(hl2) + "_bs_" + str(bz) + "_lr_" + str(int(np.log10(lr)*-1)) + "_lrd_" + str(int(lrd*100)) + "_rp_" + str(int(np.log10(rp)*-1))

                    # print("hl1=" + str(hl1) + ", bs=" + str(bz) + ", lr=" + str(lr) + ", lrd=" + str(lrd) + ", rp=" + str(rp))
                    # model_filename = "model_hl1_" + str(hl1) + "_bs_" + str(bz) + "_lr_" + str(int(np.log10(lr)*-1)) + "_lrd_" + str(int(lrd*100)) + "_rp_" + str(int(np.log10(rp)*-1))


                    ##########################################                                      #######################################
                    ########################################## CONSTRUYE LA ARQUITECTURA DEL MODELO #######################################
                    ##########################################                                      #######################################

                    model = tf.keras.models.Sequential()

                    "First Hidden Layer"
                    model.add(tf.keras.layers.Dense(hl1, activation='relu', kernel_regularizer=tf.keras.regularizers.l2(rp), kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(12)), input_shape=[12]))
                    model.add(tf.keras.layers.BatchNormalization())
                    model.add(tf.keras.layers.Dropout(0.3))
                    "Seccond Hidden Layer"
                    model.add(tf.keras.layers.Dense(hl2, activation='relu', kernel_regularizer=tf.keras.regularizers.l2(rp), kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(hl1))))
                    model.add(tf.keras.layers.BatchNormalization())
                    model.add(tf.keras.layers.Dropout(0.3))
                    "Only 1 output neuron. It will contain a value from 0-1 where 0 for 1 class ('cats') and 1 for the other ('dogs')"
                    #model.add(tf.keras.layers.Dense(1, activation='sigmoid', kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(hl1))))
                    model.add(tf.keras.layers.Dense(1, activation='sigmoid', kernel_initializer=tf.keras.initializers.RandomNormal(stddev=1/np.sqrt(hl2))))
                    

                    ##########################################                     #######################################
                    ########################################## METRICAS DEL MODELO #######################################
                    ##########################################                     #######################################

                    accuracy = tf.keras.metrics.BinaryAccuracy()
                    AUC      = tf.keras.metrics.AUC()
                    TP       = tf.keras.metrics.TruePositives()
                    FP       = tf.keras.metrics.FalsePositives()
                    TN       = tf.keras.metrics.TrueNegatives()
                    FN       = tf.keras.metrics.FalseNegatives()


                    ##########################################                   #######################################
                    ########################################## COMPILA Y ENTRENA #######################################
                    ##########################################                   #######################################

                    LR_init = tf.keras.optimizers.schedules.InverseTimeDecay(lr, int(len(lightnings_train)/bz), lrd)
                    adam = tf.keras.optimizers.Adam(learning_rate=LR_init)
                    model.compile(optimizer = adam,
                                loss = tf.keras.losses.BinaryCrossentropy(),#'crossentropy',
                                metrics=[accuracy,AUC,TP,FP,TN,FN])
                    
                    result = model.fit(ABI_train, lightnings_train,
                            batch_size=bz,
                            epochs=num_epochs,
                            verbose=2,
                            validation_data=(ABI_val, lightnings_val))

                    ##########################################                                               #######################################
                    ########################################## GUARDA MODELO (weights and biases) y METRICAS #######################################
                    ##########################################                                               #######################################

                    "Save model"
                    save_path  = "/home/storm/lightnings_research/NNs/implement_FC/FCN2/"
                    model.save(save_path + model_filename + ".keras")

                    "Save metrics history"
                    f = open(save_path + "history_" + model_filename + ".pkl", "wb") 
                    pickle.dump(result.history, f)