#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Meteorología satelital - SIATA - GOES East - Función de lectura de datos de las bandas del ABI.

import numpy as np
import netCDF4 as nc
from pyproj import Proj

# ------------------------------------- FUNCIONES PARA LA LECTURA DE LAS BANDAS DEL ABI -------------------------------------- #
#                                                                                                                              #
# La función *obtiene_bt_o_fr* lee las radiacias y las retorna como factores de reflectancia (bandas 1-6) o como temperaturas  #
# de brillo (bandas 7-16); y también devuelve las latitudes y las longitudes. Como input se requieren las rutas de la escena,  #
# aunque está habilitado para recibir una ruta individual banda específica.                                                    #
# ---------------------------------------------------------------------------------------------------------------------------- #

def obtiene_fr_o_bt(limites,rutas,banda):
    
    if len(rutas) > 1:
        ruta_esp = [ruta for ruta in rutas if ('C%s' %(banda) in ruta)][0]
    else:
        ruta_esp = rutas[0]
    archivo = nc.Dataset(ruta_esp)
    h = archivo.variables['goes_imager_projection'].perspective_point_height
    x = archivo.variables['x'][limites[0]:limites[1]]
    y = archivo.variables['y'][limites[2]:limites[3]]
    lon_0 = archivo.variables['goes_imager_projection'].longitude_of_projection_origin
    lat_0 = archivo.variables['goes_imager_projection'].latitude_of_projection_origin
    sat_sweep = archivo.variables['goes_imager_projection'].sweep_angle_axis
    if banda in ['01','02','03','04','05','06']:
        d = archivo.variables['earth_sun_distance_anomaly_in_AU'][:]**2
        esun = archivo.variables['esun'][:]
        rad = archivo.variables['Rad'][limites[2]:limites[3],limites[0]:limites[1]]
        datos =  (rad*np.pi*d)/esun
        datos[datos[:,:]<0]=0
        datos[datos[:,:]>1]=1
    else:
        fk1 = archivo.variables[u'planck_fk1'][:]
        fk2 = archivo.variables[u'planck_fk2'][:]
        bc1 = archivo.variables[u'planck_bc1'][:]
        bc2 = archivo.variables[u'planck_bc2'][:]
        rad = archivo.variables['Rad'][limites[2]:limites[3],limites[0]:limites[1]]
        datos =  ((fk2/ (np.log((fk1 / (rad)) + 1)) - bc1 )/ bc2)
        datos = datos -273.15
    archivo.close()
    x = x * h
    y = y * h
    p = Proj(proj='geos', h=h, lon_0=lon_0, swee=sat_sweep)
    xx, yy = np.meshgrid(x,y)
    lons, lats = p(xx, yy, inverse=True)
    lons = np.ma.array(lons)
    lons[lons == 1.00000000e+30] = np.ma.masked
    lats = np.ma.array(lats)
    lats[lats == 1.00000000e+30] = np.ma.masked
    return lons, lats, datos

