#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Meteorología satelital - SIATA - GOES East - Funciones de lectura de la información de los sensores ABI y GLM.

def limites_zonas():
    
    limites_zonas = {'0.5km':{'Tropical':[4297,19362,4681,13037],'Colombia_extendida':[6048,16652,6175,13754],
                              'Colombia': [9738,13051,7805,11949],'Antioquia_extendida':[9706,11774,8434,10240],
                              'Antioquia':[10203,11292,8830,9746],'Valle_de_Aburra_extendido':[10491,10971,9257,9720],
                              'Ituango':[10070,11292,8765,10517],'Synapsis':[10454,10989,9220,9744],'Valle_de_Aburra':[10680,10808,9410,9538]},
                     '1.0km':{'Tropical':[2148,9681,2340,6518],'Colombia_extendida':[3024,8326,3087,6877],
                              'Colombia': [4868,6525,3902,5974],'Antioquia_extendida':[4853,5887,4216,5120],
                              'Antioquia':[5101,5646,4414,4873], 'Valle_de_Aburra_extendido':[5245,5485,4628,4860],
                              'Ituango':[5034,5646,4382,5258],'Synapsis':[5227,5494,4610,4872],'Valle_de_Aburra':[5340,5404,4704,4768]},
                     '2.0km':{'Tropical':[1073,4840,1169,3259],'Colombia_extendida':[1512,4163,1543,3438],
                              'Colombia': [2433,3262,1950,2987],'Antioquia_extendida':[2426,2943,2107,2560],
                              'Antioquia':[2550,2823,2206,2436], 'Valle_de_Aburra_extendido':[2622,2742,2313,2430],
                              'Ituango':[2516,2823,2190,2629],'Synapsis':[2613,2747,2305,2436],'Valle_de_Aburra':[2670,2702,2352,2384]}}    
    return limites_zonas