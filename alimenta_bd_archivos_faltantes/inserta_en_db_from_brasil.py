import glob
import netCDF4 as nc
from netCDF4 import num2date
import numpy as np
import datetime as dt
import pandas as pd
import MySQLdb
from credentials import  mysql_credentials_modify

# credenciales para guardar los datos de flashes en la bd
mcm    = mysql_credentials_modify()
host   = mcm["host"]
user   = mcm["user"]
passwd = mcm["passwd"]
dbname = mcm["dbname"]

# Información sólo para Colombia
min_lat = -5
max_lat = 13
min_lon = -80
max_lon = -66

# Funcion
def fecha_estandar(variable):#para el titulo de la figura, descomentar el segundo t_unit
    tname = variable
    nctime = nc_fid.variables[tname][:] # get values
    t_unit = nc_fid.variables[tname].units # get unit  "days since 1950-01-01T00:00:00Z"
    try :
        t_cal = nc_fid.variables[tname].calendar
    except AttributeError : # Attribute doesn't exist
        t_cal = u"standard" # or standard
    list_datevar = []
    if len(nctime)!=0:
        datevar = num2date(nctime,units = t_unit,calendar = t_cal)
        list_datevar.append(datevar)
    else:
        for i in nctime:
            datevar = num2date(i,units = t_unit,calendar = t_cal)
            list_datevar.append(datevar)
    return list_datevar

ruta_nc = "/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/archivos_recuperados/"

folders = glob.glob(ruta_nc + "2023*")
folders.sort()

no_leidos = []

for carpeta in folders:

    files_nc = glob.glob(carpeta + "/*.nc")
    files_nc.sort()

    if len(files_nc) >= 1:

        for i, f in enumerate(files_nc):
            
            print(i, len(files_nc))
            
            try:
            
                nc_fid = nc.Dataset(f)
                
                flash_lats    = nc_fid.variables['flash_lat'][:]
                flash_lons    = nc_fid.variables['flash_lon'][:]
                
                pos_lats   = np.where((min_lat < flash_lats) & (flash_lats < max_lat))[0]

                if len(pos_lats) != 0:
                    
                    flash_lats = flash_lats[pos_lats]
                    flash_lons = flash_lons[pos_lats]
                    
                    pos_lons   = np.where((min_lon < flash_lons) & (flash_lons < max_lon))[0]
                    
                    if len(pos_lons) != 0:
                        
                        flash_lats = flash_lats[pos_lons]
                        flash_lons = flash_lons[pos_lons]
                
                        flash_id      = nc_fid.variables['flash_id'][pos_lats][pos_lons]
                                
                        flash_UTC1    = fecha_estandar('flash_time_offset_of_first_event')[0][pos_lats][pos_lons]
                        flash_local1  = [base2 - dt.timedelta(hours=5) for base2 in flash_UTC1]
                        
                        flash_UTC2    = fecha_estandar('flash_time_offset_of_last_event')[0][pos_lats][pos_lons]
                        flash_local2  = [base2 - dt.timedelta(hours=5) for base2 in flash_UTC2]
                        
                        flash_energy  =  nc_fid.variables['flash_energy'][:][pos_lats][pos_lons]
                        flash_area    =  nc_fid.variables['flash_area'][:][pos_lats][pos_lons]
                        flash_quality = nc_fid.variables['flash_quality_flag'][:][pos_lats][pos_lons]
                        
                        nctime = nc_fid.variables['product_time'][0] # get values
                        t_unit = nc_fid.variables['product_time'].units # get unit  "days since 1950-01-01T00:00:00Z"
                        try:
                            t_cal = nc_fid.variables['product_time'].calendar
                        except AttributeError : # Attribute doesn't exist
                            t_cal = u"standard" # or standard
                        list_datevar = []
                        product_time = num2date(nctime, units=t_unit, calendar=t_cal)
                        #product_time = fecha_estandar('product_time')[0]
                        file_name = f[f.find('OR'):]
                        
                        "Inserta los datos en la base de datos glm_product_flashes_con_key"
                        conn_db   = MySQLdb.connect (host, user, passwd, dbname)
                        values    = zip(flash_id,flash_UTC1,flash_local1,flash_UTC2, flash_local2, flash_lats, flash_lons, flash_energy, flash_area, flash_quality,[product_time] * len(flash_lats), [file_name] *len(flash_lats))
                        db_cursor = conn_db.cursor ()
                        db_cursor.executemany("INSERT IGNORE INTO %s (flash_id, fecha_utc_begin, fecha_local_begin, fecha_utc_end, fecha_local_end, latitud, longitud, energy_j, area_km2, quality, product_time,file_name) VALUES (%%s, %%s, %%s,%%s, %%s, %%s,%%s, %%s, %%s,%%s, %%s, %%s)"% "glm_product_flashes_con_key", (values))
                        db_cursor.close()
                        conn_db.commit()
                        conn_db.close()
                
                nc_fid.close()
                
                print(u'Se insertó bien el dato', f, i, len(files_nc))

            except:
                print(u'NO se insertó el dato', f, i, len(files_nc))
                no_leidos.append(f)
                
if len(no_leidos) >= 1: # si hubo datos que no se encontraron en aws
    faltantes_aws = open(ruta_nc +'/no_leidos_brasil2.txt', "w")
    for ff in no_leidos:
        faltantes_aws.write(ff + '\n')
    faltantes_aws.close()