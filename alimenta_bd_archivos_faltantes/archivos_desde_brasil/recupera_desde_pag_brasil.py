import pandas as pd
import glob 
import datetime as dt
import os
import ftplib      

FTP_HOST = "ftp.cptec.inpe.br"
FTP_USER = "anonymous"
FTP_PASS = ""

ruta_aws      = "/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/faltante_aws/"

save_brasil   = "/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/archivos_desde_brasil/"

fechas = pd.date_range('2023-01-01', '2023-12-31', freq='M')
for f in fechas:
    
    print(f)
    
    aws_faltantes = glob.glob(ruta_aws + "faltantes_aws_" + f.strftime("%Y%m") + "*.txt")
    aws_faltantes.sort()

    for af in aws_faltantes:
        # obtiene fechas en las que faltan archivos
        with open(af) as k:
            fechas_faltantes = k.readlines()
        if fechas_faltantes[0] == 'ok':
            pass
        elif fechas_faltantes[0] == 'No faltaba ninguno en storinator':
            pass
        else:
            
            f0      = dt.datetime.strptime(fechas_faltantes[0][:-1], "%Y%j%H%M%S")
            
            ftp          = ftplib.FTP(FTP_HOST, timeout=30)
            ftp.login(user=FTP_USER, passwd=FTP_PASS)  # provide the username and password
            ftp.cwd('/goes/goes16/glm/' + f0.strftime("%Y/%m/%d/")) 
            files = ftp.nlst()
            fechas_files = [O[O.find("_s")+2:O.find("_s")+15] for O in files]
            DF_files     = pd.DataFrame(data={"fechas_files":fechas_files, "GLM_files":files})

            files_to_download = []

            for ff in fechas_faltantes:
                date        = dt.datetime.strptime(ff[:-1], "%Y%j%H%M%S")
                selected_df = DF_files[DF_files["fechas_files"] == ff[:-1]]
                try:
                    files_to_download.append(selected_df["GLM_files"].values[0])
                except:
                    pass
                
            faltantes_aws = open(save_brasil + '/recupera_' + f0.strftime('%Y%m%d') + '.txt', "w")
            for a in files_to_download:
                faltantes_aws.write("ftp.cptec.inpe.br/goes/goes16/glm/" + f0.strftime("%Y/%m/%d/") + a + '\n')
            faltantes_aws.close()
            
            os.system("wget -i " + save_brasil + '/recupera_' + f0.strftime('%Y%m%d') + ".txt -P " + save_brasil + " -q --show-progress")
