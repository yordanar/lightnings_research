import pickle
import glob
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

path_missed_files = "/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/"

missed_files = glob.glob(path_missed_files + "conteo_db*_conteo.bin")
missed_files.sort()

for i, mf in enumerate(missed_files):

    if i == 0:
        monthly_series = pickle.load(open(mf, "rb"))
    else:
        monthly_series = pd.concat([monthly_series, pickle.load(open(mf, "rb"))])


series = monthly_series[:"2024-06-30"]
fechas = series.index
percentages = series[0].values

# Create the bar plot
fig= plt.figure(figsize=(10,5))
ax = fig.add_axes([0.08, 0.14, 0.9, 0.81])
bars = ax.bar(range(len(fechas)), list(percentages), color="grey")

#Set the title and labels
ax.set_title('Proportion of missing GLM files')
ax.set_xlabel(u'Time')
ax.set_ylabel('Porcentage [%]')

# Iterate over the bars and add a text annotation for those exceeding a value of two
for bar in bars:
    height = bar.get_height()
    if height > 2:
        ax.text(bar.get_x() + bar.get_width() / 2, height,
                str(round(height, 2)), ha='center', va='bottom')

ax.set_xticks(range(len(fechas))[::4])
ax.set_xticklabels([di.strftime('%y-%m') for di in fechas][::4], rotation = 45, ha="right")

plt.savefig("/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/faltantes_bd.png")
plt.close("all")
