import pandas as pd
import matplotlib.pyplot as plt
from dateutil.relativedelta import relativedelta 
import numpy as np
from credentials import mysql_credentials_query
import MySQLdb
import pickle

# path_glm = "/mnt/GOES_GLM/"

# fechas = pd.date_range('2018-01-01', '2023-12-31', freq='M')

# percentages = []

# for f in fechas:

#     f0 = f + relativedelta(days=1)
    
#     fi = f0 - relativedelta(months=1)
#     fe = f0 - relativedelta(seconds=1)
    
#     daily_fechas = pd.date_range(fi, fe, freq='D')
    
#     files_per_month = 0
    
#     for df in daily_fechas:
        
#         files = glob.glob(path_glm + df.strftime("%Y%m%d") + "/OR_GLM*.nc")
        
#         files_per_month = files_per_month + len(files)
    
#     needed = 4320 * len(daily_fechas)
    
#     percentages.append((needed - files_per_month) / needed)
    
    
# "pinta"
# percentages = np.array(percentages) * 100

# # Create the bar plot
# fig= plt.figure(figsize=(10,5))
# ax = fig.add_axes([0.08, 0.14, 0.9, 0.81])
# bars = ax.bar(range(len(fechas)), list(percentages), color="grey")

# #Set the title and labels
# ax.set_title('Proportion of missing GLM files')
# ax.set_xlabel(u'Time')
# ax.set_ylabel('Porcentage [%]')

# # Iterate over the bars and add a text annotation for those exceeding a value of two
# for bar in bars:
#     height = bar.get_height()
#     if height > 2:
#         ax.text(bar.get_x() + bar.get_width() / 2, height,
#                 str(round(height, 2)), ha='center', va='bottom')

# ax.set_xticks(range(len(fechas))[::4])
# ax.set_xticklabels([di.strftime('%y-%m') for di in fechas][::4], rotation = 45, ha="right")

# #plt.savefig("/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/faltantes_aws.png")
# plt.savefig("/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/faltantes_despues_de_brasil.png")
# plt.close("all")






############################### cuenta archivos insertados en base de datos 

year = 2024

fechas = pd.date_range(str(year)+'-01-01', str(year)+'-12-31', freq='M')

"credentials mysql"
mcq    = mysql_credentials_query()
host   = mcq["host"]
user   = mcq["user"] 
passwd = mcq["passwd"] 
dbname = mcq["dbname"]

# Info just for Colombia
latmin = -5
latmax = 13
lonmin = -80
lonmax = -66

percentages = []

for f in fechas:

    print(f)

    f0 = f + relativedelta(days=1)
    
    fi = f0 - relativedelta(months=1)
    fe = f0 - relativedelta(seconds=1)
    
    daily_fechas = pd.date_range(fi, fe, freq='D')
        
    "query"
    db_conn    = MySQLdb.connect(host, user, passwd, dbname)
    db_cursor  = db_conn.cursor()
    query      = "SELECT DISTINCT file_name FROM glm_product_flashes_con_key WHERE fecha_local_end BETWEEN '" + fi.strftime("%Y-%m-%d %H:%M:%S") + "' and '" + fe.strftime("%Y-%m-%d %H:%M:%S")  + "';"
    db_cursor.execute (query)
    data_db    = db_cursor.fetchall()
    data_array = np.array (data_db)
    db_conn.close()
        
    contador = len(data_array)
        
    needed = 4320 * len(daily_fechas)
    
    percentages.append((needed - contador) / needed)

percentages = np.array(percentages) * 100

DF = pd.DataFrame(index = fechas, data=percentages)

pickle.dump(DF, open("/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/conteo_db_" + str(year) + "_conteo.bin", "wb"))

"pinta"
# # Create the bar plot
# fig= plt.figure(figsize=(10,5))
# ax = fig.add_axes([0.08, 0.14, 0.9, 0.81])
# bars = ax.bar(range(len(fechas)), list(percentages), color="grey")

# #Set the title and labels
# ax.set_title('Proportion of missing GLM files')
# ax.set_xlabel(u'Time')
# ax.set_ylabel('Porcentage [%]')

# # Iterate over the bars and add a text annotation for those exceeding a value of two
# for bar in bars:
#     height = bar.get_height()
#     if height > 2:
#         ax.text(bar.get_x() + bar.get_width() / 2, height,
#                 str(round(height, 2)), ha='center', va='bottom')

# ax.set_xticks(range(len(fechas))[::4])
# ax.set_xticklabels([di.strftime('%y-%m') for di in fechas][::4], rotation = 45, ha="right")

# plt.savefig("/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/archivos_faltantes_en_db.png")
# plt.close("all")
        
        
        
        
        
        
        
        
        
        
        
        