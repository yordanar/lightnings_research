import glob
import datetime as dt
import os

# paths
path_storniator_not_read = "/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/faltante_storinator/"
path_aws_not_read        = "/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/faltante_aws/"
ruta_brasil              = "ftp.cptec.inpe.br/goes/goes16/glm/"

#list files

storinator_not_read      = glob.glob(path_storniator_not_read + "no_leidos_*.txt")
storinator_not_read.sort()
aws_not_read             = glob.glob(path_aws_not_read + "no_leidos_*.txt")
aws_not_read.sort()

# primero descarga desde storinator_not_read
for snr in storinator_not_read:
    
    # obtiene fechas en las que faltan archivos
    with open(snr) as k:
        archivos_faltantes = k.readlines()
        
    for ar in archivos_faltantes:
        fecha = dt.datetime.strptime(ar[ar.find("G16_s")+5:ar.find("G16_s")+18], "%Y%j%H%M%S")
        
        # arma la ruta
        ruta_file = ruta_brasil + fecha.strftime("%Y/%m/%d/") + ar
    
        os.system("wget -v --no-check-certificate " + ruta_file)



# ahora descarga desde aws_not_read
for aws in aws_not_read:
    
    # obtiene fechas en las que faltan archivos
    with open(aws) as k:
        archivos_faltantes = k.readlines()
        
    for ar in archivos_faltantes:
        fecha = dt.datetime.strptime(ar[ar.find("G16_s")+5:ar.find("G16_s")+18], "%Y%j%H%M%S")
        
        if fecha.year == 2023:
        
            # arma la ruta
            ruta_file = ruta_brasil + fecha.strftime("%Y/%m/%d/") + ar[ar.find("OR"):]
        
            os.system("wget -v --no-check-certificate " + ruta_file)


