import os
import pandas as pd
import datetime as dt
import glob
import netCDF4 as nc
from netCDF4 import num2date
import MySQLdb
import numpy as np
import sys
sys.path.insert(1,'/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes')
from credentials import  mysql_credentials_modify


fechas   = pd.date_range('2021-01-01', '2021-12-31', freq='D')
ruta_glm = '/mnt/GOES_GLM/'
save_glm = '/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/faltante_storinator/'

mcm    = mysql_credentials_modify()
host   = mcm["host"]
user   = mcm["user"]
passwd = mcm["passwd"]
dbname = mcm["dbname"]

# Información sólo para Colombia

min_lat = -5
max_lat = 13
min_lon = -80
max_lon = -66

# Función
def fecha_estandar(variable):#para el titulo de la figura, descomentar el segundo t_unit
    tname = variable
    nctime = nc_fid.variables[tname][:] # get values
    t_unit = nc_fid.variables[tname].units # get unit  "days since 1950-01-01T00:00:00Z"
    try :
        t_cal = nc_fid.variables[tname].calendar
    except AttributeError : # Attribute doesn't exist
        t_cal = u"standard" # or standard
    list_datevar = []
    if len(nctime)!=0:
        datevar = num2date(nctime,units = t_unit,calendar = t_cal)
        list_datevar.append(datevar)
    else:
        for i in nctime:
            datevar = num2date(i,units = t_unit,calendar = t_cal)
            list_datevar.append(datevar)
    return list_datevar

# copia archivos de GLM e insera en la DB
for f in fechas:

    print(f)

    "lista los archivos copiados"
    archivos = glob.glob(ruta_glm + f.strftime('%Y%m%d/') + 'OR_GLM-L2-LCFA_G16_s' + f.strftime('%Y%j') + '*.nc')
    archivos.sort()
    
    if len(archivos) != 0:

        archivos_no_leidos = []

        "inserta los datos en la DB"
        for ar in archivos:
            
            print(ar)
            
            try:
            
                ########################################################################
                # INFO FLASHES ORGANIZADA
                ########################################################################
                
                nc_fid = nc.Dataset(ar)
                
                flash_lats    = nc_fid.variables['flash_lat'][:]
                flash_lons    = nc_fid.variables['flash_lon'][:]
                
                pos_lats   = np.where((min_lat < flash_lats) & (flash_lats < max_lat))[0]

                if len(pos_lats) != 0:
                    
                    flash_lats = flash_lats[pos_lats]
                    flash_lons = flash_lons[pos_lats]
                    
                    pos_lons   = np.where((min_lon < flash_lons) & (flash_lons < max_lon))[0]
                    
                    if len(pos_lons) != 0:
                        
                        flash_lats = flash_lats[pos_lons]
                        flash_lons = flash_lons[pos_lons]
                
                        flash_id      = nc_fid.variables['flash_id'][pos_lats][pos_lons]
                                
                        flash_UTC1    = fecha_estandar('flash_time_offset_of_first_event')[0][pos_lats][pos_lons]
                        flash_local1  = [base2 - dt.timedelta(hours=5) for base2 in flash_UTC1]
                        
                        flash_UTC2    = fecha_estandar('flash_time_offset_of_last_event')[0][pos_lats][pos_lons]
                        flash_local2  = [base2 - dt.timedelta(hours=5) for base2 in flash_UTC2]
                        
                        flash_energy  =  nc_fid.variables['flash_energy'][:][pos_lats][pos_lons]
                        flash_area    =  nc_fid.variables['flash_area'][:][pos_lats][pos_lons]
                        flash_quality = nc_fid.variables['flash_quality_flag'][:][pos_lats][pos_lons]
                        
                        nctime = nc_fid.variables['product_time'][0] # get values
                        t_unit = nc_fid.variables['product_time'].units # get unit  "days since 1950-01-01T00:00:00Z"
                        try:
                            t_cal = nc_fid.variables['product_time'].calendar
                        except AttributeError : # Attribute doesn't exist
                            t_cal = u"standard" # or standard
                        list_datevar = []
                        product_time = num2date(nctime, units=t_unit, calendar=t_cal)
                        #product_time = fecha_estandar('product_time')[0]
                        file_name = ar[ar.find('OR'):]
                        
                        "Inserta los datos en la base de datos glm_product_flashes_con_key"
                        conn_db   = MySQLdb.connect (host, user, passwd, dbname)
                        values    = zip(flash_id,flash_UTC1,flash_local1,flash_UTC2, flash_local2, flash_lats, flash_lons, flash_energy, flash_area, flash_quality,[product_time] * len(flash_lats), [file_name] *len(flash_lats))
                        db_cursor = conn_db.cursor ()
                        db_cursor.executemany("INSERT IGNORE INTO %s (flash_id, fecha_utc_begin, fecha_local_begin, fecha_utc_end, fecha_local_end, latitud, longitud, energy_j, area_km2, quality, product_time,file_name) VALUES (%%s, %%s, %%s,%%s, %%s, %%s,%%s, %%s, %%s,%%s, %%s, %%s)"% "glm_product_flashes_con_key", (values))
                        db_cursor.close()
                        conn_db.commit()
                        conn_db.close()
                
                nc_fid.close()
                     
                ########################################################################
                # INFO FLASHES ORGANIZADA
                ########################################################################
                # flash_id      = nc_fid.variables['flash_id'][:]

                # flash_UTC1    = fecha_estandar('flash_time_offset_of_first_event')[0]
                # flash_local1  = [base2 - dt.timedelta(hours=5) for base2 in flash_UTC1]

                # flash_UTC2    = fecha_estandar('flash_time_offset_of_last_event')[0]
                # flash_local2  = [base2 - dt.timedelta(hours=5) for base2 in flash_UTC2]

                # flash_lats    = nc_fid.variables['flash_lat'][:]
                # flash_lons    = nc_fid.variables['flash_lon'][:]

                # flash_energy  =  nc_fid.variables['flash_energy'][:]
                # flash_area    =  nc_fid.variables['flash_area'][:]
                # flash_quality = nc_fid.variables['flash_quality_flag'][:]

                # # flag_values: [0 1 3 5]
                # #   0 -> good_quality_qf
                # #   1 -> degraded_due_to_flash_constituent_events_out_of_time_order_qf
                # #   3 -> degraded_due_to_flash_constituent_event_count_exceeds_threshold_qf
                # #   5 -> degraded_due_to_flash_duration_exceeds_threshold_qf

                # nctime = nc_fid.variables['product_time'][0] # get values
                # t_unit = nc_fid.variables['product_time'].units # get unit  "days since 1950-01-01T00:00:00Z"
                # try:
                #     t_cal = nc_fid.variables['product_time'].calendar
                # except AttributeError : # Attribute doesn't exist
                #     t_cal = u"standard" # or standard
                # list_datevar = []
                # product_time = num2date(nctime, units=t_unit, calendar=t_cal)
                # #product_time = fecha_estandar('product_time')[0]
                # file_name = ar[ar.find('OR'):]

                # "Inserta los datos en la base de datos glm_product_flashes_con_key"
                # conn_db   = MySQLdb.connect (host, user, passwd, dbname)
                # values    = zip(flash_id,flash_UTC1,flash_local1,flash_UTC2, flash_local2, flash_lats, flash_lons, flash_energy, flash_area, flash_quality,[product_time] * len(flash_lats), [file_name] *len(flash_lats))
                # db_cursor = conn_db.cursor ()
                # db_cursor.executemany("INSERT IGNORE INTO %s (flash_id, fecha_utc_begin, fecha_local_begin, fecha_utc_end, fecha_local_end, latitud, longitud, energy_j, area_km2, quality, product_time,file_name) VALUES (%%s, %%s, %%s,%%s, %%s, %%s,%%s, %%s, %%s,%%s, %%s, %%s)"% "glm_product_flashes_con_key", (values))
                # db_cursor.close()
                # conn_db.commit()
                # conn_db.close()
            except:
                archivos_no_leidos.append(ar)
    
        # se creaq archivo que informa si hubo archivos que no se pudieron leer
        
        if len(archivos_no_leidos) >= 1:
            no_leidos = open(save_glm +'/no_leidos_' + f.strftime('%Y%m%d') + '.txt', "w")
            for ff in archivos_no_leidos:
                no_leidos.write(ff[ff.find('OR'):] + '\n')
            no_leidos.close()

        # se crea el archivo que dice si faltaron archivos en storinator
        if len(archivos) >= 4320:
            
            faltantes_storinator = open(save_glm +'/faltantes_storinator_' + f.strftime('%Y%m%d') + '.txt','w')       
            faltantes_storinator.write('ok')
            faltantes_storinator.close()
            
        else:
            
            fechas_in     = set([dt.datetime.strptime(d[d.find('G16_s')+5:d.find('G16_s')+18], '%Y%j%H%M%S') for d in archivos])
            fechas_needed = set(pd.date_range(f, f.strftime('%Y%m%d 23:59:40'), freq='20S').to_pydatetime())
            fechas_out    = list(fechas_needed - fechas_in)
            fechas_out.sort()
            
            faltantes_storinator = open(save_glm +'/faltantes_storinator_' + f.strftime('%Y%m%d') + '.txt', "w")
            for ff in fechas_out:
                fecha_str = ff.strftime('%Y%j%H%M%S')
                faltantes_storinator.write(fecha_str + '\n')
            faltantes_storinator.close()
            
    else:
        
        faltantes_storinator = open(save_glm + '/faltantes_storinator_' + f.strftime('%Y%m%d') + '.txt','w')       
        faltantes_storinator.write('Faltan todos')
        faltantes_storinator.close()
