import pandas as pd
import os
import glob 

fechas      = pd.date_range("2023-01-01", "2023-12-31", freq='D')
ruta_search = "/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/"
ruta_save   = "/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/archivos_recuperados/"

for f in fechas:

    found_files = glob.glob(ruta_search + "OR_GLM-L2-LCFA_G16_s" + f.strftime("%Y%j") + "*.nc")

    if len(found_files) >= 1:

        comand0 = "mkdir " + ruta_save + f.strftime("%Y%m%d")
        os.system(comand0)
        comand1 = "mv " + ruta_search + "OR_GLM-L2-LCFA_G16_s" + f.strftime("%Y%j") + "*.nc " + ruta_save + f.strftime("%Y%m%d/.")
        os.system(comand1)
        print(f)
