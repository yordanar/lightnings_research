import datetime as dt
import pandas as pd
import numpy as np
import subprocess
import os
import glob
import netCDF4 as nc
from netCDF4 import num2date
import MySQLdb
import sys
sys.path.insert(1,'/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes')
from credentials import  mysql_credentials_modify

# Funcion
def fecha_estandar(variable):#para el titulo de la figura, descomentar el segundo t_unit
    tname = variable
    nctime = nc_fid.variables[tname][:] # get values
    t_unit = nc_fid.variables[tname].units # get unit  "days since 1950-01-01T00:00:00Z"
    try :
        t_cal = nc_fid.variables[tname].calendar
    except AttributeError : # Attribute doesn't exist
        t_cal = u"standard" # or standard
    list_datevar = []
    if len(nctime)!=0:
        datevar = num2date(nctime,units = t_unit,calendar = t_cal)
        list_datevar.append(datevar)
    else:
        for i in nctime:
            datevar = num2date(i,units = t_unit,calendar = t_cal)
            list_datevar.append(datevar)
    return list_datevar

# credenciales para guardar los datos de flashes en la bd
mcm    = mysql_credentials_modify()
host   = mcm["host"]
user   = mcm["user"]
passwd = mcm["passwd"]
dbname = mcm["dbname"]

# Información sólo para Colombia
min_lat = -5
max_lat = 13
min_lon = -80
max_lon = -66

# rutas
ruta_aws                 = 'publicAWS:noaa-goes16/GLM-L2-LCFA/'
ruta_files_no_storinator = '/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/faltante_storinator/'
ruta_files_no_aws        = '/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/faltante_aws_sin_db/'
ruta_save                = '/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/archivos_recuperados/'


fechas = pd.date_range('2023-01-14', '2023-04-30', freq='D')

for f in fechas:
    
    print(f)

    # obtiene fechas en las que faltan archivos
    with open(ruta_files_no_storinator + 'faltantes_storinator_' + f.strftime('%Y%m%d') + '.txt') as k:
        fechas_faltantes = k.readlines()

    if fechas_faltantes[0].strip() != 'ok':

        if fechas_faltantes[0].strip() == 'Faltan todos':

            # ruta donde se guardan los datos de GLM
            ruta_save2 = ruta_save + f.strftime('%Y%m%d')
            os.system('mkdir ' + ruta_save2)
            fechas_recupera = pd.date_range(f, freq='20S', periods=4320)

        else:

            # ruta donde se guardan los datos de GLM
            ruta_save2 = ruta_save + f.strftime('%Y%m%d')
            os.system('mkdir ' + ruta_save2)
            fechas_recupera = [dt.datetime.strptime(ff.strip(), '%Y%j%H%M%S') for ff in fechas_faltantes]

        no_hallados_aws = []
        no_leidos       = []

        # recupera los datos desde aws
        for iiii, ff in enumerate(fechas_recupera):

            archivos  = subprocess.check_output('rclone lsf ' + ruta_aws + ff.strftime('%Y/%j/%H/') +  ' --include OR_GLM-L2-LCFA_G16_s' + ff.strftime('%Y%j%H%M%S') + '*.nc', shell=True)
            archivos  = archivos.decode()
            archivos  = archivos.split('\n')
            archivos  = archivos[:-1]

            if len(archivos) == 0:
                no_hallados_aws.append(ff)
                print('No recuperó', ff, iiii, len(fechas_recupera))
            else:
                print('Sí recuperó', ff, iiii, len(fechas_recupera))
                os.system('rclone copy ' + ruta_aws + ff.strftime('%Y/%j/%H/') + archivos[0] + ' ' + ruta_save2)

