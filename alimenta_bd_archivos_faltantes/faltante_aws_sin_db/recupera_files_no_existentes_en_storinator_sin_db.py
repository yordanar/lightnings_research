import datetime as dt
import pandas as pd
import numpy as np
import subprocess
import os
import glob
import netCDF4 as nc
from netCDF4 import num2date
import MySQLdb
import sys
sys.path.insert(1,'/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes')
from credentials import  mysql_credentials_modify

# Funcion
def fecha_estandar(variable):#para el titulo de la figura, descomentar el segundo t_unit
    tname = variable
    nctime = nc_fid.variables[tname][:] # get values
    t_unit = nc_fid.variables[tname].units # get unit  "days since 1950-01-01T00:00:00Z"
    try :
        t_cal = nc_fid.variables[tname].calendar
    except AttributeError : # Attribute doesn't exist
        t_cal = u"standard" # or standard
    list_datevar = []
    if len(nctime)!=0:
        datevar = num2date(nctime,units = t_unit,calendar = t_cal)
        list_datevar.append(datevar)
    else:
        for i in nctime:
            datevar = num2date(i,units = t_unit,calendar = t_cal)
            list_datevar.append(datevar)
    return list_datevar

# credenciales para guardar los datos de flashes en la bd
mcm    = mysql_credentials_modify()
host   = mcm["host"]
user   = mcm["user"]
passwd = mcm["passwd"]
dbname = mcm["dbname"]

# Información sólo para Colombia
min_lat = -5
max_lat = 13
min_lon = -80
max_lon = -66

# rutas
ruta_aws                 = 'publicAWS:noaa-goes16/GLM-L2-LCFA/'
ruta_files_no_storinator = '/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/faltante_storinator/'
ruta_files_no_aws        = '/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/faltante_aws_sin_db/'
ruta_save                = '/home/yuarangoj/investigacion_descargas/alimenta_bd_archivos_faltantes/archivos_recuperados/'


fechas = pd.date_range('2023-12-04', '2023-12-31', freq='D')

for f in fechas:
    
    print(f)

    # obtiene fechas en las que faltan archivos
    with open(ruta_files_no_storinator + 'faltantes_storinator_' + f.strftime('%Y%m%d') + '.txt') as k:
        fechas_faltantes = k.readlines()

    if fechas_faltantes[0].strip() != 'ok':

        if fechas_faltantes[0].strip() == 'Faltan todos':

            # ruta donde se guardan los datos de GLM
            ruta_save2 = ruta_save + f.strftime('%Y%m%d')
            os.system('mkdir ' + ruta_save2)
            fechas_recupera = pd.date_range(f, freq='20S', periods=4320)

        else:

            # ruta donde se guardan los datos de GLM
            ruta_save2 = ruta_save + f.strftime('%Y%m%d')
            os.system('mkdir ' + ruta_save2)
            fechas_recupera = [dt.datetime.strptime(ff.strip(), '%Y%j%H%M%S') for ff in fechas_faltantes]

        no_hallados_aws = []
        no_leidos       = []

        # recupera los datos desde aws
        for iiii, ff in enumerate(fechas_recupera):

            archivos  = subprocess.check_output('rclone lsf ' + ruta_aws + ff.strftime('%Y/%j/%H/') +  ' --include OR_GLM-L2-LCFA_G16_s' + ff.strftime('%Y%j%H%M%S') + '*.nc', shell=True)
            archivos  = archivos.decode()
            archivos  = archivos.split('\n')
            archivos  = archivos[:-1]

            if len(archivos) == 0:
                no_hallados_aws.append(ff)
                print('No recuperó', ff, iiii, len(fechas_recupera))
            else:
                print('Sí recuperó', ff, iiii, len(fechas_recupera))
                os.system('rclone copy ' + ruta_aws + ff.strftime('%Y/%j/%H/') + archivos[0] + ' ' + ruta_save2)

        # busca ruta de archivos de GLM recuperados
        files_recuperados = glob.glob(ruta_save2 + '/OR_GLM*.nc')
        files_recuperados.sort()

        # mete los datos de los archivos de GLM en la bd
        for iiii, ar in enumerate(files_recuperados):

            ########################################################################
            # INFO FLASHES ORGANIZADA
            ########################################################################
            
            try:
            
                nc_fid = nc.Dataset(ar)
                
                flash_lats    = nc_fid.variables['flash_lat'][:]
                flash_lons    = nc_fid.variables['flash_lon'][:]
                
                pos_lats   = np.where((min_lat < flash_lats) & (flash_lats < max_lat))[0]

                if len(pos_lats) != 0:
                    
                    flash_lats = flash_lats[pos_lats]
                    flash_lons = flash_lons[pos_lats]
                    
                    pos_lons   = np.where((min_lon < flash_lons) & (flash_lons < max_lon))[0]
                    
                    if len(pos_lons) != 0:
                        
                        flash_lats = flash_lats[pos_lons]
                        flash_lons = flash_lons[pos_lons]
                
                        flash_id      = nc_fid.variables['flash_id'][pos_lats][pos_lons]
                                
                        flash_UTC1    = fecha_estandar('flash_time_offset_of_first_event')[0][pos_lats][pos_lons]
                        flash_local1  = [base2 - dt.timedelta(hours=5) for base2 in flash_UTC1]
                        
                        flash_UTC2    = fecha_estandar('flash_time_offset_of_last_event')[0][pos_lats][pos_lons]
                        flash_local2  = [base2 - dt.timedelta(hours=5) for base2 in flash_UTC2]
                        
                        flash_energy  =  nc_fid.variables['flash_energy'][:][pos_lats][pos_lons]
                        flash_area    =  nc_fid.variables['flash_area'][:][pos_lats][pos_lons]
                        flash_quality = nc_fid.variables['flash_quality_flag'][:][pos_lats][pos_lons]
                        
                        nctime = nc_fid.variables['product_time'][0] # get values
                        t_unit = nc_fid.variables['product_time'].units # get unit  "days since 1950-01-01T00:00:00Z"
                        try:
                            t_cal = nc_fid.variables['product_time'].calendar
                        except AttributeError : # Attribute doesn't exist
                            t_cal = u"standard" # or standard
                        list_datevar = []
                        product_time = num2date(nctime, units=t_unit, calendar=t_cal)
                        #product_time = fecha_estandar('product_time')[0]
                        file_name = ar[ar.find('OR'):]
                        
                        "Inserta los datos en la base de datos glm_product_flashes_con_key"
                        conn_db   = MySQLdb.connect (host, user, passwd, dbname)
                        values    = zip(flash_id,flash_UTC1,flash_local1,flash_UTC2, flash_local2, flash_lats, flash_lons, flash_energy, flash_area, flash_quality,[product_time] * len(flash_lats), [file_name] *len(flash_lats))
                        db_cursor = conn_db.cursor ()
                        db_cursor.executemany("INSERT IGNORE INTO %s (flash_id, fecha_utc_begin, fecha_local_begin, fecha_utc_end, fecha_local_end, latitud, longitud, energy_j, area_km2, quality, product_time,file_name) VALUES (%%s, %%s, %%s,%%s, %%s, %%s,%%s, %%s, %%s,%%s, %%s, %%s)"% "glm_product_flashes_con_key", (values))
                        db_cursor.close()
                        conn_db.commit()
                        conn_db.close()

                print(u'Se insertó bien el dato', f, iiii, len(fechas_recupera))

            except:
                print(u'NO se insertó el dato', f, iiii, len(fechas_recupera))
                no_leidos.append(ar)

        if len(no_leidos) >= 1: # si hubo archivos que se recuperaron pero no pudieron ser leídos por estar dañados
            not_read = open(ruta_files_no_aws +'/no_leidos_' + f.strftime('%Y%m%d') + '.txt', "w")
            for ff in no_leidos:
                not_read.write(ff + '\n')
            not_read.close()
        
        if len(no_hallados_aws) >= 1: # si hubo datos que no se encontraron en aws
            faltantes_aws = open(ruta_files_no_aws +'/faltantes_aws_' + f.strftime('%Y%m%d') + '.txt', "w")
            for ff in no_hallados_aws:
                fecha_str = ff.strftime('%Y%j%H%M%S')
                faltantes_aws.write(fecha_str + '\n')
            faltantes_aws.close()
        else:
            faltantes_aws = open(ruta_files_no_aws +'/faltantes_aws_' + f.strftime('%Y%m%d') + '.txt', "w")
            faltantes_aws.write('ok')
            faltantes_aws.close()
            
        #os.system('rm -r ' + ruta_save2)
        
    else:
        
        faltantes_aws = open(ruta_files_no_aws +'/faltantes_aws_' + f.strftime('%Y%m%d') + '.txt', "w")
        faltantes_aws.write('No faltaba ninguno en storinator')
        faltantes_aws.close()

