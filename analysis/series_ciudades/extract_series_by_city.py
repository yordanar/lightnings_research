import pandas as cv
import geopandas as gpd
from shapely.geometry import Point
import numpy as np
import glob
import pandas as pd
import datetime as dt
from dateutil.relativedelta import relativedelta

path_shape = "/home/storm/Shapes/med_cen.shp"
path_files = "/home/storm/lightnings_research/archivos_data/colombia_by_month_calidad/"
city       = "medellin_centro"

################################ read the shape
polygons = gpd.GeoDataFrame.from_file(path_shape)

for geom in polygons.geometry:
    if geom.type == 'Polygon':  # For Polygon geometries
        coords = np.array(list(geom.exterior.coords))
        latmax = np.max(coords[:, 1])
        latmin = np.min(coords[:, 1])
        lonmax = np.max(coords[:, 0])
        lonmin = np.min(coords[:, 0])
    elif geom.type == 'MultiPolygon':  # For MultiPolygon geometries
        for poly in geom:
            coords = list(poly.exterior.coords)
            latmax = np.max(coords[:, 1])
            latmin = np.min(coords[:, 1])
            lonmax = np.max(coords[:, 0])
            lonmin = np.min(coords[:, 0])

################################ read files

files = glob.glob(path_files + "GLMflashes_*.csv")
files.sort()

for count, f in enumerate(files):
    
    # fechas
    fecha       = f[f.find("-66E")+5:f.find("-66E")+5+6]
    fecha       = dt.datetime.strptime(fecha, "%Y%m")
    print(fecha)
    
    # read the file
    df          = pd.read_csv(f, index_col="date")
    index_df    = pd.DatetimeIndex(df.index)
    df.index    = index_df
    
    # clip a square around the shape
    clipped_data = df.query(str(latmin) + " <= latitude & latitude <= " + str(latmax) + " & " + str(lonmin) + " <= longitude & longitude <= " + str(lonmax) )
    
    # extract the lightnings inside the shape
    crs      = None

    Points   = gpd.GeoDataFrame({'Lat': clipped_data.latitude.values, 'Lon': clipped_data.longitude.values})
    geometry = [Point(xy) for xy in zip(Points.Lon, Points.Lat)]
    geo_P    = gpd.GeoDataFrame(Points, crs=crs, geometry=geometry)
    pts      = geo_P.copy()
    
    ######## Listas que se van a llenar
    
    lat_city    = []
    lon_city    = []
    fechas_city = []
    
    for i, poly in polygons.iterrows():
        for j, pt in pts.iterrows():
            if poly.geometry.contains(pt.geometry):
                lat_city.append(pt.Lat)
                lon_city.append(pt.Lon)
                fechas_city.append(clipped_data.index[j])
                pts = pts.drop([j])
    
    fechas_city = pd.DatetimeIndex(fechas_city)
    
    # contsruye la serie
    
    lightnings_city  = pd.DataFrame(index=fechas_city, data = {"counting":np.ones(len(fechas_city))})
    daily_sum        = lightnings_city.resample('D').sum()
    full_month_range = pd.date_range(start=fecha.strftime("%Y-m-") + "01", end=(fecha + relativedelta(months=1) - relativedelta(days=1)).strftime("%Y-%m-%d"), freq='D')
    daily_lightnings = daily_sum.reindex(full_month_range, fill_value=0)
    
    # Junta los resultados de cada archivo
    if count != 0:
        DAILY_LIGHTNINGS = pd.concat([DAILY_LIGHTNINGS, daily_lightnings])
    else:
        DAILY_LIGHTNINGS = daily_lightnings


# Guarda
DAILY_LIGHTNINGS.to_csv("/home/storm/lightnings_research/analysis/series_ciudades/" + city + ".csv")

