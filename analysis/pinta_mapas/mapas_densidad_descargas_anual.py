import numpy as np
import glob
import pandas as pd
import datetime as dt
import pickle
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.feature import ShapelyFeature
from cartopy.io.shapereader import Reader
import cartopy.feature as cfeature
import netCDF4 as nc
import colorcet as cc
from sklearn.preprocessing import MinMaxScaler

num_anios            = 5 # numero de años a analizar
tamanio_pixel        = 8 # resolucion de pixeles en kilómetros
lat_start, lat_end = -5, 13
lon_start, lon_end = -80, -66
lat_min, lat_max   =   4,  9.23
lon_min, lon_max   = -78, -72
figura_save          = "/home/yuarangoj/investigacion_descargas/imagenes/anual_mean/"
shape_departamentos  = "/home/yuarangoj/Shapes/Departamentos.shp"
shape_paises         = "/home/yuarangoj/Shapes/World_Countries.shp"
shape_va             = "/home/yuarangoj/Shapes/Amva.shp"

"functions"

"create grid of pixels of 8 kilometers"
def grid(res=8):
    # Define the resolution
    resolution_km = res
    earth_circumference_km = 40075
    degree_to_km = earth_circumference_km / 360
    # Calculate the number of pixels
    lat_pixels = round((lat_end - lat_start) * degree_to_km / resolution_km)
    lon_pixels = round((lon_end - lon_start) * degree_to_km / resolution_km)
    # Generate the grids
    lats = np.linspace(lat_start, lat_end, lat_pixels)
    lons = np.linspace(lon_start, lon_end, lon_pixels)
    # Create a meshgrid
    lon_grid, lat_grid = np.meshgrid(lons, lats)
    return lat_grid, lon_grid

# lectura de topografía
topo_data            = nc.Dataset("/home/yuarangoj/Topografia/elev.americas.5-min.nc")
topografia           = topo_data["data"][0]
lat_topo             = topo_data["lat"][:]
lon_topo             = topo_data["lon"][:] # se hace esta operacion
topografia           = topografia[(lat_start <= lat_topo) & (lat_topo <= lat_end)]
topografia           = topografia[:, ((360 + lon_start) <= lon_topo) & (lon_topo <= (360 + lon_end))]
lat_topo             = lat_topo[(lat_start <= lat_topo) & (lat_topo <= lat_end)]
lon_topo             = lon_topo[((360 + lon_start) <= lon_topo) & (lon_topo <= (360 + lon_end))] # se suma 360 para que los datos queden en grados positivos este, que es como están los datos del netcdf
lons_topo, lats_topo = np.meshgrid(lon_topo, lat_topo)

"lectura datos de lat,lon de la region de Antioquia extendida"
lats_malla      = pickle.load(open("/home/satelital/investigacion/storm_tracking/code_optimized/malla_lats_region.bin", "rb"))
lons_malla      = pickle.load(open("/home/satelital/investigacion/storm_tracking/code_optimized/malla_lons_region.bin", "rb"))

"hace un conteo de 4 años desde 2019 a 2022"
cont = 0
for y in range(2018, 2023):
    
    files = glob.glob("/home/yuarangoj/investigacion_descargas/analysis/mallas_conteos/colombia_8km_calidad/malla_conteo_" + str(y) + "*.bin")
    files.sort()
    
    for f in files:
        if cont == 0:
            conteo = pickle.load(open(f, "rb"))
        else:
            conteo = conteo + pickle.load(open(f, "rb"))
        cont = cont + 1

"calcula la tasa de densidad de descargas promedio anual"  
FRD = conteo / (num_anios * (tamanio_pixel**2))  # se divide por 4 que es el número de años, luego las unidades quedan como descargas/km2 año 
lat_grid, lon_grid = grid(tamanio_pixel)

# pinta
plt.close('all')
fig = plt.figure(figsize=(12, 10)) # , facecolor='none',frameon=False)
ax1 = fig.add_axes([0, 0, 1, 1], projection=ccrs.Mercator())
#gl  = ax1.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=0.6, color='k', linestyle='--', xlocs=np.arange(lon_start, lon_end, 3), ylocs=np.arange(lat_start, lat_end, 3))
#gl  = ax1.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=0.6, color='k', linestyle='--', xlocs=np.arange(lon_min, lon_max, 1), ylocs=np.arange(lat_min, lat_max, 1))
gl  = ax1.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=0.6, color='k', linestyle='--', xlocs=np.arange(-76, -75, 0.2), ylocs=np.arange(5.8, 6.8, 0.2))
gl.ylabel_style = {'size':22}
gl.xlabel_style = {'size':22}
gl.top_labels   = False
gl.right_labels = False
#ax1.set_extent([np.min(lon_grid), np.max(lon_grid), np.min(lat_grid), np.max(lat_grid)], ccrs.PlateCarree())
#ax1.set_extent([np.min(lons_malla), np.max(lons_malla), np.min(lats_malla), np.max(lats_malla)], ccrs.PlateCarree())
ax1.set_extent([-76, -75, 5.8, 6.8], ccrs.PlateCarree())
shape_feature = ShapelyFeature(Reader(shape_departamentos).geometries(), ccrs.PlateCarree(), facecolor='none', edgecolor ='k')
#shape_feature2= ShapelyFeature(Reader(shape_paises).geometries(), ccrs.PlateCarree(), facecolor='none', edgecolor ='k')
shape_feature3= ShapelyFeature(Reader(shape_va).geometries(), ccrs.PlateCarree(), facecolor='none', edgecolor ='k')

ax1.add_feature(shape_feature, edgecolor='k', linewidth=1.)
#ax1.add_feature(shape_feature2, edgecolor='k', linewidth=1.)
ax1.add_feature(shape_feature3, edgecolor='k', linewidth=1.)
#ax1.add_feature(cfeature.RIVERS, color='#85929e', linewidth=3.)

VMAX   = np.nanmean(FRD) + 6*np.nanstd(FRD)
VMAX   = 90
bounds = np.linspace(0, VMAX, 21)
values = [(bounds[i]+bounds[i+1])/2 for i in range(len(bounds)-1)]
values = np.array(values)

P = ax1.pcolormesh(lon_grid, lat_grid, FRD, cmap=cc.cm.CET_R1, transform=ccrs.PlateCarree(), vmin=0, vmax=VMAX) #, extend='max')
#CS = ax1.contour(lons_topo-360, lats_topo, topografia, levels=[1000], colors=["#bfc9ca"], transform=ccrs.PlateCarree(), linewidths=2) #), linestyles="dashdot"
#CS = ax1.contour(lons_topo-360, lats_topo, topografia, levels=[1000], colors=["#00dae5"], transform=ccrs.PlateCarree(), linewidths=2) #), linestyles="dashdot"

ax1.set_title("Tasa de densidad de descargas (TDD) promedio anual\n(2018-2022)", fontsize=22) #, color='#616a6b') #, x=0.7, y=0.04)

cb = plt.colorbar(P, ax=ax1, shrink=0.8,pad=0.015, extend="max", boundaries=bounds, values=values, format='%1.1f')
cb.ax.tick_params(labelsize=22)
#cb.ax.set_yticks(np.arange(0, 11))
#cb.ax.set_yticklabels(np.arange(0, 11))
cb.ax.set_ylabel(u"TDD [Descargas$\cdot km^{-2} \cdot año^{-1}$]", size=22)
#ax1.clabel(CS, inline=True, fmt='%1.0f', fontsize=14)

#plt.savefig(figura_save + 'TDD_anual_mean_Colombia_' + str(tamanio_pixel) + 'km_calidad.png', dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)
#plt.savefig(figura_save + 'TDD_anual_mean_Antioquia_ext_' + str(tamanio_pixel) + 'km_calidad.png', dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)
plt.savefig(figura_save + 'TDD_anual_mean_VA_' + str(tamanio_pixel) + 'km_calidad.png', dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)
plt.savefig(figura_save + 'TDD_anual_mean_VA_' + str(tamanio_pixel) + 'km_calidad.pdf', dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)

plt.close('all')
