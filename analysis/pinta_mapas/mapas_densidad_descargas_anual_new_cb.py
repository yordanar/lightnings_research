import numpy as np
import glob
import pandas as pd
import datetime as dt
import pickle
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.feature import ShapelyFeature
from cartopy.io.shapereader import Reader
import cartopy.feature as cfeature
import netCDF4 as nc
import colorcet as cc
from sklearn.preprocessing import MinMaxScaler

num_anios            = 5 # numero de años a analizar
tamanio_pixel        = 8 # resolucion de pixeles en kilómetros
lat_start, lat_end = -5, 13
lon_start, lon_end = -80, -66
lat_min, lat_max   =   4,  9.23
lon_min, lon_max   = -78, -72
figura_save          = "/home/storm/lightnings_research/imagenes/anual_mean/"
shape_departamentos  = "/home/storm/Shapes/Departamentos.shp"
shape_paises         = "/home/storm/Shapes/World_Countries.shp"
shape_va             = "/home/storm/Shapes/Amva.shp"

"functions"

"create grid of pixels of 8 kilometers"
def grid(res=8):
    # Define the resolution
    resolution_km = res
    earth_circumference_km = 40075
    degree_to_km = earth_circumference_km / 360
    # Calculate the number of pixels
    lat_pixels = round((lat_end - lat_start) * degree_to_km / resolution_km)
    lon_pixels = round((lon_end - lon_start) * degree_to_km / resolution_km)
    # Generate the grids
    lats = np.linspace(lat_start, lat_end, lat_pixels)
    lons = np.linspace(lon_start, lon_end, lon_pixels)
    # Create a meshgrid
    lon_grid, lat_grid = np.meshgrid(lons, lats)
    return lat_grid, lon_grid

# lectura de topografía
topo_data            = nc.Dataset("/home/storm/Topografia/elev.americas.5-min.nc")
topografia           = topo_data["data"][0]
lat_topo             = topo_data["lat"][:]
lon_topo             = topo_data["lon"][:] # se hace esta operacion
topografia           = topografia[(lat_start <= lat_topo) & (lat_topo <= lat_end)]
topografia           = topografia[:, ((360 + lon_start) <= lon_topo) & (lon_topo <= (360 + lon_end))]
lat_topo             = lat_topo[(lat_start <= lat_topo) & (lat_topo <= lat_end)]
lon_topo             = lon_topo[((360 + lon_start) <= lon_topo) & (lon_topo <= (360 + lon_end))] # se suma 360 para que los datos queden en grados positivos este, que es como están los datos del netcdf
lons_topo, lats_topo = np.meshgrid(lon_topo, lat_topo)

"lectura datos de lat,lon de la region de Antioquia extendida"
#lats_malla      = pickle.load(open("/home/satelital/investigacion/storm_tracking/code_optimized/malla_lats_region.bin", "rb"))
#lons_malla      = pickle.load(open("/home/satelital/investigacion/storm_tracking/code_optimized/malla_lons_region.bin", "rb"))

"hace un conteo de 4 años desde 2019 a 2022"
cont = 0
for y in range(2018, 2023):
    
    files = glob.glob("/home/storm/lightnings_research/analysis/mallas_conteos/colombia_8km_calidad/malla_conteo_" + str(y) + "*.bin")
    files.sort()
    
    for f in files:
        if cont == 0:
            conteo = pickle.load(open(f, "rb"))
        else:
            conteo = conteo + pickle.load(open(f, "rb"))
        cont = cont + 1

"calcula la tasa de densidad de descargas promedio anual"  
FRD = conteo / (num_anios * (tamanio_pixel**2))  # se divide por 4 que es el número de años, luego las unidades quedan como descargas/km2 año 
lat_grid, lon_grid = grid(tamanio_pixel)

# pinta


plt.close('all')
fig = plt.figure(figsize=(12, 10)) # , facecolor='none',frameon=False)
ax1 = fig.add_axes([0, 0, 1, 1], projection=ccrs.Mercator())
gl  = ax1.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=0.6, color='k', linestyle='--', xlocs=np.arange(lon_start, lon_end, 3), ylocs=np.arange(lat_start, lat_end, 3))
#gl  = ax1.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=0.6, color='k', linestyle='--', xlocs=np.arange(lon_min, lon_max, 1), ylocs=np.arange(lat_min, lat_max, 1))
#gl  = ax1.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=0.6, color='k', linestyle='--', xlocs=np.arange(-76, -75, 0.2), ylocs=np.arange(5.8, 6.8, 0.2))
gl.ylabel_style = {'size':22}
gl.xlabel_style = {'size':22}
gl.top_labels   = False
gl.right_labels = False
ax1.set_extent([np.min(lon_grid), np.max(lon_grid), np.min(lat_grid), np.max(lat_grid)], ccrs.PlateCarree())
#ax1.set_extent([np.min(lons_malla), np.max(lons_malla), np.min(lats_malla), np.max(lats_malla)], ccrs.PlateCarree())
#ax1.set_extent([-76, -75, 5.8, 6.8], ccrs.PlateCarree())
shape_feature = ShapelyFeature(Reader(shape_departamentos).geometries(), ccrs.PlateCarree(), facecolor='none', edgecolor ='k')
shape_feature2= ShapelyFeature(Reader(shape_paises).geometries(), ccrs.PlateCarree(), facecolor='none', edgecolor ='k')
shape_feature3= ShapelyFeature(Reader(shape_va).geometries(), ccrs.PlateCarree(), facecolor='none', edgecolor ='k')

ax1.add_feature(shape_feature, edgecolor='k', linewidth=1.)
ax1.add_feature(shape_feature2, edgecolor='k', linewidth=1.)
ax1.add_feature(shape_feature3, edgecolor='k', linewidth=1.)
#ax1.add_feature(cfeature.RIVERS, color='#85929e', linewidth=3.)


VMAX   = 95
VMIN   = 0.0
bounds = np.array([VMIN,  0.1, 1,   2,   4, 6, 8, 10, 12, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 70, 80, VMAX])


# Normalize data
data_reshaped    = FRD.flatten().reshape(-1, 1)
data_normalized  = data_reshaped/VMAX

# Categorize data into bins
data_categorized = np.digitize(data_normalized.flatten(), bounds/VMAX)
data_categorized = data_categorized -1

P = ax1.pcolormesh(lon_grid, lat_grid, data_categorized.reshape(FRD.shape), cmap=cc.cm.CET_C6s_r, transform=ccrs.PlateCarree())

CS = ax1.contour(lons_topo-360, lats_topo, topografia, levels=[1000], colors=["#bfc9ca"], transform=ccrs.PlateCarree(), linewidths=2) #), linestyles="dashdot"

ax1.set_title("Lightning density rate (LDR) anual mean\n(2018-2023)", fontsize=22) #, color='#616a6b') #, x=0.7, y=0.04)

#cb = plt.colorbar(P, ax=ax1, shrink=0.8,pad=0.015, extend="max", ticks=np.arange(len(bounds)), format='%1.0f')
cb = plt.colorbar(P, ax=ax1, shrink=0.95,pad=0.015, extend="max", ticks=np.arange(len(bounds)), format='%1.0f')
cb.ax.set_yticklabels(bounds)
cb.ax.tick_params(labelsize=22)
cb.ax.set_ylabel(u"LDR [Lightnings$\cdot km^{-2} \cdot year^{-1}$]", size=22)


plt.savefig(figura_save + 'TDD_anual_mean_Colombia_' + str(tamanio_pixel) + 'km_calidad_new_cb.png', dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)
#plt.savefig(figura_save + 'TDD_anual_mean_Antioquia_ext_' + str(tamanio_pixel) + 'km_calidad_new_cb.png', dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)
#plt.savefig(figura_save + 'TDD_anual_mean_VA_' + str(tamanio_pixel) + 'km_calidad.png', dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)

plt.close('all')




