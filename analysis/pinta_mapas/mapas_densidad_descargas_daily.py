import numpy as np
import glob
import pandas as pd
import datetime as dt
import pickle
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
from cartopy.feature import ShapelyFeature
from cartopy.io.shapereader import Reader
import cartopy.feature as cfeature
import netCDF4 as nc
import colorcet as cc
import sys
sys.path.insert(1, '/home/yuarangoj/investigacion_descargas/analysis')
import cartopy_grid_plots as cgp # modulo de Mantilla

num_anios            = 5 # numero de años a analizar
tamanio_pixel        = 8 # resolucion de pixeles en kilómetros
lat_start, lat_end = -5, 13
lon_start, lon_end = -80, -66
lat_min, lat_max   =   4,  9.23
lon_min, lon_max   = -78, -72
figura_save          = "/home/yuarangoj/investigacion_descargas/imagenes/diurnal_mean/"
shape_departamentos  = "/home/yuarangoj/Shapes/Departamentos.shp"
shape_paises         = "/home/yuarangoj/Shapes/World_Countries.shp"
shape_va             = "/home/yuarangoj/Shapes/Amva.shp"

"functions"

"create grid of pixels of 8 kilometers"
def grid(res=8):
    # Define the resolution
    resolution_km = res
    earth_circumference_km = 40075
    degree_to_km = earth_circumference_km / 360
    # Calculate the number of pixels
    lat_pixels = round((lat_end - lat_start) * degree_to_km / resolution_km)
    lon_pixels = round((lon_end - lon_start) * degree_to_km / resolution_km)
    # Generate the grids
    lats = np.linspace(lat_start, lat_end, lat_pixels)
    lons = np.linspace(lon_start, lon_end, lon_pixels)
    # Create a meshgrid
    lon_grid, lat_grid = np.meshgrid(lons, lats)
    return lat_grid, lon_grid

# lectura de topografía
topo_data            = nc.Dataset("/home/yuarangoj/Topografia/elev.americas.5-min.nc")
topografia           = topo_data["data"][0]
lat_topo             = topo_data["lat"][:]
lon_topo             = topo_data["lon"][:] # se hace esta operacion
topografia           = topografia[(lat_start <= lat_topo) & (lat_topo <= lat_end)]
topografia           = topografia[:, ((360 + lon_start) <= lon_topo) & (lon_topo <= (360 + lon_end))]
lat_topo             = lat_topo[(lat_start <= lat_topo) & (lat_topo <= lat_end)]
lon_topo             = lon_topo[((360 + lon_start) <= lon_topo) & (lon_topo <= (360 + lon_end))] # se suma 360 para que los datos queden en grados positivos este, que es como están los datos del netcdf
lons_topo, lats_topo = np.meshgrid(lon_topo, lat_topo)

"lectura datos de lat,lon de la region de Antioquia extendida"
lats_malla         = pickle.load(open("/home/satelital/investigacion/storm_tracking/code_optimized/malla_lats_region.bin", "rb"))
lons_malla         = pickle.load(open("/home/satelital/investigacion/storm_tracking/code_optimized/malla_lons_region.bin", "rb"))
lat_grid, lon_grid = grid(tamanio_pixel)

"hace un conteo de 5 años desde 2018 a 2022"

conteo_monthly_diurnal = np.zeros([12, 24] + list(np.array(lat_grid.shape)-1))   # 12 meses x 24 horas

for i, m in enumerate(range(1, 13)):
    
    files = glob.glob("/home/yuarangoj/investigacion_descargas/analysis/mallas_conteos/colombia_8km_calidad_diurnal_monthly/malla_conteo_20*" + "%.2d" % m + ".bin")
    
    for f in files:
        malla_conteo = pickle.load(open(f, "rb"))
        for h in range(0, 24):
            conteo_monthly_diurnal[i, h] = conteo_monthly_diurnal[i, h] + malla_conteo[h]

# se pasa a hora local
conteo_monthly_diurnal_lt = np.roll(conteo_monthly_diurnal, -5, axis=1) # -5 por las cinco horas de diferencia, es decir UTC-5

# hace un conteo diurno pero por bloques de dos horas
conteo_diurno = np.zeros([12] + list(np.array(lat_grid.shape)-1))
for l, h in enumerate(range(12)):  
    conteo_diurno[l] = np.sum(conteo_monthly_diurnal_lt[:, l*2], axis=0) + np.sum(conteo_monthly_diurnal_lt[:, l*2+1], axis=0)     
    
area         = tamanio_pixel**2
ciclo_diurno = (conteo_diurno * 12) / (num_anios * area)

####################### ciclo diurno

"crea el gráfico"
proj = ccrs.PlateCarree() # Define the map projection (PlateCarree) and set the image extent
#img_extent = [np.min(lons_malla), np.max(lons_malla), np.min(lats_malla), np.max(lats_malla)]
img_extent = [np.min(lon_grid), np.max(lon_grid), np.min(lat_grid), np.max(lat_grid)]

# Define the grid size (number of rows and columns)
num_rows    = 2
num_columns = 6

# Define properties for the grid
horiz_spacing = 0.015
vert_spacing  = 0.05

# Define font properties for axis labels and title
font_prop = {'fontsize': 12, 'fontweight': 'semibold', 'color': '#434343'}
font_prop_title = {'fontsize': 12,
                    'fontweight': 'semibold', 'color': '#434343'}

# Define the contour levels for the temperature plot
#VMAX   = 90
VMAX   = 250

# Use the function to calculate properties of the grid
grid_prop = x_coords, y_coords, x_fig, y_fig = cgp.define_grid_fig(
    num_rows, num_columns,
    horiz_spacing=horiz_spacing, vert_spacing=vert_spacing)

# Create a figure with a specified size
fig = plt.figure(figsize=(12, 4.5))
#fig = plt.figure(figsize=(12, 3.5))

# Initialize the index for selecting time slices of the temperature data
idx = 0

# Loop through each row and column to create a grid of subplots
for fi in range(num_rows):
    for ci in range(num_columns):
        # Add axes to the figure with the calculated properties
        ax = fig.add_axes([x_coords[ci], y_coords[fi],
                            x_fig, y_fig],
                            projection=proj)
        # Add geographic features to the plot
        # Set the image extent and aspect ratio of the plot and others
        gl  = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=0.6, color='k', linestyle='--', xlocs=np.arange(lon_start, lon_end, 3), ylocs=np.arange(lat_start, lat_end, 3))
        #gl  = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=0.6, color='k', linestyle='--', xlocs=np.arange(lon_min, lon_max, 1), ylocs=np.arange(lat_min, lat_max, 1))
        #gl.ylabel_style = {'size':22}
        gl.top_labels   = False
        gl.right_labels = False
        #gl.xlabel_style = {'size':22}
        ax.set_extent(img_extent, proj)
        ax.set_aspect('auto')
        shape_feature = ShapelyFeature(Reader(shape_departamentos).geometries(), ccrs.PlateCarree(), facecolor='none', edgecolor ='k')
        shape_feature2= ShapelyFeature(Reader(shape_paises).geometries(), ccrs.PlateCarree(), facecolor='none', edgecolor ='k')
        #shape_feature3= ShapelyFeature(Reader(shape_va).geometries(), ccrs.PlateCarree(), facecolor='none', edgecolor ='k')

        ax.add_feature(shape_feature, edgecolor='k', linewidth=0.5)
        ax.add_feature(shape_feature2, edgecolor='k', linewidth=0.5)
        #ax.add_feature(shape_feature3, edgecolor='k', linewidth=0.5)
        #ax.add_feature(cfeature.RIVERS, color='#b1b7bc', linewidth=0.5)

        # Remove y-axis labels for subplots that aren't in the first column
        if ci > 0:
            gl.left_labels = False

        # Remove x-axis labels for subplots that are not in the last row
        if fi < (num_rows - 1):
            gl.bottom_labels = False
            
        # Plot the data
        P  = ax.pcolormesh(lon_grid, lat_grid, ciclo_diurno[idx], vmin=0, vmax=VMAX, cmap=cc.cm.CET_R1, transform=ccrs.PlateCarree()) # ) #, extend='max')
        #CS = ax.contour(lons_topo-360, lats_topo, topografia, levels=[1000], colors=["#bfc9ca"], transform=ccrs.PlateCarree(), linewidths=0.6) #)
        #CS = ax.contour(lons_topo-360, lats_topo, topografia, levels=[1000], colors=["#00dae5"], transform=ccrs.PlateCarree(), linewidths=0.6) #), linestyles="dashdot"        #ax.clabel(CS, inline=True, fmt='%1.0f', fontsize=10)

        # Add a title to each subplot
        ax.set_title("%.2d" % (idx*2) + ":00 - " + "%.2d" % (idx*2+2) + ":00 (LT)",
                        fontdict=font_prop_title, pad=0.2)
        
        # Increment the index to move to the next time slice
        idx += 1

# Define the orientation and label of the colorbar
orientation = 'horizontal'
label = u'TDD [Descargas$\cdot km^{-2} \cdot año^{-1}$]\n[$10^{-3}$ x Descargas$\cdot km^{-2} \cdot hora^{-1}$]'

# Add a colorbar to the figure
cb_ticks = np.linspace(0, VMAX, 11)
cb_ticklabels = ["%.2d" % cbt + "\n" + str(round(1000 * cbt/(365*24), 2)) for cbt in cb_ticks]

cgp.add_colorbar(fig=fig, cs=P, label=label,
                orientation=orientation,
                grid_prop=grid_prop,
                cbar_factor=0.8,
                cbar_width=0.025, y_coord_cbar=-0.08, my_own_ticks=True, cb_ticks=cb_ticks, cb_ticklabels=cb_ticklabels)

#plt.savefig(figura_save + 'TDD_diurnal_mean_Antioquia_extend_solo_topo_' + str(tamanio_pixel) + 'km_2.png', dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)
#plt.savefig(figura_save + 'TDD_diurnal_mean_Antioquia_extend_solo_topo_' + str(tamanio_pixel) + 'km_2.pdf', dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)
plt.savefig(figura_save + 'TDD_diurnal_mean_Colombia_extend_' + str(tamanio_pixel) + 'km_2.png', dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)
plt.savefig(figura_save + 'TDD_diurnal_mean_Colombia_extend_' + str(tamanio_pixel) + 'km_2.pdf', dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)

#plt.savefig(figura_save + 'TDD_diurnal_mean_Antioquia_extend_' + str(tamanio_pixel) + 'km_1.png', dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)
#plt.savefig(figura_save + 'TDD_diurnal_mean_Antioquia_extend_' + str(tamanio_pixel) + 'km_1.pdf', dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)
#plt.savefig(figura_save + 'TDD_diurnal_mean_Colombia_extend_solo_topo_' + str(tamanio_pixel) + 'km_1.png', dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)
#plt.savefig(figura_save + 'TDD_diurnal_mean_Colombia_extend_solo_topo_' + str(tamanio_pixel) + 'km_1.pdf', dpi=200, bbox_inches='tight', pad_inches=0) #, transparent=True,)

plt.close("all")
