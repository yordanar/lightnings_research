import pandas as pd
import numpy as np
import datetime as dt
import glob
import pickle
import geopandas as gpd
from shapely.geometry import Point

################### datos de la region
area_municipios = [206, 78, 70, 142.36, 380.64, 21.09, 78.78, 35, 15, 135] # Barbosa, Girardota, Copacabana, Bello, Medellín, Itagui, Envigado, La Estrella, Sabaneta, Caldas
municipios      = ['Barbosa','Girardota','Copacabana','Bello',u'Medellín',u'Itaguí','Envigado','La Estrella', 'Sabaneta', 'Caldas']

latmax = 6.6
latmin = 5.9
lonmax = -75.2
lonmin = -75.8

#####################  functions

"create grid of pixels of 8 kilometers"
def grid(res=8):
    # Define the boundaries
    lat_start, lat_end = -5, 13
    lon_start, lon_end = -80, -66
    # Define the resolution
    resolution_km = res
    earth_circumference_km = 40075
    degree_to_km = earth_circumference_km / 360
    # Calculate the number of pixels
    lat_pixels = round((lat_end - lat_start) * degree_to_km / resolution_km)
    lon_pixels = round((lon_end - lon_start) * degree_to_km / resolution_km)
    # Generate the grids
    lats = np.linspace(lat_start, lat_end, lat_pixels)
    lons = np.linspace(lon_start, lon_end, lon_pixels)
    # Create a meshgrid
    lon_grid, lat_grid = np.meshgrid(lons, lats)
    return lat_grid, lon_grid

############## crea malla
resolution = 2 # kms
lat_grid, lon_grid = grid(resolution)

############# shape VA
ruta_shape = '/home/storm/Shapes/mpios_alt.shp'
polygons = gpd.GeoDataFrame.from_file(ruta_shape)

####################### lectura de datos de descargas

files = glob.glob("/home/storm/lightnings_research/archivos_data/colombia_by_month_calidad/GLMflashes_*.csv")
files.sort()

data_mes  = {}

for f in files:
    
    fecha       = f[f.find("-66E")+5:f.find("-66E")+5+6]
    fecha       = dt.datetime.strptime(fecha, "%Y%m")
    print(fecha)
    
    grid_conteo = np.zeros([24] + list(np.array(lat_grid.shape)-1))
    
    df          = pd.read_csv(f, index_col="date")
    index_df    = pd.DatetimeIndex(df.index)

    df_selected = df[(df['latitude'] >= latmin) & (df['latitude'] < latmax) & (df['longitude'] >= lonmin) & (df['longitude'] < lonmax)]
    Lats        = df_selected["latitude"].values
    Lons        = df_selected["longitude"].values
    
    crs = None
    Points=gpd.GeoDataFrame({'Lat':Lats,'Lon':Lons})
    geometry = [Point(xy) for xy in zip(Points.Lon, Points.Lat)]
    geo_P = gpd.GeoDataFrame(Points, crs=crs, geometry=geometry)
    pts = geo_P.copy()

    LATITUDES  = []
    LONGITUDES = []
    
    for i, poly in polygons.iterrows():

        latitudes    = []
        longitudes   = []
        
        for j, pt in pts.iterrows():

            if poly.geometry.contains(pt.geometry):

                latitudes.append(Lats[j])
                longitudes.append(Lons[j])
                LATITUDES.append(Lats[j])
                LONGITUDES.append(Lons[j])
                pts = pts.drop([j])
        
        data_mes.update({municipios[i] + "_" + fecha.strftime("%Y%m"): [latitudes, longitudes]})
        
pickle.dump(data_mes, open("/home/storm/lightnings_research/analysis/conteo_VA/conteo_mensual_VA_" + str(resolution) + "km.bin", "wb"))


########
# data = pickle.load(open("/home/storm/lightnings_research/analysis/conteo_VA/conteo_mensual_VA_" + str(resolution) + "km.bin", "rb"))

# keys = list(data.keys())

# for i, m in enumerate(municipios):

#     suma = 0

#     for a in range(2019, 2023):

#         for M in range(1, 13):

#             k = m+"_"+str(a)+"%.2d" % M

#             if k in keys:

#                 suma = suma + len(data[k][0])
                
#             else:
                
#                 k
    
#     print(m, suma/4, suma/(area_municipios[i]*4))
