import pandas as pd
import numpy as np
import datetime as dt
import glob
import pickle

# resolución malla
resolution = 2

# paths
#path_save = "/home/storm/lightnings_research/analysis/mallas_conteos/VA_" + str(resolution) + "km_calidad/" 
path_save = "/home/storm/lightnings_research/analysis/mallas_conteos/colombia_" + str(resolution) + "km_calidad/"

# region
# latmax = 6.538
# latmin = 5.971 
# lonmax = -75.190 
# lonmin = -75.753 
latmax = 13
latmin = -5 
lonmax = -66
lonmin = -80

# functions
"create grid of pixels"
def grid(latmin, latmax, lonmin, lonmax, res=8):
    # Define the boundaries
    lat_start, lat_end = np.floor(latmin), np.ceil(latmax)
    lon_start, lon_end = np.floor(lonmin), np.ceil(lonmax)
    # Define the resolution
    resolution_km = res
    earth_circumference_km = 40075
    degree_to_km = earth_circumference_km / 360
    # Calculate the number of pixels
    lat_pixels = round((lat_end - lat_start) * degree_to_km / resolution_km)
    lon_pixels = round((lon_end - lon_start) * degree_to_km / resolution_km)
    # Generate the grids
    lats = np.linspace(lat_start, lat_end, lat_pixels)
    lons = np.linspace(lon_start, lon_end, lon_pixels)
    # Create a meshgrid
    lon_grid, lat_grid = np.meshgrid(lons, lats)
    return lat_grid, lon_grid

# crea la malla
lat_grid, lon_grid = grid(latmin, latmax, lonmin, lonmax, resolution)

#files = glob.glob("/home/storm/lightnings_research/archivos_data/colombia_by_month/GLMflashes_*.csv")
files = glob.glob("/home/storm/lightnings_research/archivos_data/colombia_by_month_calidad/GLMflashes_*.csv")
files.sort()

for f in files[60:72]:
    
    "lee fecha"    
    fecha       = f[f.find("-66E")+5:f.find("-66E")+5+6]
    fecha       = dt.datetime.strptime(fecha, "%Y%m")

    print(fecha)


    "lee archivo"
    DF          = pd.read_csv(f, index_col="date")

    "hace un primer filtro para hacer más rápida la busqueda"
    DF = DF[(DF['latitude'] >= np.floor(latmin)) & (DF['latitude'] <= np.ceil(latmax)) & (DF['longitude'] >= np.floor(lonmin)) & (DF['longitude'] < np.ceil(lonmax))]

    
    grid_conteo = np.zeros(np.array(lat_grid.shape)-1)
    
    for i in range(lat_grid.shape[0]-1):
        
        for j in range(lat_grid.shape[1]-1):
            
            lat_1       = lat_grid[i,j]
            lat_2       = lat_grid[i+1,j+1]

            lon_1       = lon_grid[i,j]
            lon_2       = lon_grid[i+1,j+1]
            
            filtered_df = DF[(DF['latitude'] >= lat_1) & (DF['latitude'] < lat_2) & (DF['longitude'] >= lon_1) & (DF['longitude'] < lon_2)]

            grid_conteo[i,j] = len(filtered_df)
            
    pickle.dump(grid_conteo, open(path_save + "malla_conteo_" + fecha.strftime("%Y%m") + ".bin", "wb"))



