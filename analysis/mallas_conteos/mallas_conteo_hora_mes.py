import pandas as pd
import numpy as np
import datetime as dt
import glob
import pickle

RES = 2 # resolución de la malla
"functions"

"create grid of pixels of 8 kilometers"
def grid(res=8):
    # Define the boundaries
    lat_start, lat_end = -5, 13
    lon_start, lon_end = -80, -66
    # Define the resolution
    resolution_km = res
    earth_circumference_km = 40075
    degree_to_km = earth_circumference_km / 360
    # Calculate the number of pixels
    lat_pixels = round((lat_end - lat_start) * degree_to_km / resolution_km)
    lon_pixels = round((lon_end - lon_start) * degree_to_km / resolution_km)
    # Generate the grids
    lats = np.linspace(lat_start, lat_end, lat_pixels)
    lons = np.linspace(lon_start, lon_end, lon_pixels)
    # Create a meshgrid
    lon_grid, lat_grid = np.meshgrid(lons, lats)
    return lat_grid, lon_grid

lat_grid, lon_grid = grid(RES)

files = glob.glob("/home/storm/lightnings_research/archivos_data/colombia_by_month_calidad/GLMflashes_*.csv")
files.sort()

for f in files[60:72]:
    
    fecha       = f[f.find("-66E")+5:f.find("-66E")+5+6]
    fecha       = dt.datetime.strptime(fecha, "%Y%m")
    print(fecha)
    
    grid_conteo = np.zeros([24] + list(np.array(lat_grid.shape)-1))
    
    df          = pd.read_csv(f, index_col="date")
    index_df    = pd.DatetimeIndex(df.index)
    
    for h in range(0, 24):
        df_selected = df[index_df.hour == h]
    
        for i in range(lat_grid.shape[0]-1):

            for j in range(lat_grid.shape[1]-1):
                
                lat_1       = lat_grid[i,j]
                lat_2       = lat_grid[i+1,j+1]

                lon_1       = lon_grid[i,j]
                lon_2       = lon_grid[i+1,j+1]
                
                filtered_df = df_selected[(df_selected['latitude'] >= lat_1) & (df_selected['latitude'] < lat_2) & (df_selected['longitude'] >= lon_1) & (df_selected['longitude'] < lon_2)]

                grid_conteo[h, i,j] = len(filtered_df)
                
    pickle.dump(grid_conteo, open("/home/storm/lightnings_research/analysis/mallas_conteos/colombia_" + str(RES) + "km_calidad_diurnal_monthly/malla_conteo_" + fecha.strftime("%Y%m") + ".bin", "wb"))

            

