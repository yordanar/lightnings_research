import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.colors as colors

# función para pintar matriz de contingencia
def contingency_matrix(TP, TN, FP, FN, path_save, name_distribution, name_contingencia):
    # TP           = 2000 
    # TN           = 2500
    # FP           = 1153
    # FN           = 4400

    num_positive = TP + FN
    num_negative = TN + FP
    
    # cálculos
    total_data    = num_positive + num_negative
    perc_positive = round(num_positive/(total_data) * 100)
    perc_negative = 100 - perc_positive # num_negative/(total_data) * 100

    perc_TP       = round(TP/total_data*100)
    perc_FP       = round(FP/total_data*100)
    perc_TN       = round(TN/total_data*100)
    perc_FN       = round(FN/total_data*100)

    #crea matriz de 100 pixeles de 1's y 0's
    rows    = 10
    columns = 10

    reales = np.zeros((rows, columns))

    count = 0
    flag_break = False
    for j in range(columns):
        for i in range(rows):
            
            reales[i, j] = 1
            
            count = count + 1
            
            if round(count/(rows*columns) * 100) >= perc_positive:
                flag_break = True
                break
        
        if flag_break == True:
            break
            

    # pinta la matriz de 0's y 1's
    "crea la barra de colores"
    Nc        = 2 # número de colores
    Colors    = [(46/255., 134/255., 193/255.), (231/255., 70/255., 60/255.)]
    bounds    = np.array([-0.5, 0.5, 1.5])
    labelsx   = ['negative', 'positive']
    cmap_name = 'my_list'
    cm        = colors.LinearSegmentedColormap.from_list(cmap_name, Colors, N=Nc)
    norm      = colors.BoundaryNorm(boundaries=bounds, ncolors=Nc)

    "crea las coordenadas"
    x = np.arange(columns+1)
    y = np.arange(rows+1)
    X, Y = np.meshgrid(x, y)

    "pinta"
    plt.close("all") 
    fig   = plt.figure(figsize=(12,8))
    ax    = fig.add_axes([0.1, 0.17, 0.8, 0.73])
    cb_ax = fig.add_axes([0.1, 0.05, 0.8, 0.12], frame_on=False, visible=False)
    C     = ax.pcolormesh(X, Y, reales, cmap=cm, norm=norm)

    "colorbar"
    cb  = fig.colorbar(C, orientation='horizontal', ticks=np.arange(Nc), aspect=70, ax=cb_ax)
    cb.ax.tick_params (labelsize=25)
    #cb.ax.set_position([0.07, 0.08, 0.38, 0.28])
    cb.ax.set_xticklabels(labelsx)

    "set ticks and labels size"
    ax.set_xticks(x)
    ax.set_xticklabels(x, fontsize=20)
    ax.set_yticks(y)
    ax.set_yticklabels(y, fontsize=20)

    "pinta lineas"
    for j in x:
        ax.axvline(x=j, ymin=0, ymax=rows, color="k")
    for i in y:
        ax.axhline(y=i, xmin=0, xmax=columns, color="k")
        
    "save"    
    plt.savefig(path_save + name_distribution)
    plt.close("all")





    # pinta la matriz de 0's y 1's con los pronósticos
    "crea la barra de colores"
    Nc        = 2 # número de colores
    Colors    = [(46/255., 134/255., 193/255.), (231/255., 70/255., 60/255.)]
    bounds    = np.array([-0.5, 0.5, 1.5])
    labelsx   = ['negative', 'positive']
    cmap_name = 'my_list'
    cm        = colors.LinearSegmentedColormap.from_list(cmap_name, Colors, N=Nc)
    norm      = colors.BoundaryNorm(boundaries=bounds, ncolors=Nc)

    "crea las coordenadas"
    x = np.arange(columns+1)
    y = np.arange(rows+1)
    X, Y = np.meshgrid(x, y)

    "pinta"
    plt.close("all") 
    fig   = plt.figure(figsize=(12,8))
    ax    = fig.add_axes([0.1, 0.17, 0.8, 0.73])
    cb_ax = fig.add_axes([0.1, 0.05, 0.8, 0.12], frame_on=False, visible=False)
    C     = ax.pcolormesh(X, Y, reales, cmap=cm, norm=norm)

    "colorbar"
    cb  = fig.colorbar(C, orientation='horizontal', ticks=np.arange(Nc), aspect=70, ax=cb_ax)
    cb.ax.tick_params (labelsize=25)
    #cb.ax.set_position([0.07, 0.08, 0.38, 0.28])
    cb.ax.set_xticklabels(labelsx)

    "set ticks and labels size"
    ax.set_xticks(x)
    ax.set_xticklabels(x, fontsize=20)
    ax.set_yticks(y)
    ax.set_yticklabels(y, fontsize=20)

    "pinta lineas"
    for j in x:
        ax.axvline(x=j, ymin=0, ymax=rows, color="k")
    for i in y:
        ax.axhline(y=i, xmin=0, xmax=columns, color="k")
        
    "pinta los verdaderos positivos"
    counter = 0
    flag_break = False

    for j in range(columns):
        for i in range(rows):
            
            if round(counter/(rows*columns) * 100) <= perc_TP:
                ax.plot(j+0.5, i+0.5, marker="*", markersize=30, color="#f5b041")
            else:
                ax.plot(j+0.5, i+0.5, marker="o", markersize=30, color="#9b59b6") 
            
            counter = counter + 1

            if round(counter/(rows*columns) * 100) >= perc_TP + perc_FN:
                flag_break = True
                break
        
        if flag_break == True:
            break

    "pinta los negativos"
    counter = 0
    flag_break = False

    for j in np.arange(columns)[::-1]:
        for i in np.arange(rows)[::-1]:
            
            if round(counter/(rows*columns) * 100) <= perc_TN:
                ax.plot(j+0.5, i+0.5, marker="*", markersize=30, color="#f5b041")
            else:
                ax.plot(j+0.5, i+0.5, marker="o", markersize=30, color="#9b59b6")

            counter = counter + 1
            if round(counter/(rows*columns) * 100) >= perc_TN + perc_FP:
                flag_break = True
                break
        
        if flag_break == True:
            break

    "save"    
    plt.savefig(path_save + name_contingencia)
    plt.close("all")



contingency_matrix(688978, 1156303, 81010, 85869, "/home/storm/lightnings_research/indices/", "distr_ppt", "ppt_cont")